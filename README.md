[![pipeline status](https://gitlab.cern.ch/silab/bdaq53/badges/development/pipeline.svg)](https://gitlab.cern.ch/silab/bdaq53/commits/development) [![coverage report](https://gitlab.cern.ch/silab/bdaq53/badges/development/coverage.svg)](https://gitlab.cern.ch/silab/bdaq53/commits/development)

# bdaq53
DAQ and verification environment for the [RD53A](https://cds.cern.ch/record/2287593) prototype based on the [Basil](https://github.com/SiLab-Bonn/basil) framework.
The firmware and software are compatible with the various hardware platforms see [Readout-Hardware](https://gitlab.cern.ch/silab/bdaq53/wikis/Hardware/Readout-Hardware) for details.

### Installation
General information about setup and usage, FAQ, tweaks etc. can be found on our [wiki page](https://gitlab.cern.ch/silab/bdaq53/wikis/home).

- Install [conda](https://conda.io/miniconda.html) for Python 3

- Install dependencies and bdaq53:
```
conda install numpy bitarray pyyaml scipy numba pytables matplotlib tqdm pyzmq blosc psutil coloredlogs
pip install git+https://gitlab.cern.ch/silab/bdaq53.git@master
```

### Usage
- [Flash appropriate bit file](https://gitlab.cern.ch/silab/bdaq53/wikis/fpga-configuration), which is attached to the latest [tag](https://gitlab.cern.ch/silab/bdaq53/tags)

- Check if everything is correctly connected (according to your [IP settings](https://gitlab.cern.ch/silab/bdaq53/wikis/Hardware/FPGA-configuration#ip-address-configuration) ):
```
ping 192.168.10.12
```

- Run a scan:
```
bdaq53 scan_digital
```
- For help, run:
```
bdaq53 --help
```
- Use the online monitor:
```
bdaq53_monitor
```
- Connect to EUDAQ:
```
bdaq53_eudaq
```

## Support
For questions and comments, visit https://mattermost.web.cern.ch/rd53-testing

## Development
If you want to help in the development of bdaq53, a few extra steps are needed:

- Clone bdaq53 repository from slected branch
```bash
git clone -b development https://gitlab.cern.ch/silab/bdaq53.git
```

- Download folder with test data and example files (if needed)
  1. Install git lfs extension: https://github.com/git-lfs/git-lfs/wiki/Installation
  2. Download data folder with large binary files:

    ```bash
     git lfs pull
    ```

- Setup bdaq53
```bash
cd bdaq53
python setup.py develop
```

- Create a branch for your changes and once you're done, create a merge request to the development branch

### Firmware

- Download and setup [Basil](https://github.com/SiLab-Bonn/basil):
```bash
git clone -b development https://github.com/SiLab-Bonn/basil;
cd basil
python setup.py develop
cd ..
```

- Download and install Vivado [2018.1](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/2018-1.html) + a valid licence. The web edition is sufficient for BDAQ53 but not for the KC705 FPGA model. Other versions of Vivado should also work.

- Set the environmental variable `XILINX_VIVADO` for Vivado according to your xilinx installation directory (default should be `/opt/XILINX/Vivado/2018.1/`):
```bash
export XILINX_VIVADO={your_xilinx_install_directory}/Vivado/2018.1
```

- Download the latest Kintex 7 version of SiTCP and copy the files to the `/firmware/SiTCP` folder [GitHub project](https://github.com/BeeBeansTechnologies/SiTCP_Netlist_for_Kintex7) and add the line `` `default_nettype wire `` at the top of `TIMER.V` and `WRAP_SiTCP_GMII_XC7K_32K.V`.

- Modify the following line in `/firmware/SiTCP/WRAP_SiTCP_GMII_XC7K_32_K.V`:

   Replace this line 
    ```verilog
    assign MY_IP_ADDR[31:0] = (~FORCE_DEFAULTn | (EXT_IP_ADDR[31:0]==32'd0) ? DEFAULT_IP_ADDR[31:0] : EXT_IP_ADDR[31:0] );
    ```
   with
    ```verilog
    assign MY_IP_ADDR[31:0] = EXT_IP_ADDR[31:0];
    ```

- Generate a bit file

    You can find bitfiles and memory configuration files for BDAQ53 and KC705 [here](https://gitlab.cern.ch/silab/bdaq53/tags). They can be used to configure the FPGA or to flash the non volatile QSPI memory chips [(Wiki page with instructions)](https://gitlab.cern.ch/silab/bdaq53/wikis/Hardware/FPGA-configuration).

    If you prefer to build your own configuration, create the Vivado projects by starting the run.tcl script from a console. The script also generates the bitfiles.
    ```bash
    cd {YOUR_BDAQ53_PATH}/firmware/vivado
    vivado -mode tcl -source run.tcl
    ```
    The projects are located in a new folder _{YOUR_BDAQ53_PATH}/firmware/vivado`/designs`_.


### Simulation

In order to run the simulation environment with the RTL model of RD53A, you can follow the same instructions sa above, but skip the bitfile generation.
- Download and setup [cocotb](https://github.com/potentialventures/cocotb):
```bash
pip install cocotb
```

- Export the RD53A chip design repository folder to the env. variable `RD53A_SRC`. It will be used as the DUT within the simulation environment:
```bash
export RD53A_SRC={your RD53A chip design directory}
```

- The simulation supports the HDL simulators IUS and Questa. If needed, install one of them.

    ***Questa***: The free simulator package is sufficient. After installation, run:
    ```bash
    export PATH={your Questa directory}/questasim/linux_x86_64:$PATH
    ./install.linux64
    ```

- Setup the simulator, by sourcing one of the `setup_[ius,questa].sh` scripts in `/tests`. This compiles the necessary libraries for the selected simulator.

    For example:
    ```bash
    source setup_questa.sh
    ```

- Run one of the Python tests "*test_....py*" in the `/tests` folder.


### Repository structure
`/firmware` Firmware sources
-  `/src`  Verilog sources and constraint files for the RD53A-DAQ firmware.
-  `/vivado` - run.tcl script to generate a Vivado project from the source files.

`/bdaq53` Software package including BASIL drivers and configuration.
-  `/scans` Different scans and scan configuration files
-  `/analysis` Software for data anlysis and plotting
-  `/tests` Testbench and unittests to run a cosimulation with the DAQ firmare and the RD53 digital design as well as software unit tests.

`/data`  Git-lfs folder with large binary files for examples and unit test fixtures