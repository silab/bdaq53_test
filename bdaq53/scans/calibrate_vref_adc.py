#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This calibration routine calibrates the VREF ADC to 0.9V.
    Please make sure you have a Multimeter connected to your PC and activated the periphery module in testbench.yaml.

    Probe_locations:
        direct:  Connect the ground lead of the multimeter to the ground point on the SCC: Use Pin 8 of the SLDO_PLL_MON header.
                Remove the VREF_ADC jumper on the SCC and connect the positive lead of the multimeter to right pin of this jumper.

        lemo:    Connect your Multimeter to the IMUX Lemo on the single chip card.
'''

from tqdm import tqdm
import time
import numpy as np

from bdaq53.system.scan_base import ScanBase

scan_configuration = {
    'samples': 2,
    'periphery_device': 'Sourcemeter',
    'probe_location': 'lemo'
}


class VREFADCCalib(ScanBase):
    scan_id = 'calibrate_vrefadc'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

    def _scan(self, samples=1, target=0.9, probe_location='direct', periphery_device='Multimeter', yes=False, **_):
        '''
        VREF_ADC calibration main loop

        Parameters
        ----------
        samples: int
            Amount of samples to measure at each point.
        target: float
            Target VREF_ADC to trim to. Default for RD53A is 0.9V.
        probe_location: 'direct' or 'lemo'
            Is the multimeter / sourcemeter connected to the LEMO VMUX out connector or directly to the VREF_ADC pin?
        periphery_device: 'Multimeter' or 'Sourcemeter'
            Are you using a Multimeter or Sourcemeter for this measurement?
        yes: bool
            If true, skip interruption.
        '''

        if probe_location == 'lemo':
            bg_factor = 2.
            self.chip.registers['MONITOR_SELECT'].write(int('1' + format(32, '06b') + format(9, '07b'), 2))
        else:
            if not yes:
                input('Connect the multimeter to the correct pin for chip {0} and press Enter to continue...'.format(self.chip.get_sn()))
            bg_factor = 1.

        adc_trim = int(format(self.chip.registers['MONITOR_CONFIG'].read(), '011b')[-6:], 2)

        self.log.info('Trimming VREF_ADC...')
        pbar = tqdm(total=5, unit=' Trim steps')
        vref_trim = 0
        for i in range(4, -1, -1):
            vref_trim += 2 ** i
            self.chip.registers['MONITOR_CONFIG'].write(int(format(vref_trim, '05b') + format(adc_trim, '06b'), 2))
            time.sleep(0.5)

            voltages = []
            for _ in range(samples):
                voltages.append(self.periphery.aux_devices[periphery_device].get_voltage())
            voltage = np.mean(voltages) * bg_factor

            if voltage > target * 1.01:
                vref_trim -= 2 ** i
            pbar.update(1)
        pbar.close()

        self.chip.registers['MONITOR_CONFIG'].write(int(format(vref_trim, '05b') + format(adc_trim, '06b'), 2))
        self.chip.configuration['trim']['MON_BG_TRIM'] = vref_trim

        self.log.success('Optimal MON_BG_TRIM is {0}. Trimmed voltage is {1:1.3f}V.'.format(vref_trim, voltage))


if __name__ == "__main__":
    with VREFADCCalib(scan_config=scan_configuration) as calibration:
        calibration.scan()
