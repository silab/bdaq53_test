#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Tune global and local threshold and disable noisy and stuck pixels.
    Run a threshold scan in the end to verify results.
'''

from bdaq53.scans.tune_global_threshold import GDACTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.scan_noise_occupancy import NoiseOccScan
from bdaq53.scans.scan_stuck_pixel import StuckPixelScan
from bdaq53.scans.scan_threshold import ThresholdScan


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    'maskfile': None,

    # Target threshold
    'VCAL_MED': 500,
    'VCAL_HIGH': 680,   # 180 DVCAL corresponds to about 2000 e

    # Noise occupancy scan settings
    'n_triggers': 1e7,
    'min_occupancy': 10
}


if __name__ == '__main__':
    with GDACTuning(scan_config=scan_configuration) as global_tuning:
        global_tuning.scan()
        gdacs = global_tuning.analyze()

    with TDACTuning(scan_config=scan_configuration) as local_tuning:
        local_tuning.scan()
        tdacs = local_tuning.analyze()

    scan_configuration['maskfile'] = 'auto'     # Definitely use maskfile created by tuning from here on
    with NoiseOccScan(scan_config=scan_configuration) as noise_occ_scan:
        noise_occ_scan.scan()
        noise_occ_scan.analyze()

    with StuckPixelScan(scan_config=scan_configuration) as stuck_pix_scan:
        stuck_pix_scan.scan()
        stuck_pix_scan.analyze()

    # Calculate scan range for threshold scan
    target_threshold = scan_configuration['VCAL_HIGH'] - scan_configuration['VCAL_MED']
    scan_configuration['VCAL_HIGH_start'] = scan_configuration['VCAL_MED'] + target_threshold - 30
    scan_configuration['VCAL_HIGH_stop'] = scan_configuration['VCAL_MED'] + target_threshold + 30
    scan_configuration['VCAL_HIGH_step'] = 2
    with ThresholdScan(scan_config=scan_configuration) as thr_scan:
        thr_scan.scan()
        thr_scan.analyze()
