#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
   Extract timewalk by measuring distance between reference signal (from FPGA) and Hitor signal (from charge injections) using the TDC module.
   For this, TX0 has to be connected with RX0 (as short as possible) in order to connect
   the trigger signal (CMD_LOOP_START_PULSE) to the trigger input of the TDC module. Check the wiki for a detailed instruction.
   If the hit delay is not within the measurement range (255 x 1.5625 ns) adjust the delay of pulser_cmd_start_loop.

   Note:
   In order to determine properly the timewalk highest injected charge should be in region of saturating hit delay.
'''

from tqdm import tqdm
import numpy as np
import tables as tb
from scipy import stats

from bdaq53.scans.calibrate_hitor import HitorCalib
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import analysis_utils as au


scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'n_injections': 500,  # increase amount of injections for better statistics
    'VCAL_MED': 500,
    # VCAL values which are scanned
    'VCAL_HIGH_values': range(700, 2000, 100)
}


class TimewalkScan(HitorCalib):
    scan_id = "timewalk_scan"
    # No _configure/_scan method needed since it is the same as for the hitor calibration scan, thus inherit from hitor calibration

    def _analyze(self):
        '''
        Analyze raw data and timewalk.
        '''
        max_tdc_trig_dist = 2**8
        max_timewalk = 50
        max_hit_delay = 25.0

        def create_timewalk_hist(hit_array, n_scan_params, max_tdc_trig_dist):
            '''
            Create timewalk histograms.

            Parameters
            ----------
            hit_array : array_like
                Array containing hits.
            n_scan_params : int
                Length of scan parameters.
            max_tdc_trig_dist : int
                Maximum TDC trigger distance value.
            '''

            # 2D Timewalk histogram. FIXME: Could be int, but numpy.histogram returns float.
            hist_tdc_dist = np.zeros(shape=(n_scan_params, max_tdc_trig_dist), dtype=np.float32)  # 2d tdc distance histogram
            hist_tdc_dist_single_pixel = np.zeros(shape=(n_scan_params, max_tdc_trig_dist), dtype=np.float32)  # 2d tdc distance histogram for single pixel
            hist_tdc_dist_corr = np.zeros(shape=(n_scan_params, max_tdc_trig_dist), dtype=np.float32)  # 2d tdc distance histogram offset corrected
            stat_2d_tdc_mean_hists = np.zeros(shape=(400, 192, n_scan_params), dtype=np.float32)  # 2d map of mean tdc distance for each scan parameter

            self.log.info('Creating timewalk histograms...')
            # Loop first over last scan parameter in order to get mean delay at highest charge for each pixel.
            # Needed for individual pixel timewalk correction.
            for par, hits in au.hits_of_parameter(hits=hit_array, scan_parameter_selection=[n_scan_params - 1], reverse_order=True, chunk_size=10000000):
                # Select only events with good TDC status (no errors, ambiguities)
                sel_tdc = (hits['tdc_status'] == 1)
                hits_sel_tdc = hits[sel_tdc]
                tdc_trg_dist = hits_sel_tdc['tdc_timestamp']
                col = hits_sel_tdc['col']
                row = hits_sel_tdc['row']
                bins = [range(0, 401), range(0, 193)]
                # Calculate mean timewalk offset for each pixel (timewalk at highest charge)
                timewalk_offset, _, _, _ = stats.binned_statistic_2d(x=col, y=row, values=tdc_trg_dist,
                                                                     statistic='mean', bins=bins)

            # Loop over hits with same scan parameter
            for par, hits in tqdm(au.hits_of_parameter(hits=hit_array, chunk_size=10000000), total=n_scan_params):
                # Select only events with good (no errors, ambiguities) TDC status
                sel_tdc = (hits['tdc_status'] == 1)
                hits_sel_tdc = hits[sel_tdc]
                self.log.debug('Selected %.1f%% of hits for scan parameter id %i' % (100. * np.count_nonzero(sel_tdc) / sel_tdc.shape[0], par))
                tdc_trg_dist = hits_sel_tdc['tdc_timestamp']
                scan_param_id = hits_sel_tdc['scan_param_id']
                col = hits_sel_tdc['col']
                row = hits_sel_tdc['row']
                single_pixel_sel = np.logical_and(col == selected_pixel[0], row == selected_pixel[1])
                # Subtract delay offset for each pixel
                tdc_trg_dist_corr = tdc_trg_dist - timewalk_offset[col, row]
                single_pixel_tdc = tdc_trg_dist[single_pixel_sel]
                single_pixel_tdc = single_pixel_tdc - timewalk_offset[selected_pixel[0], selected_pixel[1]]

                # Histogram charge values and trigger distance values
                x_edges = range(0, n_scan_params + 1)
                y_edges = range(0, max_tdc_trig_dist + 1)
                hist, xedges_timewalk, yedges_timewalk = np.histogram2d(x=scan_param_id,
                                                                        y=tdc_trg_dist,
                                                                        bins=(x_edges, y_edges))
                hist_tdc_dist += hist

                hist, xedges_timewalk, yedges_timewalk = np.histogram2d(x=scan_param_id[single_pixel_sel],
                                                                        y=single_pixel_tdc,
                                                                        bins=(x_edges, y_edges))

                hist_tdc_dist_single_pixel += hist

                hist, xedges_timewalk, yedges_timewalk = np.histogram2d(x=scan_param_id,
                                                                        y=tdc_trg_dist_corr,
                                                                        bins=(x_edges, y_edges))

                hist_tdc_dist_corr += hist

                # Calculate 2D map of mean tdc trigger distance
                bins = [range(0, 401), range(0, 193)]
                stat_2d_tdc_mean_hists[:, :, par], _, _, _ = stats.binned_statistic_2d(x=col, y=row, values=tdc_trg_dist,
                                                                                       statistic='mean', bins=bins)

            return hist_tdc_dist, hist_tdc_dist_single_pixel, hist_tdc_dist_corr, xedges_timewalk, yedges_timewalk, stat_2d_tdc_mean_hists

        self.configuration['bench']['analysis']['store_hits'] = True
        self.configuration['bench']['analysis']['use_tdc_trigger_dist'] = True
        self.configuration['bench']['analysis']['analyze_tdc'] = True
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            vcal_high = a.get_scan_param_values(scan_parameter='vcal_high')
            vcal_med = a.get_scan_param_values(scan_parameter='vcal_med')
            start_col, stop_col = a.run_config['start_column'], a.run_config['stop_column']
            start_row, stop_row = a.run_config['start_row'], a.run_config['stop_row']

        # Create timewalk histograms
        with tb.open_file(self.output_filename + '_interpreted.h5', mode='r+') as io_file:
            enable_mask = io_file.root.masks.disable[start_col:stop_col, start_row:stop_row]
            enabled_pixels = (np.where(enable_mask == 1))
            # Select random pixel from hits for which timewalk curve is plotted
            index = np.random.randint(0, enabled_pixels[0].shape[0])
            selected_pixel = [enabled_pixels[0][index] + start_col, enabled_pixels[1][index] + start_row]

            delta_vcal_range = vcal_high - vcal_med
            n_scan_params = len(delta_vcal_range)
            percentile = 99.85

            (hist_tdc_dist, hist_tdc_dist_single_pixel, hist_tdc_dist_corr, xedges_timewalk, yedges_timewalk, stat_2d_tdc_mean_hists) = create_timewalk_hist(
                hit_array=io_file.root.Hits,
                n_scan_params=n_scan_params,
                max_tdc_trig_dist=max_tdc_trig_dist)

            yedges_timewalk = yedges_timewalk * 1.5625  # Calculate timewalk in ns, sampled with 640 MHz
            # Convert xedges from scan parameter integer into delta VCAL values. Extrapolate last bin using last two values.
            xedges_timewalks = []
            for par in xedges_timewalk:
                if par == n_scan_params:
                    xedges_timewalks.append(delta_vcal_range[-1] + (delta_vcal_range[-1] - delta_vcal_range[-2]))
                else:
                    xedges_timewalks.append(delta_vcal_range[int(par)])
            xedges_timewalk = np.array(xedges_timewalks)
            timewalk_bin_centers = (yedges_timewalk[:-1] + yedges_timewalk[1:]) / 2.

            # Calculate mean timewalk and low/high percentiles from histograms for single pixel
            mean_single_pixel, perc_high_single_pixel, perc_low_single_pixel = [], [], []
            for one_slice in hist_tdc_dist_single_pixel:
                if np.count_nonzero(one_slice):  # check if slice has non-zero occupancy (could be scan parameter below threshold)
                    mean_single_pixel.append(np.dot(one_slice, timewalk_bin_centers) / np.sum(one_slice))
                    perc_high_single_pixel.append(np.percentile(np.ma.repeat(timewalk_bin_centers, one_slice.astype(np.int64), axis=0), q=percentile))
                    perc_low_single_pixel.append(np.percentile(np.ma.repeat(timewalk_bin_centers, one_slice.astype(np.int64), axis=0), q=100.0 - percentile))
                else:
                    perc_high_single_pixel.append(np.nan)
                    perc_low_single_pixel.append(np.nan)
                    mean_single_pixel.append(np.nan)
            mean_single_pixel, perc_high_single_pixel, perc_low_single_pixel = np.array(mean_single_pixel), np.array(perc_high_single_pixel), np.array(perc_low_single_pixel)

            # Calculate mean timewalk and low/high percentiles from histograms for all pixels
            mean_pixel_corr, perc_high_pixel_corr, perc_low_pixel_corr = [], [], []
            for one_slice in hist_tdc_dist_corr:
                if np.count_nonzero(one_slice):  # check if slice has non-zero occupancy (could be scan parameter below threshold)
                    mean_pixel_corr.append(np.dot(one_slice, timewalk_bin_centers) / np.sum(one_slice))
                    perc_high_pixel_corr.append(np.percentile(np.ma.repeat(timewalk_bin_centers, one_slice.astype(np.int64), axis=0), q=percentile))
                    perc_low_pixel_corr.append(np.percentile(np.ma.repeat(timewalk_bin_centers, one_slice.astype(np.int64), axis=0), q=100.0 - percentile))
                else:
                    mean_pixel_corr.append(np.nan)
                    perc_high_pixel_corr.append(np.nan)
                    perc_low_pixel_corr.append(np.nan)
            mean_pixel_corr, perc_high_pixel_corr, perc_low_pixel_corr = np.array(mean_pixel_corr), np.array(perc_high_pixel_corr), np.array(perc_low_pixel_corr)

            # Store created histograms
            out_hist_tdc_dist = io_file.create_carray(
                io_file.root,
                name='hist_tdc_dist',
                title='2d tdc distance histogram',
                obj=hist_tdc_dist,
                filters=tb.Filters(complib='blosc',
                                   complevel=5,
                                   fletcher32=False))

            io_file.create_carray(
                io_file.root,
                name='hist_tdc_dist_single_pixel',
                title='2d tdc distance histogram for single pixel',
                obj=hist_tdc_dist_single_pixel,
                filters=tb.Filters(complib='blosc',
                                   complevel=5,
                                   fletcher32=False))

            io_file.create_carray(
                io_file.root,
                name='hist_tdc_dist_corr',
                title='2d tdc distance histogram offset corrected',
                obj=hist_tdc_dist_corr,
                filters=tb.Filters(complib='blosc',
                                   complevel=5,
                                   fletcher32=False))

            io_file.create_carray(
                io_file.root,
                name='stat_2d_tdc_mean_hists',
                title='2d map of mean tdc distance for each scan parameter',
                obj=stat_2d_tdc_mean_hists,
                filters=tb.Filters(complib='blosc',
                                   complevel=5,
                                   fletcher32=False))

            out_hist_tdc_dist.attrs.xedges_timewalk = xedges_timewalk
            out_hist_tdc_dist.attrs.yedges_timewalk = yedges_timewalk
            out_hist_tdc_dist.attrs.perc_high_single_pixel = perc_high_single_pixel
            out_hist_tdc_dist.attrs.perc_low_single_pixel = perc_low_single_pixel
            out_hist_tdc_dist.attrs.perc_high_pixel_corr = perc_high_pixel_corr
            out_hist_tdc_dist.attrs.perc_low_pixel_corr = perc_low_pixel_corr
            out_hist_tdc_dist.attrs.mean_pixel_corr = mean_pixel_corr
            out_hist_tdc_dist.attrs.mean_single_pixel = mean_single_pixel
            out_hist_tdc_dist.attrs.delta_vcal_range = delta_vcal_range
            out_hist_tdc_dist.attrs.max_timewalk = max_timewalk

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()
                # Plot timewalk histograms
                p.create_timewalk_plots(hist_tdc_dist, hist_tdc_dist_single_pixel, hist_tdc_dist_corr, xedges_timewalk, yedges_timewalk,
                                        mean_single_pixel, perc_high_single_pixel, perc_low_single_pixel, mean_pixel_corr, perc_high_pixel_corr,
                                        perc_low_pixel_corr, stat_2d_tdc_mean_hists, selected_pixel, delta_vcal_range, max_timewalk, max_hit_delay)


if __name__ == "__main__":
    with TimewalkScan(scan_config=scan_configuration) as scan:
        scan.scan()
        scan.analyze()
