#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over different amounts of injected charge
    to find the effective threshold of the enabled pixels.
    It is ~40% faster than the standard scan_threshold.py script
    thanks to shortened injection times and the use of
    external trigger words for offline analysis.
    The shortened injection times requires the desoldering
    of C30 and C31 on the Single Chip Card of RD53A
'''

from tqdm import tqdm
import numpy as np

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 700,
    'VCAL_HIGH_stop': 1200,
    'VCAL_HIGH_step': 10
}


class ThresholdScan(ScanBase):
    scan_id = 'fast_threshold_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, TDAC=None, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        TDAC : int / np.ndarray
            If int: TDAC value to use for all enabled pixels, overwriting any existing mask.
            If np.ndarray: TDAC mask to use for enabled pixels. Has to have shape to match start_column:stop_column, start_row:stop_row
        '''
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        if TDAC is not None:
            if type(TDAC) == int:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC
            elif type(TDAC) == np.ndarray:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC[start_column:stop_column, start_row:stop_row]

        self.chip.masks.update(force=True)
        self.bdaq['tlu']['TRIGGER_ENABLE'] = True
        self.bdaq['tlu']['TRIGGER_MODE'] = 0
        self.bdaq['tlu']['TRIGGER_SELECT'] = 2 ** 8
        self.bdaq['tlu']['TRIGGER_COUNTER'] = 0

    def _scan(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, CAL_EDGE_latency=9, wait_cycles=300, **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''

        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        scan_param_id_range = range(0, len(vcal_high_range))

        self.log.info('Starting scan...')
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_start, vcal_med=VCAL_MED)
        self.chip.registers['LATENCY_CONFIG'].write(CAL_EDGE_latency * 4 + 12)
        pbar = tqdm(total=self.chip.masks.get_mask_steps() * len(vcal_high_range), unit=' Mask steps')
        with self.readout():
            for fe in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                for cnt, vcal_high in enumerate(vcal_high_range):
                    scan_param_id = scan_param_id_range[cnt]
                    self.chip.registers['VCAL_HIGH'].write(vcal_high)
                    self.add_trigger_table_data(n_injections, scan_param_id)
                    if not fe == 'skipped' and fe == 'SYNC':
                        self.chip.inject_analog_single(repetitions=n_injections, latency=CAL_EDGE_latency,
                                                       wait_cycles=wait_cycles, send_ecr=True)
                    elif not fe == 'skipped':
                        self.chip.inject_analog_single(repetitions=n_injections, latency=CAL_EDGE_latency,
                                                       wait_cycles=wait_cycles)
                    pbar.update(1)
                vcal_high_range = np.flip(vcal_high_range)
                scan_param_id_range = np.flip(scan_param_id_range)

        pbar.close()
        self.log.success('Scan finished')


    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            mean_thr = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            mean_noise = np.median(a.noise_map[np.nonzero(a.noise_map)])
            if np.isfinite(mean_thr):
                self.log.info('Mean threshold is %i [Delta VCAL]' % (int(mean_thr)))
            else:
                self.log.error('Mean threshold could not be determined!')

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return mean_thr, mean_noise


if __name__ == '__main__':
    with ThresholdScan(scan_config=scan_configuration) as scan:
        scan.scan()
        scan.analyze()
