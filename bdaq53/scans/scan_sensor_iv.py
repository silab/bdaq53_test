#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan records an IV curve of the sensor of a module
    while making sure the chip is powered and configured.
'''

import time
from tqdm import tqdm

import tables as tb

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import plotting


scan_configuration = {
    'module_name': 'module_0',
    'hv_current_limit': 1e-6,
    'VBIAS_start': 0,
    'VBIAS_stop': -5,
    'VBIAS_step': -1
}


class RawDataTable(tb.IsDescription):
    voltage = tb.Int32Col(pos=1)
    current = tb.Float64Col(pos=2)


class SensorIVScan(ScanBase):
    scan_id = 'sensor_iv_scan'

    # FIXME: There should be a way to run this scan once per module instead of once per chip
    # def __init__(self, scan_config={}):
    #     super(SensorIVScan, self).__init__(scan_config=scan_config, scan_config_override_per_scan=None, scan_scope='per_module')

    def _configure(self, module_name='module_0', **_):
        if not self.periphery.enabled:
            raise Exception('Periphery module needs to be enabled!')
        if not (module_name in list(self.periphery.module_devices.keys()) and 'HV' in self.periphery.module_devices[module_name].keys()):
            raise Exception('No sensor bias device defined for {}!'.format(module_name))

    def _scan(self, module_name='module_0', VBIAS_start=0, VBIAS_stop=-5, VBIAS_step=-1, hv_current_limit=1e-6, **_):
        '''
        Sensor IV scan main loop

        Parameters
        ----------
        VBIAS_start : int
            First bias voltage to scan
        VBIAS_stop : int
            Last bias voltage to scan. This value is included in the scan.
        VBIAS_step : int
            Stepsize to increase the bias voltage by in every step.
        '''

        self.h5_file.remove_node(self.h5_file.root, 'raw_data')
        self.raw_data_table = self.h5_file.create_table(self.h5_file.root, name='raw_data', title='Raw data', description=RawDataTable)

        self.periphery.power_off_HV(module_name)
        try:
            self.periphery.power_on_HV(module_name, hv_voltage=0, hv_current_limit=hv_current_limit)

            add = 1 if VBIAS_stop > 0 else -1
            pbar = tqdm(total=len(range(VBIAS_start, VBIAS_stop + add, VBIAS_step)), unit='Voltage step')

            for v_bias in range(VBIAS_start, VBIAS_stop + add, VBIAS_step):
                self.periphery.power_on_HV(module_name, hv_voltage=v_bias, hv_current_limit=hv_current_limit)
                time.sleep(1)
                current = float(self.periphery.module_devices[module_name]['HV'].get_current())
                row = self.raw_data_table.row
                row['voltage'] = v_bias
                row['current'] = current
                row.append()
                self.raw_data_table.flush()
                pbar.update(1)

                if abs(current) >= hv_current_limit * 0.99:
                    self.log.error('Current limit reached. Aborting scan!')
                    break

            pbar.close()
            self.log.success('Scan finished')
        except Exception as e:
            self.log.error('An error occurred: %s' % e)
        finally:
            self.periphery.power_off_HV(module_name)

    def _analyze(self):
        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
                p.create_standard_plots()


if __name__ == '__main__':
    with SensorIVScan(scan_config=scan_configuration) as scan:
        scan.scan()
        scan.analyze()
