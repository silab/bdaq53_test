#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import os
import time
import math
import numpy as np

from bdaq53.chips.chip_base import ChipBase, RegisterObject, MaskObject
from bdaq53.analysis import analysis_utils
from bdaq53.analysis import rd53b_analysis as anb
from bdaq53.system import logger as logger

FLAVOR_COLS = {'DIFF': range(0, 400)}


def get_flavor(col):
    for fe, cols in FLAVOR_COLS.items():
        if col in cols:
            return fe


def get_tdac_range(fe):
    return -15, 15, 31, 1


class RD53ACalibration(object):
    # Natural constants
    e = 1.6021766208e-19
    kb = 1.38064852e-23

    def __init__(self, calibration=None):
        if calibration is None:
            calibration = {}
        # Chip specific calibration constants
        try:
            self.e_conversion = {}
            self.e_conversion['slope'] = calibration['e_conversion'].get('slope', 10.4)
            self.e_conversion['slope_error'] = calibration['e_conversion'].get('slope_error', 0.10)
            self.e_conversion['offset'] = calibration['e_conversion'].get('offset', 180)
            self.e_conversion['offset_error'] = calibration['e_conversion'].get('offset_error', 60)
        except KeyError:
            self.e_conversion = {'slope': 10.4, 'slope_error': 0.10, 'offset': 180, 'offset_error': 60}

        self.ADC_a = calibration.get('ADC_a', 0.2)      # Conversion factors ADC counts - voltage: slope
        self.ADC_b = calibration.get('ADC_b', 15.5)     # Conversion factors ADC counts - voltage: offset

        self.t_sensors_calibrated = False
        Nf_global = calibration.get('Nf', 1.24)         # Chip specific conversion factors for on-chip T sensors: voltage - temperature
        if 'Nf_0' in calibration.keys():
            self.t_sensors_calibrated = True
        self.Nf_0 = calibration.get('Nf_0', Nf_global)  # Generally, there is one Nf per sensor. For a default fallback in case of
        self.Nf_1 = calibration.get('Nf_1', Nf_global)  # uncalibrated chips, all fall back to a global value.
        self.Nf_2 = calibration.get('Nf_2', Nf_global)
        self.Nf_3 = calibration.get('Nf_3', Nf_global)

    def return_all_values(self):
        values = {}
        values['e_conversion_slope'] = self.e_conversion['slope']
        values['e_conversion_slope_error'] = self.e_conversion['slope_error']
        values['e_conversion_offset'] = self.e_conversion['offset']
        values['e_conversion_offset_error'] = self.e_conversion['offset_error']
        values['ADC_a'] = self.ADC_a
        values['ADC_b'] = self.ADC_b
        values['Nf_0'] = self.Nf_0
        values['Nf_1'] = self.Nf_1
        values['Nf_2'] = self.Nf_2
        values['Nf_3'] = self.Nf_3

        return values

    def _dV_to_T(self, dV, sensor):
        Nf = eval('self.Nf_{0}'.format(sensor))
        return self.e / (Nf * self.kb * np.log(15)) * dV - 273.15

    def get_V_from_ADC(self, ADC, use_offset=True):
        if use_offset:
            return round(((self.ADC_a * ADC + self.ADC_b) / 1000), 3)
        else:
            return round(((self.ADC_a * ADC) / 1000), 3)

    def get_temperature_from_ADC(self, dADC, sensor):
        # ToDo: Calibrate diode temperature sensors
        return round((self._dV_to_T(self.get_V_from_ADC(dADC, use_offset=False), sensor=sensor)), 3)

    def _get_temperature_from_ADC_resistive_sensors(self, dADC, sensor):
        # ToDo: Calibrate resisitve temperature sensors
        return dADC

class ITkPixV1MaskObject(MaskObject):
    def remap_to_global(self, col, row):
        ''' Returns global address from column and row '''
        core_col = int(col / 8)
        core_row = int(row / 8)
        region = 2 * (row % 8) + int((col % 8) / 4)
        pixel_pair = int((col % 4) / 2)
        pixel = col % 2

        return (core_col, core_row, region, pixel_pair, pixel)

    def remap_to_local(self, core_col, core_row, region, pixel_pair, pixel):
        ''' Returns column and row from global address'''
        col = core_col * 8 + (region % 2) * 4 + pixel_pair * 2 + pixel
        row = core_row * 8 + int(region / 2)

        return (col, row)

    def remap_to_registers(self, core_col, core_row, region, pixel_pair):
        region_bin = '{0:04b}'.format(region)
        region_col = '{0:#06b}{1:s}{2:01b}'.format(core_col, region_bin[3], pixel_pair)
        region_row = '{0:#06b}{1:s}'.format(core_row, region_bin[:3])

        return (region_col, region_row)

    def get_pixel_data(self, column, row):
        ''' Return register value for one given pixel '''
        enable = str(int(self['enable'][column, row]))
        injection_enable = str(int(self['injection'][column, row]))
        hitbus_enable = str(int(self['hitbus'][column, row]))

        if self['tdac'][column, row] >= 0:
            tdac = str(bin(self['tdac'][column, row]))[2:].zfill(5)
        else:
            tdac = '1' + str(bin(abs(self['tdac'][column, row])))[2:].zfill(4)

        return tdac + hitbus_enable + injection_enable + enable

    def get_double_pixel_data(self, core_col, core_row, region, pixel_pair):
        ''' Return register value for given double pixel '''
        return int('0b' + self.get_pixel_data(*self.remap_to_local(core_col, core_row, region, pixel_pair, 1)) +
                   self.get_pixel_data(*self.remap_to_local(core_col, core_row, region, pixel_pair, 0)), 2)

    def reset_all(self):
        super(ITkPixV1MaskObject, self).reset_all()

    def update(self, force=False, broadcast=False):
        ''' Write the actual pixel register configuration

            Only write changes for speed up
        '''
        # if force and self.chip.bdaq.board_version == 'SIMULATION':
        #     return []
        sync_counter = 0
        self._find_changes()
        if broadcast:
            write_range = (0, 8, 1)
            broadcast_value = 4
        else:
            write_range = (168, 176, 2)
            broadcast_value = 0
        if force:
            write_mask = np.ones(self.dimensions, bool)
            self.chip.registers['PIX_DEFAULT_CONFIG'].set(0x9ce2)  # Write Magic numbers to disable constant reset
            self.chip.registers['PIX_DEFAULT_CONFIG_B'].set(0x631d)  # Write Magic numbers to disable constant reset
            self.chip.registers.write_all()
        else:   # Find out which pixels need to be updated
            write_mask = self.to_write
        to_write = np.column_stack((np.where(write_mask)))

        data = []
        indata = self.chip.write_sync(write=False)
        if len(to_write) < 10000:   # Write only double pixels which need to be updated
            self.chip.registers['PIX_MODE'].set(2)
            indata += self.chip.registers['PIX_MODE'].get_write_command(2)    # Disable broadcast, auto-col, auto_row

            indata += self.chip.write_sync_01(write=False)
            last_coords = (-1, -1, -1, -1, -1, -1)
            written = []
            for (col, row) in to_write:
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, row)
                (region_col, region_row) = self.remap_to_registers(core_col, core_row, region, pixel_pair)

                # Speedup block
                if (core_col, core_row, region, pixel_pair) in written:  # This pixel pair has already been updated
                    continue
                if col != last_coords[0]:  # Downshifting patterns always changes two pixels below each other
                    indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                if row != last_coords[1]:  # Sideshifting patterns always changes two pixels next to each other
                    indata += self.chip.registers['REGION_ROW'].get_write_command(int(region_row, 2))
                last_coords = (col, row, core_col, core_row, region, pixel_pair)
                written.append((core_col, core_row, region, pixel_pair))

                # Write pixel pair value
                indata += self.chip.registers['PIX_PORTAL'].get_write_command(self.get_double_pixel_data(core_col, core_row, region, pixel_pair))
                if sync_counter > 8:
                    indata += self.chip.write_sync(write=False)
                    sync_counter = 0
                sync_counter += 1
                if len(indata) > 4000:    # Write command to chip before it gets too long
                    self.chip.write_command(indata)
                    data.append(indata)
                    indata = self.chip.write_sync_01(write=False)

            self.chip.write_command(indata)
            data.append(indata)
        else:
            self.chip.registers['PIX_MODE'].set(3 + broadcast_value)
            indata += self.chip.registers['PIX_MODE'].get_write_command(3 + broadcast_value)    # Disable broadcast, enable auto-row
            indata += self.chip.write_sync(write=False)
            for col in range(write_range[0], write_range[1], write_range[2]):  # Loop over every second column, since two neighboring pixels are always written together
                # Set column address to next column pair
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, 0)
                (region_col, _) = self.remap_to_registers(core_col, core_row, region, pixel_pair)
                indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                # Set row to 0
                indata += self.chip.registers['REGION_ROW'].get_write_command(0)
                for row in range(384):  # Loop over every row, write double pixels
                    (_, core_row, region, _, _) = self.remap_to_global(col, row)
                    indata += self.chip.registers['PIX_PORTAL'].get_write_command(self.get_double_pixel_data(core_col, core_row, region, pixel_pair))
                    if sync_counter > 16:
                        indata += self.chip.write_sync(write=False)
                        sync_counter = 0
                    sync_counter += 1
                    if len(indata) > 4000:  # Write command to chip before it gets too long
                        self.chip.write_command(indata)
                        data.append(indata)
                        indata = self.chip.write_sync_01(write=False)
            self.chip.write_command(indata)
            data.append(indata)
        # Set this mask as last mask to be able to find changes in next update()
        for name, mask in self.items():
            self.was[name][:] = mask[:]
        return data

    def _update_bit_mode(self, force=False, write_TDAC=False):
        indata = self.chip.write_sync(write=False)
        if force:
            self.chip.registers['PIX_DEFAULT_CONFIG'].set(0x9ce2)  # Write Magic numbers to disable constant reset
            self.chip.registers['PIX_DEFAULT_CONFIG_B'].set(0x631d)  # Write Magic numbers to disable constant reset
            indata += self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0x9ce2)  # Write Magic numbers to disable constant reset
            indata += self.chip.registers['PIX_DEFAULT_CONFIG_B'].get_write_command(0x631d)  # Write Magic numbers to disable constant reset
        data = []
        pix_data = []
        for col in range(0, 400, 2):  # Loop over every second column, since two neighboring pixels are always written together
            indata += self.chip.write_sync(write=False)
            self.chip.registers['PIX_MODE'].set((int(write_TDAC) << 1) + 1)
            indata += self.chip.registers['PIX_MODE'].get_write_command((int(write_TDAC) << 1) + 1)  # Enable broadcast, enable auto-row
            indata += self.chip.registers['REGION_ROW'].get_write_command(0)
            (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, 0)
            (region_col, _) = self.remap_to_registers(core_col, core_row, region, pixel_pair)
            indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
            indata += self.chip.write_sync(write=False)
            for row in range(384):  # Loop over every row, write double pixels
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, row)
                full_pix_conf = self.get_double_pixel_data(core_col, core_row, region, pixel_pair)
                if not write_TDAC:
                    pix_bit = ((full_pix_conf & 0x700) >> 3) + (full_pix_conf & 0x7)
                else:
                    pix_bit = (((full_pix_conf >> 11) & 0x1f) << 5) + ((full_pix_conf >> 3) & 0x1f)
                pix_data += [pix_bit]
                if len(indata) + len(self.chip._write_pixels_fast(pix_data)) > 4000:  # Write command to chip before it gets too long
                    indata += self.chip._write_pixels_fast(pix_data)
                    self.chip.write_command(indata)
                    data.append(indata)
                    indata = self.chip.write_sync(write=False)
                    indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                    pix_data = []

            indata += self.chip._write_pixels_fast(pix_data)
            pix_data = []
            self.chip.write_command(indata)
            data.append(indata)
            indata = self.chip.write_sync(write=False)
        data.append(indata)

        # Set this mask as last mask to be able to find changes in next update()
        for name, mask in self.items():
            self.was[name][:] = mask[:]
        return data

    def _update_broadcast(self, force=False, write_TDAC=False):
        indata = self.chip.write_sync(write=False)
        if force:
            self.chip.registers['PIX_DEFAULT_CONFIG'].set(0x9ce2)  # Write Magic numbers to disable constant reset
            self.chip.registers['PIX_DEFAULT_CONFIG_B'].set(0x631d)  # Write Magic numbers to disable constant reset
            indata += self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0x9ce2)  # Write Magic numbers to disable constant reset
            indata += self.chip.registers['PIX_DEFAULT_CONFIG_B'].get_write_command(0x631d)  # Write Magic numbers to disable constant reset
        data = []
        pix_data = []
        for col in range(0, 8, 2):  # Loop over every second column, since two neighboring pixels are always written together
            indata += self.chip.write_sync(write=False)
            self.chip.registers['PIX_MODE'].set((int(write_TDAC) << 1) + 5)
            indata += self.chip.registers['PIX_MODE'].get_write_command((int(write_TDAC) << 1) + 5)  # Enable broadcast, enable auto-row
            indata += self.chip.registers['REGION_ROW'].get_write_command(0)
            (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, 0)
            (region_col, _) = self.remap_to_registers(core_col, core_row, region, pixel_pair)
            indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
            indata += self.chip.write_sync(write=False)
            for row in range(384):  # Loop over every row, write double pixels
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, row)
                full_pix_conf = self.get_double_pixel_data(core_col, core_row, region, pixel_pair)
                if not write_TDAC:
                    pix_bit = ((full_pix_conf & 0x700) >> 3) + (full_pix_conf & 0x7)
                else:
                    pix_bit = (((full_pix_conf >> 11) & 0x1f) << 5) + ((full_pix_conf >> 3) & 0x1f)
                pix_data += [pix_bit]
                if len(indata) + len(
                        self.chip._write_pixels_fast(pix_data)) > 4000:  # Write command to chip before it gets too long
                    indata += self.chip._write_pixels_fast(pix_data)
                    self.chip.write_command(indata)
                    data.append(indata)
                    indata = self.chip.write_sync(write=False)
                    indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                    pix_data = []

            indata += self.chip._write_pixels_fast(pix_data)
            pix_data = []
            self.chip.write_command(indata)
            data.append(indata)
            indata = self.chip.write_sync(write=False)
        data.append(indata)

        # Set this mask as last mask to be able to find changes in next update()
        for name, mask in self.items():
            self.was[name][:] = mask[:]
        return data

    def apply_good_pixel_mask_diff(self):
        pass

    def load_logo_mask(self, masks=['injection']):
        from pylab import imread
        img = np.invert(imread(os.path.join(os.path.dirname(__file__), 'RD53b_logo_400_384_full.bmp'))).astype('bool')
        img_mask = np.transpose(img[:, :, 0])
        img_mask[:, 0] = False
        img_mask[:, -1] = False

        for name in masks:
            self[name] = img_mask
        return img_mask


class ITkPixV1(ChipBase):
    '''
    Main class for ITkPixV1 chip
    '''

    cmd_data_map = {
        0: 0b01101010,
        1: 0b01101100,
        2: 0b01110001,
        3: 0b01110010,
        4: 0b01110100,
        5: 0b10001011,
        6: 0b10001101,
        7: 0b10001110,
        8: 0b10010011,
        9: 0b10010101,
        10: 0b10010110,
        11: 0b10011001,
        12: 0b10011010,
        13: 0b10011100,
        14: 0b10100011,
        15: 0b10100101,
        16: 0b10100110,
        17: 0b10101001,
        18: 0b01011001,
        19: 0b10101100,
        20: 0b10110001,
        21: 0b10110010,
        22: 0b10110100,
        23: 0b11000011,
        24: 0b11000101,
        25: 0b11000110,
        26: 0b11001001,
        27: 0b11001010,
        28: 0b11001100,
        29: 0b11010001,
        30: 0b11010010,
        31: 0b11010100
    }

    trigger_map = {
        0: 0b00101011,
        1: 0b00101011,
        2: 0b00101101,
        3: 0b00101110,
        4: 0b00110011,
        5: 0b00110101,
        6: 0b00110110,
        7: 0b00111001,
        8: 0b00111010,
        9: 0b00111100,
        10: 0b01001011,
        11: 0b01001101,
        12: 0b01001110,
        13: 0b01010011,
        14: 0b01010101,
        15: 0b01010110
    }
    CMD_SYNC = [0b10000001, 0b01111110]  # 0x(817E)
    CMD_PLL_LOCK = [0b01010101, 0b01010101]
    # CMD_TRIGGER = trigger_map[x]
    CMD_READ_TRIGGER = 0b01101001
    CMD_CLEAR = 0b01011010
    CMD_GLOBAL_PULSE = 0b01011100
    CMD_CAL = 0b01100011
    CMD_REGISTER = 0b01100110
    CMD_RDREG = 0b01100101
    CMD_NULL = 0b01101001

    macro_regs = [{'columns': range(0, 128),
                   'macro_columns': range(0, 16),
                   'macro_name': 'EnCoreColumnCalibration_0'},

                  {'columns': range(128, 256),
                   'macro_columns': range(16, 32),
                   'macro_name': 'EnCoreColumnCalibration_1'},

                  {'columns': range(256, 384),
                   'macro_columns': range(32, 48),
                   'macro_name': 'EnCoreColumnCalibration_2'},

                  {'columns': range(384, 400),
                   'macro_columns': range(48, 50),
                   'macro_name': 'EnCoreColumnCalibration_3'}]

    core_regs = [{'columns': range(0, 128),
                  'core_columns': range(0, 16),
                  'core_name': 'EnCoreCol_0'},

                 {'columns': range(128, 256),
                  'core_columns': range(16, 32),
                  'core_name': 'EnCoreCol_1'},

                 {'columns': range(256, 384),
                  'core_columns': range(32, 48),
                  'core_name': 'EnCoreCol_2'},

                 {'columns': range(384, 432),
                  'core_columns': range(48, 54),
                  'core_name': 'EnCoreCol_3'}]

    default_dac_values = {'DAC_PREAMP_L': 50,
                          'DAC_PREAMP_R': 51,
                          'DAC_PREAMP_TL': 52,
                          'DAC_PREAMP_TR': 53,
                          'DAC_PREAMP_T': 54,
                          'DAC_PREAMP_M': 55,
                          'DAC_PRECOMP': 56,
                          'DAC_COMP': 57,
                          'DAC_VFF': 100,
                          'DAC_TH1_L': 100,
                          'DAC_TH1_R': 100,
                          'DAC_TH1_M': 100,
                          'DAC_TH2': 0,
                          'DAC_LCC': 100,
                          'LEACKAGE_FEEDBACK': 0}

    voltage_mux = {'VREF_ADC': 0,
                   'I_MUX': 1,
                   'NTC_PAD': 2,
                   'VREF_VDAC': 3,
                   'VDDA_HALF_CAP': 4,
                   'TEMPSENS_T': 5,
                   'TEMPSENS_B': 6,
                   'VCAL_HI': 7,
                   'VCAL_MED': 8,
                   'DAC_TH2': 9,
                   'DAC_TH1_M': 10,
                   'DAC_TH1_L': 11,
                   'DAC_TH1_R': 12,
                   'RADSENS_A': 13,
                   'TEMPSENS_A': 14,
                   'RADSENS_D': 15,
                   'TEMPSENS_D': 16,
                   'RADSENS_C': 17,
                   'TEMPSENS_C': 18,
                   'GNDA19': 19,
                   'GNDA20': 20,
                   'GNDA21': 21,
                   'GNDA22': 22,
                   'GNDA23': 23,
                   'GNDA24': 24,
                   'GNDA25': 25,
                   'GNDA26': 26,
                   'GNDA27': 27,
                   'GNDA28': 28,
                   'GNDA29': 29,
                   'GNDA30': 30,
                   'VREF_CORE': 31,
                   'VREF_PRE': 32,
                   'VINA_HALF': 33,
                   'VDDA_HALF': 34,
                   'VREFA': 35,
                   'VOFS_HALF': 36,
                   'VIND_HALF': 37,
                   'VDDD_HALF': 38,
                   'VREFD': 39}

    current_mux = {'IREF': 0,
                   'CDR_VCO_MAIN': 1,
                   'CDR_VCO_BUF': 2,
                   'CDR_CP': 3,
                   'CDR_FD': 4,
                   'CDR_BUF': 5,
                   'CML_TAP2': 6,
                   'CML_TAP1': 7,
                   'CML_MAIN': 8,
                   'NTC_PAD': 9,
                   'CAP': 10,
                   'CAP_PARASIT': 11,
                   'PREAMP_MATRIX_MAIN': 12,
                   'DAC_PRECOMP': 13,
                   'DAC_COMP': 14,
                   'DAC_TH2': 15,
                   'DAC_TH1_M': 16,
                   'DAC_LCC': 17,
                   'DAC_FB': 18,
                   'DAC_PREAMP_L': 19,
                   'DAC_TH1_L': 20,
                   'DAC_PREAMP_R': 21,
                   'DAC_PREAMP_TL': 22,
                   'DAC_TH1_R': 23,
                   'DAC_PREAMP_T': 24,
                   'DAC_PREAMP_TR': 25,
                   'IINA': 28,
                   'I_SHUNT_A': 29,
                   'IIND': 30,
                   'I_SHUNT_D': 31}

    flavor_cols = FLAVOR_COLS

    def __init__(self, bdaq, chip_sn='0x0000', chip_id=0, receiver='rx0', config=None):
        self.log = logger.setup_derived_logger('ITkPixV1 - ' + chip_sn)
        self.bdaq = bdaq
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if chip_id not in range(16):
            raise ValueError("Invalid Chip ID %r, use integer 0-15" % chip_id)

        self.chip_type = 'ITKPixV1'
        self.chip_sn = chip_sn
        self.chip_id = chip_id
        self.receiver = receiver

        if config is None or len(config) == 0:
            self.log.warning("No explicit configuration supplied. Using 'ITkPixV1_default.cfg.yaml'!")
            with open(os.path.join(os.path.dirname(__file__), 'chips', 'ITkPixV1_default.cfg.yaml'), 'r') as f:
                self.configuration = yaml.full_load(f)
        elif isinstance(config, dict):
            self.configuration = config
        elif isinstance(config, str) and os.path.isfile(config):
            with open(config) as f:
                self.configuration = yaml.full_load(f)
        else:
            raise TypeError('Supplied config has unknown format!')

        self.registers = RegisterObject(self, 'ITkPixV1_registers.yaml')

        masks = {'enable': {'default': False},
                 'injection': {'default': False},
                 'hitbus': {'default': False},
                 'tdac': {'default': 0},
                 'lin_gain_sel': {'default': True},
                 'injection_delay': {'default': 351}}
        self.masks = ITkPixV1MaskObject(self, masks, (400, 384))

        # Load disabled pixels from chip config
        if 'disable' in self.configuration.keys():
            for pix in self.configuration['disable']:
                self.masks.disable_mask[pix[0], pix[1]] = False

        self.calibration = RD53ACalibration(self.configuration['calibration'])

    def init(self):
        if self.bdaq.board_version != 'SIMULATION':
            self.write_trimbits()   # Try to write trimbits for better communication
            self.init_communication()
            self.write_trimbits(verify=True)  # Make sure that trimbits are written
            self.reset()
            if self.bdaq.enable_NTC:
                self.get_temperature_NTC()
            else:
                self.get_temperature_sensors()
        else:
            self.init_communication()

    def get_sn(self):
        return self.chip_sn

    def write_trimbits(self, verify=False):
        '''
        Write trimbits for all reference voltages to the chip, if they are supplied in the chip config.
        '''

        if 'trim' not in self.configuration:
            return

        vref_a_trim = self.configuration['trim'].get('VREF_A_TRIM', 16)
        vref_d_trim = self.configuration['trim'].get('VREF_D_TRIM', 16)
        self.registers['VOLTAGE_TRIM'].write(int('00' + '{0:04b}'.format(vref_a_trim) + '{0:04b}'.format(vref_d_trim), 2), verify=verify)

        mon_adc_trim = self.configuration['trim'].get('MON_ADC', 0)
        self.registers['MON_ADC'].write(int('{0:03b}'.format(1) + '{0:06b}'.format(mon_adc_trim), 2), verify=verify)

    def setup_aurora(self, tx_lanes=1, CB_Wait=255, CB_Send=1, only_cb=False, bypass_mode=False, **_):
        self.write_sync(write=False) * 32
        if bypass_mode:
            self.log.info("Switching chip to BYPASS MODE is not yet implemented and will most likely fail.")
            self.registers['CdrConf'].set(int('0b'))

        elif self.bdaq.board_options & self.bdaq.board_options_map['640Mbps']:
            self.log.info("Chip is running at 640Mb/s")
            self.registers['CdrConf'].set(1)
        else:
            self.log.info("Chip is running at 1.28Gb/s")
        if only_cb is False:
            self.log.debug("Aurora transmitter settings: Lanes=%u, CB_Wait=%u, CB_Send=%u", tx_lanes, CB_Wait, CB_Send)
            if tx_lanes == 4:
                self.log.info("4 Aurora lanes active")
                # Sets 4-lane-mode
                self.registers['AuroraConfig'].set(0b0111101100111)
                # Enable 4 CML outputs
                self.registers['CML_CONFIG'].set(0b00001111)
            elif tx_lanes == 3:
                self.log.info("3 Aurora lanes active")
                # Sets 3-lane-mode
                self.registers['AuroraConfig'].set(0b0011101100111)
                # Enable 2 CML outputs
                self.registers['CML_CONFIG'].set(0b00000111)
            elif tx_lanes == 2:
                self.log.info("2 Aurora lanes active")
                # Sets 2-lane-mode
                self.registers['AuroraConfig'].set(0b0001101100111)
                # Enable 2 CML outputs
                self.registers['CML_CONFIG'].set(0b00000011)
            elif tx_lanes == 1:
                self.log.info("1 Aurora lane active")
                # Sets 1-lane-mode
                self.registers['AuroraConfig'].set(0b0000101100111)
                # Enable 1 CML outputs
                self.registers['CML_CONFIG'].set(0b00000001)
            else:
                self.log.error("Aurora lane configuration (1,2,3,4) must be specified")
        else:
            self.log.debug("Aurora settings: CB_Wait=%u, CB_Send=%u", CB_Wait, CB_Send)

        # Set CB frame distance and number
        self.registers['AURORA_CB_CONFIG0'].set(((CB_Wait << 4) | CB_Send & 0xf) & 0xffff)
        self.registers['AURORA_CB_CONFIG1'].set((CB_Wait >> 12) & 0xff)  # Set CB frame distance and number
        self.registers.write_all()

        # Reset Aurora and serializers
        self.send_global_pulse(bitnames=['reset_serializer'], pulse_width=0xff)
        self.write_command(self.write_sync(write=True) * 16)
        self.send_global_pulse(bitnames=['reset_aurora'], pulse_width=0xff)

    def init_communication(self):
        self.log.info('Initializing communication...')
        # Configure cmd encoder
        self.bdaq['cmd'].reset()
        self.bdaq.set_chip_type_ITkPixV1()
        self._write_reset()
        self.write_command(self.write_sync(write=False) * 32)
        self.write_ecr()
        self.registers['GCR_DEFAULT_CONFIG'].write(0xac75)  # Write Magic numbers to disable constant reset
        self.registers['GCR_DEFAULT_CONFIG_B'].write(0x538a)  # Write Magic numbers to disable constant reset
        # Switch chip to 1.28 GHZ & disable chip_id
        self.registers['DataMerging'].write(0b000001000000)
        self.registers['RingOscConfig'].write(0x7fff)
        self.registers['RingOscConfig'].write(0x5eff)
        # # Wait for PLL lock
        self.bdaq.wait_for_pll_lock()
        self.setup_aurora(tx_lanes=self.bdaq.rx_lanes[self.receiver])

        # Workaround for locking problems
        for _ in range(30):
            self.bdaq.set_chip_type_ITkPixV1()
            self.write_command(self.write_sync(write=False) * 32)
            self.write_ecr()
            try:
                self.bdaq.wait_for_aurora_sync()
            except RuntimeError:
                pass
            else:
                break

            time.sleep(0.01)
        else:
            self.bdaq.wait_for_aurora_sync()
        self.log.success('Communication established')

    def write_global_pulse(self, width, write=True):
        # 0101_1100    ChipId<3:0>,0    Width<3:0>,0
        indata = self.registers['GlobalPulseWidth'].get_write_command(width)
        indata += [self.CMD_GLOBAL_PULSE]
        indata += [self.cmd_data_map[self.chip_id]]
        if write:
            self.write_command(indata)

        return indata

    def write_cal(self, cal_edge_mode=0, cal_edge_width=10, cal_edge_dly=2, cal_aux_value=0, cal_aux_dly=0, write=True):
        '''
            Command to send a digital or analog injection to the chip.
            Digital or analog injection is selected globally via the INJECTION_SELECT register.

            For digital injection, only CAL_edge signal is relevant:
                - CAL_edge_mode switches between step (0) and pulse (1) mode
                - CAL_edge_dly is counted in bunch crossings. It sets the delay before the rising edge of the signal
                - CAL_edge_width is the duration of the pulse (only in pulse mode) and is counted in cycles of the 160MHz clock
            For analog injection, the CAL_aux signal is used as well:
                - CAL_aux_value is the value of the CAL_aux signal
                - CAL_aux_dly is counted in cycles of the 160MHz clock and sets the delay before the edge of the signal

            {ChipId[3:0],CalEdgeMode, CalEdgeDelay[2:0],CalEdgeWidth[5:4]}{CalEdgeWidth[3:0],CalAuxMode, CalAuxDly[4:0]}
        '''
        indata = [self.CMD_CAL]
        indata += [self.cmd_data_map[self.chip_id]]
        indata += [self.cmd_data_map[((cal_edge_mode << 4) + ((cal_edge_dly & 0b11110) >> 1))]]  # mode & e-delay
        indata += [self.cmd_data_map[((cal_edge_dly & 0b00001) << 4) + ((cal_edge_width & 0b11110000) >> 4)]]  # Eduration
        indata += [self.cmd_data_map[(((cal_edge_width & 0b00001111) << 1) + cal_aux_value)]]  # cal_aux
        indata += [self.cmd_data_map[(cal_aux_dly)]]  # A-delay

        if write:
            self.write_command(indata)

        return indata

    def _read_register(self, address, write=True):
        indata = [self.CMD_RDREG]
        indata += [self.cmd_data_map[self.chip_id]]
        indata += [self.cmd_data_map[address >> 5]]  # first 4 bits of address
        indata += [self.cmd_data_map[address & 0x1f]]  # last 5 bits of address

        if write:
            self.write_command(indata)
        return indata

    def _get_register_value(self, address, timeout=1000, tries=10):
        self.enable_monitor_data()
        self.send_global_pulse('reset_monitor_data', pulse_width=4)
        for _ in range(tries):
            self._read_register(address)
            self.write_command(self.write_sync(write=False) * 10)
            for _ in range(timeout):
                if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                    data = self.bdaq['FIFO'].get_data()
                    userk_data = analysis_utils.process_userk(anb.interpret_userk_data(data))
                    if len(userk_data) == 0:
                        continue
                    index_i = np.where(userk_data['Address'] == address)[0]
                    if len(index_i) == 0:
                        continue
                    return userk_data['Data'][index_i][0]
                self.write_command(self.write_sync(write=False) * 10)
            else:
                self.log.warning('Timeout while waiting for register response.')
        else:
            raise RuntimeError('Timeout while waiting for register response.')

    def _write_register(self, address, data, write=True):
        '''
            Sends write command to register with data

            Parameters:
            ----------
                address : int
                    Address of the register to be written to

            Returns:
            ----------
                indata : binarray
                    Boolean representation of register write command.
        '''
        indata = [self.CMD_REGISTER]  # Write Command
        indata += [self.cmd_data_map[self.chip_id]]
        indata += [self.cmd_data_map[address >> 5]]  # Write mode + first 4 bits of address
        indata += [self.cmd_data_map[address & 0x1f]]  # Last 5 bits of address
        indata += [self.cmd_data_map[data >> 11]]  # First 5 bits of data
        indata += [self.cmd_data_map[(data >> 6) & 0x1f]]  # Middle 5 bits of data
        indata += [self.cmd_data_map[(data >> 1) & 0x1f]]  # Middle 5 bits of data
        indata += [self.cmd_data_map[(data & 0x1) << 4]]  # Last bit of data
        if write:
            self.write_command(indata)

        return indata

    def _write_pixels_fast(self, data, write=False):
        '''
            Sends write command to register with data

            Parameters:
            ----------
                address : int
                    Address of the register to be written to

            Returns:
            ----------
                indata : binarray
                    Boolean representation of register write command.
        '''
        sync_counter = 0
        indata = [self.CMD_REGISTER]  # Write Command
        indata += [self.cmd_data_map[self.chip_id]]
        indata += [self.cmd_data_map[(0 >> 5) + 16]]  # Write mode + first 4 bits of address
        indata += [self.cmd_data_map[0 & 0x1f]]  # Last 5 bits of address
        for data_words in data:
            indata += [self.cmd_data_map[data_words >> 5]]  # First 5 bits of data
            indata += [self.cmd_data_map[(data_words) & 0x1f]]  # Last 5 bits of data
            if sync_counter > 32:
                indata += self.write_sync(write=False)
                sync_counter = 0
            sync_counter += 1
        if write:
            self.write_command(indata)
        return indata

    def write_null(self, write=True):
        indata = [self.CMD_NULL] * 2  # [0b01101001]
        if write:
            self.write_command(indata)
        return indata

    def write_ecr(self, write=True):
        indata = [self.CMD_CLEAR]
        indata += [self.cmd_data_map[self.chip_id]]
        if write:
            self.write_command(indata)
        return indata

    def write_bcr(self, write=True):
        self.log("Write BCR not yet implemented")
        return 0

    def write_sync(self, write=True):
        indat = [0b10000001, 0b01111110]
        if write:
            self.write_command(indat)
        return indat

    def write_sync_01(self, write=True):
        indata = [0b10101010]
        indata += [0b10101010]
        if write:
            self.write_command(indata)
        return indata

    def _write_reset(self, write=True):
        indata = [0xff] * 10
        indata += [0x00] * 10
        if write:
            self.write_command(indata, repetitions=30)
        return indata

    def send_trigger(self, trigger, tag=0, write=True):
        # Trigger is always followed by 5 Data bits
        indata = [self.trigger_map[trigger]]
        indata += [self.cmd_data_map[tag]]
        if write:
            self.write_command(indata)
        return indata

    def _send_trigger_read(self, trigger_tag, write=True):
        # Trigger is always followed by 5 Data bits
        indata = [self.CMD_READ_TRIGGER]
        indata += [self.cmd_data_map[trigger_tag]]
        if write:
            self.write_command(indata)
        return indata

    def send_trigger_tag(self, trigger, trigger_tag, write=True):
        if trigger == 0:
            self.log.error("Illegal trigger number")
            return
        else:
            indata = [self.trigger_map[trigger]]
            indata += [self.cmd_data_map[trigger_tag]]
            if write:
                self.write_command(indata)
            return indata

    def write_command(self, data, repetitions=1, wait_for_done=True, wait_for_ready=True):
        '''
            Write data to the command encoder.

            Parameters:
            ----------
                data : list
                    Up to [get_cmd_size()] bytes
                repetitions : integer
                    Sets repetitions of the current request. 1...2^16-1. Default value = 1.
                wait_for_done : boolean
                    Wait for completion after sending the command. Not advisable in case of repetition mode.
                wait_for_ready : boolean
                    Wait for completion of preceding commands before sending the command.
        '''
        if isinstance(data[0], list):
            for indata in data:
                self.write_command(indata, repetitions, wait_for_done)
            return

        assert (0 < repetitions < 65536), "Repetition value must be 0<n<2^16"
        if repetitions > 1:
            self.log.debug("Repeating command %i times." % (repetitions))

        if wait_for_ready:
            while (not self.bdaq['cmd'].is_done()):
                pass

        self.bdaq['cmd'].set_data(data)
        self.bdaq['cmd'].set_size(len(data))
        self.bdaq['cmd'].set_repetitions(repetitions)
        self.bdaq['cmd'].start()
        # self.command_counter += (len(data) * repetitions)
        # self.log.info("Sent Commands: %i ", self.command_counter)
        if wait_for_done:
            while (not self.bdaq['cmd'].is_done()):
                pass

    def send_global_pulse(self, bitnames, pulse_width, global_pulse_route_file=None):
        '''
            Used to set a single or multiple bits of GLOBAL_PULSE_ROUTE register by name, using names defined in rd53a_global_pulse_route.yaml

            Parameters:
            ----------
                bitnames : str or list of str
                    Name(s) of the bit(s) to be set to 1
        '''

        if not global_pulse_route_file:
            global_pulse_route_file = os.path.join(self.proj_dir, 'chips' + os.sep + 'ITkPixV1_global_pulse_route.yaml')

        with open(global_pulse_route_file, 'r') as infile:
            global_pulse_route_map = yaml.full_load(infile)

        if type(bitnames) == str:
            bitnames = [bitnames]

        listofnames = [bit['name'] for bit in global_pulse_route_map]

        for bitname in bitnames:
            if bitname not in listofnames:
                self.log.warning('Could not find %s in possible values for register GLOBAL_PULSE_ROUTE' % bitname)

        val = ''
        for i in range(16):
            if global_pulse_route_map[i]['name'] in bitnames:
                val += '1'
            else:
                val += '0'

        val = '0b' + val[::-1]
        self.registers['GlobalPulseConf'].write(int(val, 2))
        self.write_global_pulse(width=pulse_width)

    def reset(self):
        self._write_reset(write=True)
        if self.bdaq.board_version == 'SIMULATION':
            return

        #  Set all registers to default
        self.registers.reset_all()

        self.masks.reset_all()  # Set all masks to default and
        self.masks.update(force=True)   # write all masks to chip

        self.enable_core_col_clock(range(50))  # Enable clock on full chip
        self.enable_macro_col_cal(range(200))  # Enable analog calibration on full chip

    def generate_trigger_command(self, trigger_pattern):
        pattern = str(bin(trigger_pattern))[2:]
        cmd = []
        for tag, i in enumerate(range(math.ceil(len(pattern) / 4))):
            pat = pattern[i * 4:i * 4 + 4]
            cmd += self.send_trigger_tag(int(pat, 2), trigger_tag=tag, write=False)
        return cmd

    def setup_digital_injection(self, fine_delay=9):
        indata = self.write_sync(write=False) * 10
        self.registers['CalibrationConfig'].set(int('0b10' + format(fine_delay, '06b'), 2))  # Enable digital injection with zero delay
        indata += self.registers['CalibrationConfig'].get_write_command(int('0b10' + format(fine_delay, '06b'), 2))  # Enable digital injection with zero delay
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=4, cal_edge_dly=2, write=False)  # Injection
        indata += self.write_sync(write=False) * 10
        self.write_command(indata)

    def inject_digital(self, cal_edge_width=4, cal_edge_dly=2, latency=124, wait_cycles=400, trigger_pattern=0xffffffff, repetitions=1):
        '''
            Injects a digital pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
                repetitions : int
                    Number of times, the injection command is repeated, i.e. number of injections
        '''
        indata = self.write_sync(write=False) * 10
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=cal_edge_width, cal_edge_dly=cal_edge_dly, write=False)  # Injection
        indata += self.write_sync(write=False) * latency  # Wait for latency
        indata += self.generate_trigger_command(trigger_pattern)
        if self.bdaq.board_version != 'SIMULATION':
            indata += self.write_sync(write=False) * wait_cycles  # Wait for data
        self.write_command(indata, repetitions=repetitions)
        return indata

    def setup_analog_injection(self, vcal_high, vcal_med, fine_delay=9, injection_mode=0):
        indata = self.write_sync(write=False) * 10

        self.registers['CalibrationConfig'].set(int('0b0' + format(injection_mode, '01b') + format(fine_delay, '06b'), 2))  # Enable analog injection in uniform mode with configured fine delay
        self.registers['VCAL_HIGH'].set(vcal_high)  # Set VCAL_HIGH
        self.registers['VCAL_MED'].set(vcal_med)  # Set VCAL_MED
        indata += self.registers['CalibrationConfig'].get_write_command(int('0b0' + format(injection_mode, '01b') + format(fine_delay, '06b'), 2))  # Enable analog injection in uniform mode with configured fine delay
        indata += self.registers['VCAL_HIGH'].get_write_command(vcal_high)  # Set VCAL_HIGH
        indata += self.registers['VCAL_MED'].get_write_command(vcal_med)  # Set VCAL_MED

        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_aux_value=0, cal_edge_dly=0, write=False)  # CalEdge -> 0
        indata += self.write_sync(write=False) * 10
        indata += self.write_cal(cal_edge_mode=0, cal_edge_width=1, cal_edge_dly=0, cal_aux_value=0, write=False)  # CalEdge -> 1 (inject)
        indata += self.write_sync(write=False) * 124  # Wait for latency
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, cal_aux_value=0, write=False)  # CalEdge -> 0
        self.write_command(indata)

    def revive(self):
        pass

    def inject_analog_single(self, repetitions=1, latency=124, wait_cycles=400, send_ecr=False, trigger_pattern=0xffffffff, send_trigger=True, write=True):
        '''
            Injects a single analog pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger
        '''

        indata = self.write_sync(write=False)
        indata += self.write_cal(cal_edge_mode=0, cal_edge_width=1, cal_edge_dly=0, cal_aux_value=0, write=False)  # CalEdge -> 1 (inject)
        indata += self.write_sync(write=False) * latency  # Wait for latency
        if send_trigger:
            indata += self.generate_trigger_command(trigger_pattern)
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, cal_aux_value=0, write=False)  # CalEdge -> 0
        if self.bdaq.board_version != 'SIMULATION':
            indata += self.write_sync(write=False) * wait_cycles  # Wait for data
        if write:
            self.write_command(indata, repetitions=repetitions)
        return indata

    def inject_analog_double(self, repetitions=1, latency=124, wait_cycles=400, write=True):
        '''
            Injects a two consecutive analog pulses in a short time in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger
        '''

        indata = self.write_sync(write=False)
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=30, cal_edge_dly=16, cal_aux_value=1, cal_aux_dly=30, write=False)  # CalEdge -> 1 (inject) 25-->19
        indata += self.write_sync(write=False) * latency  # Wait for latency
        for tag in range(8):
            indata += self.send_trigger_tag(trigger=0b1111, trigger_tag=tag, write=False)
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, cal_aux_value=0, write=False)  # CalEdge -> 0
        if self.bdaq.board_version != 'SIMULATION':
            indata += self.write_sync(write=False) * wait_cycles  # Wait for data

        if write:
            self.write_command(indata, repetitions=repetitions)
        return indata

    def toggle_output_select(self, repetitions=1, latency=122, wait_cycles=400, fine_delay=9, send_clear=False):
        '''
            Toggles between digital and analog injection. If the comparator is stuck high
            this creates a transient on hit_t and thus a hit.

            Note
            ----
            Chip implementation:
            assign hit_t = EnDigHit ? (CalEdge & cal_en) : hit ;
            assign HitOut = hit_t & hit_en;

            See also Figure 29 in RD53 manual.

            Parameters:
            ----------
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
        '''

        indata = self.write_sync_01(write=False)

        if send_clear:
            indata += self.write_global_pulse(width=8, write=False)
            indata += self.write_ecr(write=False)
            indata += self.write_ecr(write=False)
            indata += self.write_sync_01(write=False) * 60
        # Enable digital injection = falling edge for stuck high pixels
        indata = self.registers['CalibrationConfig'].get_write_command(0x80 + fine_delay)
        indata += self.write_sync_01(write=False) * 10
        # Enable analog injection, this creates a rising edge if comparator output is stuck high
        indata += self.registers['CalibrationConfig'].get_write_command(fine_delay)
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.generate_trigger_command(0xffffffff)
        indata += self.write_sync_01(write=False) * wait_cycles
        self.write_command(indata, repetitions=repetitions)

    def enable_core_col_clock(self, core_cols=None, write=True):
        '''
            Enable clocking of given core columns. After POR everything is disabled.

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        # Enable everything
        indata = self.write_sync_01(write=False)
        indata += self.registers['EnCoreCol_0'].get_write_command(0xffff)
        indata += self.registers['EnCoreCol_1'].get_write_command(0xffff)
        indata += self.registers['EnCoreCol_2'].get_write_command(0xffff)
        indata += self.registers['EnCoreCol_3'].get_write_command(0b111111)

        if core_cols:
            indata += self.write_sync_01(write=False)
            for r in self.core_regs:
                bits = []
                for i, core_col in enumerate(r['core_columns']):
                    if core_col in core_cols:
                        bits.append(i)

                bits = list(set(bits))  # Remove duplicates

                data = ''
                for i in range(len(r['core_columns'])):
                    data += '1' if i in bits else '0'
                indata += self.registers[r['core_name']].get_write_command(int(data[::-1], 2))

        if write:
            self.write_command(indata)
            self.write_ecr()

        return indata

    def enable_macro_col_cal(self, macro_cols=None):
        '''
            Enable analog calibration of given macro (double-) columns. After POR everything is enabled.

            ----------
            Parameters:
                macro_cols : list of int
                    A list of macro columns to enable. Default = None disables everything
        '''

        # Enable everything
        indata = self.write_sync_01(write=False)
        indata += self.registers['EnCoreColumnCalibration_0'].get_write_command(0xffff)
        indata += self.registers['EnCoreColumnCalibration_1'].get_write_command(0xffff)
        indata += self.registers['EnCoreColumnCalibration_2'].get_write_command(0xffff)
        indata += self.registers['EnCoreColumnCalibration_3'].get_write_command(0b111111)

        if macro_cols:
            indata = self.write_sync_01(write=False)
            for r in self.macro_regs:
                bits = []
                for i, core_col in enumerate(r['macro_columns']):
                    if core_col in macro_cols:
                        bits.append(i)

                bits = list(set(bits))  # Remove duplicates

                data = ''
                for i in range(len(r['macro_columns'])):
                    data += '1' if i in bits else '0'

                indata += self.registers[r['macro_name']].get_write_command(int(data[::-1], 2))

        self.write_command(indata)
        self.write_ecr()

    def enable_monitor_data(self):
        self.bdaq.set_monitor_filter(receivers=self.receiver, mode='filter')
        self.registers['ServiceDataConf'].set(int('0b100110010', 2))
        self.registers['ServiceDataConf'].write(int('0b100110010', 2))
        self.log.debug('Monitor data enabled')

    def disable_monitor_data(self):
        self.registers['ServiceDataConf'].set(int('0b000110010', 2))
        self.registers['ServiceDataConf'].write(int('0b000110010', 2))
        self.bdaq.set_monitor_filter(receivers=self.receiver, mode='block')
        logger.debug('Monitor data disabled')

    def get_ADC_value(self, name):
        '''
        Sends multiplexer settings and reads ADC measurement
        Output is in ADC LSB.

        Parameters:
        ----------
            name: str
                The name of the ADC register to be measured

        Returns:
        ----------
            ADC value
        '''
        mux_type = None

        if name in self.current_mux.keys():
            mux_type = 'current'
            address = self.current_mux[name]
            bitstring = int('1' + format(address, '06b') + format(11, '06b'), 2)
        elif name in self.voltage_mux.keys():
            mux_type = 'voltage'
            address = self.voltage_mux[name]
            bitstring = int('1' + format(32, '06b') + format(address, '06b'), 2)
        else:
            raise ValueError('Invalid address: %s (%s)' % (name, type(name)))

        self.send_global_pulse(bitnames='reset_monitor_data', pulse_width=8)  # Reset Monitor Data
        self.send_global_pulse(bitnames='reset_adc', pulse_width=8)  # Reset ADC
        self.registers['MonitorConfig'].write(bitstring)  # Select MUX before all were 4
        self.send_global_pulse(bitnames='adc_start_conversion', pulse_width=8)  # Start ADC

        val_adc = self.registers['MonitoringDataADC'].read()    # Read out ADC value
        if mux_type == 'voltage':
            val_real = self.calibration.get_V_from_ADC(val_adc)
        elif mux_type == 'current':
            # FIXME: To be calibrated
            val_real = None
        return val_adc, val_real

    def get_chip_status(self, timeout=10000):
        '''
            Returns a map of all important chip parameters.
            Can only be called before or after a scan!
        '''

        voltages = {}
        currents = {}

        self.log.info('Recording chip status...')
        try:
            for vmonitor in self.voltage_mux.keys():
                voltages[vmonitor] = self.get_ADC_value(vmonitor)

            for cmonitor in self.current_mux.keys():
                currents[cmonitor] = self.get_ADC_value(cmonitor)
        except RuntimeError as e:
            self.log.error('There was an error while receiving the chip status: %s' % e)

        return voltages, currents

    def get_ring_oscillators(self, pulse_width=8):
        '''
            Returns data of ring oscillators:
            {
                RING_OSC_i: {
                    raw_data: Raw register data, consisting of a cycle counter and the actual counter,
                    counter: Value of the actual counter (last 12 bits of register value),
                    frequency: Counter value devided by pulse length. In Hz.
                }
            }
        '''

        oscillators = {}
        bank = 'A'
        for i in range(8):
            osc_data = {}
            raw_data = self._get_single_ring_oscillator(bank=bank, number=i, pulse_width=pulse_width)
            counter = int('{0:016b}'.format(raw_data)[-12:], 2)
            frequency = counter / (2**pulse_width / 160e6)

            osc_data['raw_data'] = raw_data
            osc_data['counter'] = counter
            osc_data['frequency'] = frequency

            oscillators['RING_OSC_' + str(bank) + '_' + str(i)] = osc_data
        bank = 'B'
        for i in range(34):
            osc_data = {}
            raw_data = self._get_single_ring_oscillator(bank=bank, number=i, pulse_width=pulse_width)
            counter = int('{0:016b}'.format(raw_data)[-12:], 2)
            frequency = counter / (2**pulse_width / 160e6)

            osc_data['raw_data'] = raw_data
            osc_data['counter'] = counter
            osc_data['frequency'] = frequency

            oscillators['RING_OSC_' + str(i)] = osc_data

        return oscillators

    def _get_single_ring_oscillator(self, bank='A', number=0, pulse_width=8):
        self.write_command(self.write_sync(write=False) * 300)
        self.registers['RingOscConfig'].write(0x7fff)
        self.registers['RingOscConfig'].write(0x3eff)
        if bank == 'A':
            bitstring = int(format(number, '03b') + format(0, '06b'), 2)
        elif bank == 'B':
            bitstring = int(format(0, '03b') + format(number, '06b'), 2)
        else:
            self.log.error('Bank %s doe not exist.' % bank)
        self.registers['RingOscRoute'].write(bitstring)

        self.send_global_pulse('start_ringosc_' + (bank.lower()), pulse_width=pulse_width)
        self.write_command(self.write_sync(write=False) * 40)
        raw_data = self.registers['RING_OSC_' + str(bank) + '_OUT'].read()
        return raw_data

    def get_temperature_sensors(self, samples=1, diode_current=14, log=True):
        sensor_array = ['TEMPSENS_T', 'TEMPSENS_B', 'TEMPSENS_A', 'TEMPSENS_D', 'TEMPSENS_C']
        temp = {}
        try:
            for sensor in sensor_array:
                temp[sensor] = {}
                if sensor in ['TEMPSENS_A', 'TEMPSENS_D', 'TEMPSENS_C']:
                    temp[sensor]['temp'], temp[sensor]['dADC'] = self._get_diode_temperature_sensor(sensor, samples, diode_current)
                    self.log.debug('Sensor: {0}\nTemperature: {1}\nADC: {2}'.format(sensor, temp[sensor]['temp'], temp[sensor]['dADC']))
                elif sensor in ['TEMPSENS_T', 'TEMPSENS_B']:
                    temp[sensor]['temp'], temp[sensor]['dADC'] = self._get_resistive_temperature_sensor(sensor, samples)
                    self.log.debug('Sensor: {0}\nTemperature: {1}\nADC: {2}'.format(sensor, temp[sensor]['temp'], temp[sensor]['dADC']))
            mean_temp = round(np.mean([val['temp'] for val in temp.values()]), 0)
            t_error = 3 if self.calibration.t_sensors_calibrated else 6
            if log:
                self.log.info('Mean chip temperature is ({0} +- {1})°C'.format(int(mean_temp), t_error))

        except RuntimeError as e:
            temp = {0: -99, 1: -99, 2: -99, 3: -99}
            mean_temp = -99
            self.log.error('There was an error reading temperature sensors: %s' % e)
        return mean_temp, temp

    def _get_resistive_temperature_sensor(self, sensor, samples, log=True):
        lower = []
        sensor_id_dict = {'TEMPSENS_T': 0, 'TEMPSENS_B': 1}
        before_value = self.registers['MON_ADC'].get()
        for reps in range(samples):
            if sensor == 'TEMPSENS_T':
                self.registers['MON_ADC'].write(0x80 + (before_value & 0x3f))
            elif sensor == 'TEMPSENS_B':
                self.registers['MON_ADC'].write(0x100 + (before_value & 0x3f))
            self.write_sync_01()
            lower.append(self.get_ADC_value(sensor)[0])
        self.registers['MON_ADC'].write(0x40 + (before_value & 0x3f))
        mean_adc = int(np.mean(lower))
        mean_temp = self.calibration._get_temperature_from_ADC_resistive_sensors(mean_adc, sensor=sensor_id_dict[sensor])
        if log:
            self.log.info('Mean chip temperature is ({0} +- 6)°C'.format(int(mean_temp)))
        return mean_temp, mean_adc

    def _get_diode_temperature_sensor(self, sensor, samples=1, diode_current=14, log=True):
        lower, upper = [], []
        sensor_id_dict = {'TEMPSENS_A': 0, 'TEMPSENS_D': 1, 'TEMPSENS_C': 2}
        for reps in range(samples):
            bitstring = int('1' + format(diode_current, '04b') + '01' + format(diode_current, '04b') + '0', 2)
            self.registers['MON_SENS_SLDO'].write(bitstring)
            self.registers['MON_SENS_ACB'].write(bitstring >> 6)
            self.write_sync_01()
            lower.append(self.get_ADC_value(sensor)[0])

            bitstring = int('1' + format(diode_current, '04b') + '11' + format(diode_current, '04b') + '1', 2)
            self.registers['MON_SENS_SLDO'].write(bitstring)
            self.registers['MON_SENS_ACB'].write(bitstring >> 6)
            self.write_sync_01()
            upper.append(self.get_ADC_value(sensor)[0])
        mean_adc = int(np.mean(upper) - np.mean(lower))
        mean_temp = self.calibration.get_temperature_from_ADC(mean_adc, sensor=sensor_id_dict[sensor])
        if log:
            self.log.info('Mean chip temperature is ({0} +- 6)°C'.format(int(mean_temp)))
        return mean_temp, mean_adc

    def get_temperature_NTC(self, log=True):
        T = self.bdaq.get_temperature_NTC(self.receiver)
        if log:
            self.log.info('NTC temperature is {0:1.2f}°C'.format(T))
        return T

    def get_temperature(self, log=True):
        if self.bdaq.enable_NTC:
            return self.get_temperature_NTC(log=log)
        else:
            return self.get_temperature_sensors(log=log)[0]

    def get_flavor(self, col):
        return get_flavor(col)

    def get_tdac_range(self, fe):
        return get_tdac_range(fe)


if __name__ == '__main__':
    ITkPixV1_chip = ITkPixV1()
    ITkPixV1_chip.init()
