''' Script to check that the scan API
    follows the intended behavior.
'''

import unittest
from importlib import reload
from unittest import mock

import bdaq53
from bdaq53.system import scan_base  # noqa: F401
from bdaq53.chips import rd53a  # noqa: F401
from bdaq53.tests import utils


def create_test_scan():
    ''' Must be in method to allow delayed import of ScanBase '''
    from bdaq53.system.scan_base import ScanBase

    class TestScan(ScanBase):
        scan_id = 'test_scan'

        def _configure(self, **_):
            pass

        def _scan(self, **_):
            pass

        def analyze(self):
            pass

    return TestScan


class TestScanStateMachine(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        ''' Do the minimum necessary to run bdaq53 without hardware '''
        utils.patch_chip_read_write(bdaq53.chips.rd53a.RD53A._write_register, bdaq53.chips.rd53a.RD53A._get_register_value)

        # Mock hardware communication via bdaq53.bdaq53.BDAQ53
        # Use the fact that patching base classes patches all derived classes and therefore also bdaq53.bdaq53.BDAQ53
        cls.bdaq53_patcher = mock.patch('bdaq53.system.bdaq53.BDAQ53')
        bdaq53_mock = cls.bdaq53_patcher.start()
        bdaq53_mock.return_value.get_temperature_NTC.return_value = 0
        bdaq53_mock.return_value.rx_lanes.__getitem__.return_value = 1

        # Mock write command, since chip hardware not available
        cls.write_patcher = mock.patch('bdaq53.chips.rd53a.RD53A.write_command')
        cls.write_patcher.return_value = 0b0
        cls.write_patcher.start()
        # Mock ADC read command, since chip hardware not available
        cls.read_ADC_patcher = mock.patch('bdaq53.chips.rd53a.RD53A.get_ADC_value')
        cls.read_ADC_patcher.return_value = (0, 0)
        cls.read_ADC_patcher.start()
        # Mock fifo readout
        cls.fifo_patcher = mock.patch('bdaq53.system.fifo_readout.FifoReadout')
        fifo_mock = cls.fifo_patcher.start()
        fifo_mock.return_value.print_readout_status.return_value = ([0], [0], [0])

        # Reload mocked import
        reload(bdaq53.system.scan_base)

        cls.TestScan = create_test_scan()

    @classmethod
    def tearDownClass(cls):
        cls.bdaq53_patcher.stop()
        cls.write_patcher.stop()
        cls.read_ADC_patcher.stop()
        cls.fifo_patcher.stop()
        utils.unpatch_chip_read_write(bdaq53.chips.rd53a.RD53A)

    def test_no_init(self):
        ''' Without calls to any scan function
            this should not raise an exception
        '''
        with self.TestScan() as _:
            pass

    def test_std_usage(self):
        ''' Test with std. scan API call
        '''
        with self.TestScan() as scan:
            scan.scan()
            scan.analyze()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestScanStateMachine)
    unittest.TextTestRunner(verbosity=2).run(suite)
