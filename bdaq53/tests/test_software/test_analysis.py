#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import os
import shutil
import unittest

import numpy as np
import matplotlib
import tables as tb
import yaml

import bdaq53  # noqa: E731
from bdaq53.analysis import analysis  # noqa: E731
from bdaq53.analysis.plotting import Plotting  # noqa: E731
from bdaq53.tests import utils  # noqa: E731

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))

matplotlib.use('Agg')  # Allow headless plotting


def get_mean_from_histogram(counts, bin_positions, axis=0):
    return np.average(counts, axis=axis, weights=bin_positions) * bin_positions.sum() / np.nansum(counts, axis=axis)


class TestAnalysis(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestAnalysis, cls).setUpClass()
        plt_logger = logging.getLogger('Plotting')
        cls._plt_log_handler = utils.MockLoggingHandler(level='DEBUG')
        plt_logger.addHandler(cls._plt_log_handler)
        cls.plt_log_messages = cls._plt_log_handler.messages

        ana_logger = logging.getLogger('Analysis')
        cls._ana_log_handler = utils.MockLoggingHandler(level='DEBUG')
        ana_logger.addHandler(cls._ana_log_handler)
        cls.ana_log_messages = cls._ana_log_handler.messages

        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)

    @classmethod
    def tearDownClass(cls):
        test_files = ['digital_scan', 'analog_scan', 'threshold_scan',
                      'ext_trigger_scan_tb', 'ext_trigger_scan_force_trg',
                      'tune_tlu', 'tot_calibration', 'injection_delay_scan',
                      'hitor_calibration', 'tot_tuning', 'timewalk_inj_scan', 'ext_trigger_scan_tb_tdc',
                      'digital_scan2', 'scan_pixel_registers', 'threshold_scan_ext_trig']
        for test_file in test_files:
            utils.try_remove(os.path.join(data_folder, test_file + '_interpreted.h5'))
            utils.try_remove(os.path.join(data_folder, test_file + '_interpreted.pdf'))
        utils.try_remove(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output_data'))
        utils.try_remove(os.path.join(data_folder, 'analog_scan_clustered.h5'))
        utils.try_remove(os.path.join(data_folder, 'analog_scan_clustered_prop.h5'))
        utils.try_remove(os.path.join(data_folder, 'injection_delay_scan.masks.h5'))
        utils.try_remove(os.path.join(data_folder, 'ext_trigger_scan_tb_event_aligned.h5'))
        utils.try_remove(os.path.join(data_folder, 'ext_trigger_scan_tb_aligned.h5'))
        utils.try_remove(os.path.join(data_folder, 'last_scan.pdf'))
        utils.try_remove(os.path.join(data_folder, 'par_scan_interpreted.h5'))
        utils.try_remove(os.path.join(data_folder, 'digital_scan2.h5'))
        utils.try_remove(os.path.join(data_folder, 'digital_scan.cfg.yaml'))
        utils.try_remove(os.path.join(data_folder, 'digital_scan2.cfg.yaml'))
        utils.try_remove(os.path.join(data_folder, 'digital_scan_tmp_1.h5'))
        utils.try_remove(os.path.join(data_folder, 'digital_scan_tmp_2.h5'))

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls._plt_log_handler.reset()
        cls._ana_log_handler.reset()

    def test_dig_scan_ana(self):
        ''' Test analysis of digital scan data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'digital_scan_interpreted.h5'),
            os.path.join(data_folder, 'digital_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_chunked_ana(self):
        ''' Test analysis of digital scan data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, chunk_size=99991) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'digital_scan_interpreted.h5'),
            os.path.join(data_folder, 'digital_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_ana_scan_ana(self):
        ''' Test analysis of analog scan data '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'analog_scan_interpreted.h5'),
            os.path.join(data_folder, 'analog_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_thr_scan_inter(self):
        ''' Test interpretation of threshold scan data '''
        raw_data_file = os.path.join(data_folder, 'threshold_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=False) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_interpreted.h5'),
            os.path.join(data_folder, 'threshold_scan_interpreted_result.h5'),
            node_names=['HistOcc', 'HistTot', 'HistRelBCID'],
            exact=True)
        self.assertTrue(data_equal, msg=error_msg)

        with Plotting(analyzed_data_file=a.analyzed_data_file, pdf_file=None,
                      level='preliminary', qualitative=False, internal=False,
                      save_single_pdf=False, save_png=False) as p:
            p.create_standard_plots()

        # Check that no errors are logged (= all plots created)
        self.assertFalse(self.plt_log_messages['error'])

    def test_thr_scan_ext_trig(self):
        ''' Test interpretation of threshold scan data '''
        raw_data_file = os.path.join(data_folder, 'threshold_scan_ext_trig.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=False) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_ext_trig_interpreted.h5'),
            os.path.join(data_folder, 'threshold_scan_ext_trig_interpreted_result.h5'),
            node_names=['HistOcc', 'HistTot', 'HistRelBCID'],
            exact=True)
        self.assertTrue(data_equal, msg=error_msg)

        with Plotting(analyzed_data_file=a.analyzed_data_file, pdf_file=None,
                      level='preliminary', qualitative=False, internal=False,
                      save_single_pdf=False, save_png=False) as p:
            p.create_standard_plots()

        # Check that no errors are logged (= all plots created)
        self.assertFalse(self.plt_log_messages['error'])

    def test_clustering_full(self):
        ''' Test cluster results against fixture '''
        with analysis.Analysis(raw_data_file=os.path.join(data_folder, 'analog_scan.h5'),
                               analyzed_data_file=os.path.join(data_folder, 'analog_scan_clustered.h5'),
                               cluster_hits=True, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'analog_scan_clustered.h5'),
            os.path.join(data_folder, 'analog_scan_clustered_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_clustering_properties(self):
        ''' Check validity of cluster results '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        analyzed_data_file = os.path.join(data_folder, 'analog_scan_clustered_prop.h5')
        with analysis.Analysis(raw_data_file=raw_data_file,
                               analyzed_data_file=analyzed_data_file,
                               store_hits=True, cluster_hits=True) as a:
            a.analyze_data()

        # Deduce properties from hit table
        with tb.open_file(analyzed_data_file) as in_file:
            hits = in_file.root.Hits[:]
            # Omit late hits do prevent double counting
            # Only ToT < 14 is clustered
            hits = hits[hits['tot'] < 14]
            n_hits = hits.shape[0]
            tot_sum = hits['tot'].sum()

        with tb.open_file(analyzed_data_file) as in_file:
            self.assertEqual(in_file.root.Cluster[:]['size'].sum(), n_hits)
            self.assertEqual(in_file.root.Cluster[:]['tot'].sum(), tot_sum)
            # Check hists
            hist_cluster_tot = in_file.root.HistClusterTot[:]
            tot_cls_tot = np.sum(hist_cluster_tot * np.arange(hist_cluster_tot.shape[0]))
            tot_cls_shape = np.sum(in_file.root.HistClusterShape[:])
            self.assertEqual(tot_cls_tot, tot_sum)
            self.assertEqual(tot_cls_shape, in_file.root.Cluster[:].shape[0])

    def test_emtpy_data(self):
        ''' Test for useful error message for empty data
        '''
        raw_data_file = os.path.join(data_folder, 'empty_data.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        self.assertTrue(any('Data is empty' in s for s in self.ana_log_messages['warning']))

    def test_ext_trg_ana(self):
        ''' Test analysis of external trigger scan with TLU trigger words

            Test beam data with TLU not waiting for RD53A device
        '''
        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan_tb.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, chunk_size=3989) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_ext_trg_ana_force_trg(self):
        ''' Test analysis of external trigger scan with TLU trigger words
            and forced trigger alignment.

            Test beam data (from SYNC, HW based veto) with TLU. This test data has not the best quality due to issues with
            SYNC flavor operation (ECR,...), thus this test data is very small. Nevertheless it is better than no test.
            Improved test data will be delivered soon.
        '''
        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan_force_trg.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, align_method=2) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_force_trg_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_force_trg_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, align_method=2, chunk_size=3989) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_force_trg_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_force_trg_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_tune_tlu_ana(self):
        ''' Test analysis of tune TLU
        '''
        from bdaq53.scans.tune_tlu import TuneTlu
        tune_tlu = TuneTlu(bench_config=self.bench_config)
        tune_tlu.output_filename = os.path.join(data_folder, 'tune_tlu')
        tune_tlu._analyze()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'tune_tlu_interpreted.h5'),
            os.path.join(data_folder, 'tune_tlu_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_calibrate_tot_ana(self):
        ''' Test analysis of calibrate tot scan
        '''
        from bdaq53.scans.calibrate_tot import TotCalibration
        cal_tot = TotCalibration(bench_config=self.bench_config)
        cal_tot.output_filename = os.path.join(data_folder, 'tot_calibration')
        cal_tot._analyze()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'tot_calibration_interpreted.h5'),
            os.path.join(data_folder, 'tot_calibration_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_scan_pixel_registers(self):
        ''' Test analysis of pixel register scan
        '''
        from bdaq53.scans.scan_pixel_registers import PixelRegisterScan
        pix_reg = PixelRegisterScan(bench_config=self.bench_config)
        pix_reg.output_filename = os.path.join(data_folder, 'scan_pixel_registers')
        pix_reg._analyze()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'scan_pixel_registers_interpreted.h5'),
            os.path.join(data_folder, 'scan_pixel_registers_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_injection_delay_ana(self):
        ''' Test analysis of injection delay scan
        '''
        from bdaq53.scans.scan_injection_delay import InjDelayScan
        from bdaq53.chips.rd53a import RD53A
        cal_injdel = InjDelayScan(bench_config=self.bench_config)
        cal_injdel.output_filename = os.path.join(data_folder, 'injection_delay_scan')
        cal_injdel.maskfile = None
        cal_injdel.chip_conf = cal_injdel._chips_confs[0]
        cal_injdel.chip = RD53A(cal_injdel.bdaq)
        cal_injdel.configuration['scan'] = {'start_column': 128, 'stop_column': 264, 'maskfile': None}
        cal_injdel._analyze()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'injection_delay_scan_interpreted.h5'),
            os.path.join(data_folder, 'injection_delay_scan_interpreted_result.h5'),
            exact=False)
        self.assertTrue(data_equal, msg=error_msg)

    def test_calibrate_hitor_ana(self):
        ''' Test analysis of calibrate hitor scan
        '''
        from bdaq53.scans.calibrate_hitor import HitorCalib
        cal_hitor = HitorCalib(bench_config=self.bench_config)
        cal_hitor.output_filename = os.path.join(data_folder, 'hitor_calibration')
        cal_hitor._analyze()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'hitor_calibration_interpreted.h5'),
            os.path.join(data_folder, 'hitor_calibration_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_tune_tot_ana(self):
        ''' Test analysis of tune TOT script
        '''
        from bdaq53.scans.tune_tot import TotTuning
        tune_tot = TotTuning(bench_config=self.bench_config)
        tune_tot.output_filename = os.path.join(data_folder, 'tot_tuning')
        tune_tot._analyze()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'tot_tuning_interpreted.h5'),
            os.path.join(data_folder, 'tot_tuning_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_bdaq53_converter_ana(self):
        ''' Test bdaq53 converter for testbeam analysis
        '''
        from bdaq53.analysis import bdaq53_converter as bdaq53_cv
        bdaq53_cv.process_raw_data(raw_data_file=os.path.join(data_folder, 'ext_trigger_scan_tb.h5'))

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_tb_interpreted_bdaq53_cv_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_tb_event_aligned.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_tb_event_aligned_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_tb_aligned.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_tb_aligned_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_tdc_ana(self):
        ''' Test TDC analysis (+ clustering) using raw data recorded at Testbeam using TDC method.
        Many bad TDC words (ambiguities, errors) due to bad data, but better than not testing it.
        '''
        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan_tb_tdc.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, analyze_tdc=True, use_tdc_trigger_dist=True, align_method=0, cluster_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'ext_trigger_scan_tb_tdc_interpreted.h5'),
            os.path.join(data_folder, 'ext_trigger_scan_tb_tdc_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_par_analysis(self):
        ''' Test analysis per scan parameter id

            The test data is a digital scan repeated 3 times.
            First time with scan par id = 0
            Second time with scan par id = 1
            Third time with scan par id = 0
        '''
        raw_data_file = os.path.join(data_folder, 'par_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        with tb.open_file(os.path.join(data_folder, 'par_scan_interpreted.h5')) as in_file:
            occ = in_file.root.HistOcc[:]
            self.assertTrue(np.all(occ[:, :, 0] - 2 * occ[:, :, 1] == 0))
            self.assertTrue(in_file.root.Hits[0]['scan_param_id'] == 0)
            self.assertTrue(in_file.root.Hits[:]['scan_param_id'].max() == 1)
            self.assertTrue(in_file.root.Hits[-1]['scan_param_id'] == 0)

    def test_non_blocking_analysis(self):
        ''' Test analysis of digital scan data on parallel processes'''

        from bdaq53.scans.scan_digital import DigitalScan

        scans = []  # scan objects with attached processes}

        shutil.copy(os.path.join(data_folder, 'digital_scan.h5'), os.path.join(data_folder, 'digital_scan2.h5'))

        with DigitalScan() as scan:
            # Hack needed variables that are set at runtime
            scan.output_filename = os.path.join(data_folder, 'digital_scan_tmp_1')  # raw_data_file#raw_data_file[:-3] + '_interpreted_%d' % i
            roc = scan.ScanDataContainer()
            scan.chip_conf = {'send_data': None}
            scan._configure_readout(roc)
            scan.fifo_readout_dict['rx0'] = roc
            scan.run_name = 'digital_scan'
            scan._output_directories_per_scan[0] = data_folder

            scan.configuration['bench']['analysis']['blocking'] = False
            scan.analyze()
            scans.append(scan)

        with DigitalScan() as scan:
            # Hack needed variables that are set at runtime
            scan.output_filename = os.path.join(data_folder, 'digital_scan_tmp_2')  # raw_data_file#raw_data_file[:-3] + '_interpreted_%d' % i
            roc = scan.ScanDataContainer()
            scan.chip_conf = {'send_data': None}
            scan._configure_readout(roc)
            scan.fifo_readout_dict['rx0'] = roc
            scan.run_name = 'digital_scan2'
            scan._output_directories_per_scan[0] = data_folder

            scan.configuration['bench']['analysis']['blocking'] = False
            scan.analyze()
            scans.append(scan)

        for scan in scans:
            scan.wait_for_analysis()
            data_equal, error_msg = utils.compare_h5_files(scan.output_filename + '_interpreted.h5',
                                                           os.path.join(data_folder, 'digital_scan_interpreted_result.h5'),
                                                           ignore_nodes=['/Hits', ])
            self.assertTrue(data_equal, msg=error_msg)


if __name__ == '__main__':
    unittest.main()
