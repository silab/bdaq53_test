#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import importlib
import pkgutil
import inspect
import logging
import time
import os
import unittest
from importlib import reload
from unittest import mock

import yaml

import bdaq53
from bdaq53 import scans
from bdaq53.system import scan_base  # noqa: F401
from bdaq53.chips import rd53a  # noqa: F401
from bdaq53.tests import utils

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


def import_submodules(package, recursive=True):
    """ Import all submodules of a module, recursively, including subpackages

    :param package: package (name or actual module)
    :type package: str | module
    :rtype: dict[str, types.ModuleType]
    """
    if isinstance(package, str):
        package = importlib.import_module(package)
    results = {}
    for _, name, is_pkg in pkgutil.walk_packages(package.__path__):
        full_name = package.__name__ + '.' + name
        results[full_name] = importlib.import_module(full_name)
        if recursive and is_pkg:
            results.update(import_submodules(full_name))
    return results


def get_scan_classes():
    scan_classes = []
    for mod_name, module in import_submodules(package=scans, recursive=False).items():
        module_classes = [m for m in inspect.getmembers(module, inspect.isclass) if m[1].__module__ == mod_name]
        if module_classes:  # no class defined in meta scans
            if 'Scan' in module_classes[0][0]:  # check if class is a scan (not tuning,  calibration, ...)
                scan_classes.append(module_classes[0][1])
    return scan_classes


class TestScans(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestScans, cls).setUpClass()

        analysis_logger = logging.getLogger('Analysis')
        cls.analysis_log_handler = utils.MockLoggingHandler(level='DEBUG')
        analysis_logger.addHandler(cls.analysis_log_handler)
        cls.analysis_log_messages = cls.analysis_log_handler.messages

        # Do the minimum necessary to run bdaq53 without hardware
        utils.patch_chip_read_write(bdaq53.chips.rd53a.RD53A._write_register, bdaq53.chips.rd53a.RD53A._get_register_value)
        # Mock hardware communication via bdaq53.bdaq53.BDAQ53
        # Use the fact that patching base classes patches all derived classes and therefore also bdaq53.bdaq53.BDAQ53
        cls.bdaq53_patcher = mock.patch('bdaq53.system.bdaq53.BDAQ53')
        bdaq53_mock = cls.bdaq53_patcher.start()
        bdaq53_mock.return_value.get_temperature_NTC.return_value = 0
        bdaq53_mock.return_value.rx_lanes.__getitem__.return_value = 1
        bdaq53_mock.return_value.get_trigger_counter.return_value = 1  # enough to stop scan
        bdaq53_mock.return_value.get_tlu_erros.return_value = (0, 0)
        # Mock write command, since chip hardware not available
        cls.write_patcher = mock.patch('bdaq53.chips.rd53a.RD53A.write_command')
        cls.write_patcher.return_value = 0b0
        cls.write_patcher.start()
        # Mock ADC read command, since chip hardware not available
        cls.read_ADC_patcher = mock.patch('bdaq53.chips.rd53a.RD53A.get_ADC_value')
        cls.read_ADC_patcher.return_value = (0, 0)
        cls.read_ADC_patcher.start()
        # Mock fifo readout
        cls.fifo_patcher = mock.patch('bdaq53.system.fifo_readout.FifoReadout')
        fifo_mock = cls.fifo_patcher.start()
        fifo_mock.return_value.print_readout_status.return_value = ([0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0])

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)

        # Reload mocked import
        reload(bdaq53.system.scan_base)

    @classmethod
    def tearDownClass(cls):
        cls.bdaq53_patcher.stop()
        cls.write_patcher.stop()
        cls.read_ADC_patcher.stop()
        cls.fifo_patcher.stop()
        utils.unpatch_chip_read_write(bdaq53.chips.rd53a.RD53A)

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls.analysis_log_handler.reset()

    def check_scan_success(self, scan_log_messages, skip_analysis=False):
        ''' Check the log output if scan was successfull '''
        if scan_log_messages['error'] or self.analysis_log_messages['error']:
            return False
        if 'Scan finished' not in scan_log_messages['success'] or 'All done!' not in scan_log_messages['success']:
            return False
        if not skip_analysis:
            if 'Analyzing data...' not in self.analysis_log_messages['info']:
                return False
        return True

    def test_scans(self):
        ''' Run all scans to check for runtime errors.

            If possible also run analysis step.
        '''
        skip_scan = ['Eudaq',  # needs special setup; tested on dedicated runner
                     'DAC',  # needs periphery multimeter
                     'SensorIVScan',  # needs periphery sourcemeter
                     'TimewalkScan',  # takes too long?!
                     'PixelRegisterScan'  # has custom analysis
                     ]
        # Skip analysis in scans analysis that need data and otherwise fails
        skip_analysis = ['InTimeThrScan',  # complex analysis that crashed on empty data
                         'InjDelayScan',  # complex analysis that crashed on empty data
                         'NoiseOccScan',  # needs analyzed data file
                         'SourceScan',  # needs analyzed data file
                         'StuckPixelScan',  # needs analyzed data file
                         'fg']  # needs analyzed data file

        for ScanClass in get_scan_classes():
            if any([s in ScanClass.__name__ for s in skip_scan]):
                continue

            # Catch scan output to check for errors reported
            scan_logger = logging.getLogger(ScanClass.__name__)
            scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
            scan_logger.addHandler(scan_log_handler)
            scan_log_messages = scan_log_handler.messages

            with ScanClass() as scan:
                skip_ana_step = any([s in ScanClass.__name__ for s in skip_analysis])
                scan.scan()
                # Check if analysis can be run
                if not skip_ana_step:
                    time.sleep(1)  # give slow CERN file system time to close file handle
                    scan.analyze()

            self.assertTrue(self.check_scan_success(scan_log_messages, skip_analysis=skip_ana_step))
            self.analysis_log_handler.reset()


if __name__ == '__main__':
    unittest.main()
