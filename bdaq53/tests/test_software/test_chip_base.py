#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import unittest
from unittest import mock
import numpy as np

import bdaq53  # noqa: E731
from bdaq53.chips import rd53a, chip_base

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


class TestChipBase(unittest.TestCase):

    def test_shift_pattern_classic(self):
        ''' Test the normal and inverted classic shift pattern '''

        classic_pattern = chip_base.DoubleShiftPattern(dimensions=(400, 192), mask_step=24)
        classic_pattern_inv = chip_base.DoubleShiftPatternInv(dimensions=(400, 192), mask_step=24)

        for mask, mask_inv in zip(classic_pattern, classic_pattern_inv):
            self.assertTrue(np.all(~mask == mask_inv))

    def test_masking_loop(self):
        ''' Test if consecutive calls to the masking loop give expected results '''

        start_column = 0
        stop_column = 400
        start_row = 0
        stop_row = 192

        chip = rd53a.RD53A(bdaq=mock.Mock())
        cmds = []  # store all send command unraveled

        # Write initial masks
        def configure(chip):
            chip.masks['enable'][:] = False
            chip.masks['injection'][:] = False
            chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
            chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
            chip.masks.apply_disable_mask()
            chip.masks.update()

        def store_cmd(cmd):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            configure(chip)
            cmds = []  # reset written commands
            for _ in chip.masks.shift(masks=['enable', 'injection']):
                pass

            old_cmds = cmds.copy()
            configure(chip)
            cmds = []  # reset written commands
            for _ in chip.masks.shift(masks=['enable', 'injection']):
                pass

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds, dtype=np.int16), np.array(old_cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))

            chip_masks = chip.masks.was.copy()  # actual masks in chip

            configure(chip)
            cmds = []  # reset written commands
            for _ in chip.masks.shift(masks=['enable']):  # shift only one mask to create different commands
                pass
            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds, dtype=np.int16), np.array(old_cmds, dtype=np.int16)
            self.assertTrue(~np.array_equal(a, b))  # less masks are shifted, thus commands have to be different

            # After the end of the loop the mask in chip should be the same
            self.assertTrue(chip_masks, chip.masks.was.copy())

            cmds = []  # reset written commands
            for _ in chip.masks.shift(masks=['enable', 'injection']):
                pass

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds, dtype=np.int16), np.array(old_cmds, dtype=np.int16)
            self.assertTrue(~np.array_equal(a, b))  # chip not reset, thus different commands should be send

    def test_masking_cache(self):
        ''' Test if consecutive calls to the masking loop with cache enabled gives same results '''

        start_column = 0
        stop_column = 400
        start_row = 0
        stop_row = 192

        chip = rd53a.RD53A(bdaq=mock.Mock())
        cmds = []  # store all send command unraveled

        # Write initial masks
        def configure(chip):
            chip.masks['enable'][:] = False
            chip.masks['injection'][:] = False
            chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
            chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
            chip.masks.apply_disable_mask()
            chip.masks.update()

        def store_cmd(cmd):
            cmds.extend(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            configure(chip)
            cmds = []  # reset written commands
            for _ in chip.masks.shift(masks=['enable', 'injection'], cache=True):
                pass

            old_cmds = cmds.copy()
            configure(chip)
            cmds = []  # reset written commands
            for _ in chip.masks.shift(masks=['enable', 'injection'], cache=True):
                pass

            # Use numpy arrays with data since they can be checked for equality much faster
            a, b = np.array(cmds, dtype=np.int16), np.array(old_cmds, dtype=np.int16)
            self.assertTrue(np.array_equal(a, b))


if __name__ == '__main__':
    unittest.main()
