#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import copy
import logging
import os
import unittest
from importlib import reload
from unittest import mock

import yaml

import bdaq53
from bdaq53.system import scan_base  # noqa: F401
from bdaq53.chips import rd53a  # noqa: F401
from bdaq53.tests import utils

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


class TestScanBase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestScanBase, cls).setUpClass()

        # Catch digital scan output to check for errors reported
        scan_logger = logging.getLogger('DigitalScan')
        cls.scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
        scan_logger.addHandler(cls.scan_log_handler)
        cls.scan_log_messages = cls.scan_log_handler.messages
        analysis_logger = logging.getLogger('Analysis')
        cls.analysis_log_handler = utils.MockLoggingHandler(level='DEBUG')
        analysis_logger.addHandler(cls.analysis_log_handler)
        cls.analysis_log_messages = cls.analysis_log_handler.messages

        # Do the minimum necessary to run bdaq53 without hardware
        utils.patch_chip_read_write(bdaq53.chips.rd53a.RD53A._write_register, bdaq53.chips.rd53a.RD53A._get_register_value)
        # Mock hardware communication via bdaq53.bdaq53.BDAQ53
        # Use the fact that patching base classes patches all derived classes and therefore also bdaq53.bdaq53.BDAQ53
        cls.bdaq53_patcher = mock.patch('bdaq53.system.bdaq53.BDAQ53')
        bdaq53_mock = cls.bdaq53_patcher.start()
        bdaq53_mock.return_value.get_temperature_NTC.return_value = 0
        bdaq53_mock.return_value.rx_lanes.__getitem__.return_value = 1
        # Mock write command, since chip hardware not available
        cls.write_patcher = mock.patch('bdaq53.chips.rd53a.RD53A.write_command')
        cls.write_patcher.return_value = 0b0
        cls.write_patcher.start()
        # Mock ADC read command, since chip hardware not available
        cls.read_ADC_patcher = mock.patch('bdaq53.chips.rd53a.RD53A.get_ADC_value')
        cls.read_ADC_patcher.return_value = (0, 0)
        cls.read_ADC_patcher.start()
        # Mock fifo readout
        cls.fifo_patcher = mock.patch('bdaq53.system.fifo_readout.FifoReadout')
        fifo_mock = cls.fifo_patcher.start()
        fifo_mock.return_value.print_readout_status.return_value = ([0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0])

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)

        # Reload mocked import
        reload(bdaq53.system.scan_base)

    @classmethod
    def tearDownClass(cls):
        cls.bdaq53_patcher.stop()
        cls.write_patcher.stop()
        cls.read_ADC_patcher.stop()
        cls.fifo_patcher.stop()
        utils.unpatch_chip_read_write(bdaq53.chips.rd53a.RD53A)

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls.scan_log_handler.reset()
        cls.analysis_log_handler.reset()

    def check_scan_success(self):
        ''' Check the log output if scan was successfull '''
        if self.scan_log_messages['error'] or self.analysis_log_messages['error']:
            return False
        if 'Scan finished' not in self.scan_log_messages['success'] or 'All done!' not in self.scan_log_messages['success']:
            return False
        if 'Analyzing data...' not in self.analysis_log_messages['info']:
            return False
        return True

    def test_scan_single_chip(self):
        from bdaq53.scans import scan_digital
        with scan_digital.DigitalScan() as scan:
            scan.scan()
            scan.analyze()
        self.assertTrue(self.check_scan_success())

    def test_scan_quad_module(self):
        ''' Test digital scan on one quad chip module '''
        from bdaq53.scans import scan_digital
        # Add second chip to module
        bench_config = copy.deepcopy(self.bench_config)
        for i in range(1, 4):
            bench_config['modules']['module_0']['fe_%d' % i] = copy.deepcopy(bench_config['modules']['module_0']['fe_0'])
            bench_config['modules']['module_0']['fe_%d' % i]['chip_sn'] = '0x000%d' % (i + 1)
            bench_config['modules']['module_0']['fe_%d' % i]['receiver'] = "rx%d" % i
            bench_config['modules']['module_0']['fe_%d' % i]['send_data'] = "tcp://127.0.0.1:550%d" % i

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.scan()
            scan.analyze()
        self.assertTrue(self.check_scan_success())

    def test_scan_four_modules(self):
        ''' Test digital scan on four single chip modules '''
        from bdaq53.scans import scan_digital
        # Add second module
        bench_config = copy.deepcopy(self.bench_config)
        for i in range(1, 4):
            bench_config['modules']['module_%d' % i] = copy.deepcopy(bench_config['modules']['module_0'])
            bench_config['modules']['module_%d' % i]['fe_0']['chip_sn'] = '0x000%d' % (i + 1)
            bench_config['modules']['module_%d' % i]['fe_0']['receiver'] = "rx%d" % i
            bench_config['modules']['module_%d' % i]['fe_0']['send_data'] = "tcp://127.0.0.1:550%d" % i

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.scan()
            scan.analyze()
        self.assertTrue(self.check_scan_success())

    def test_scan_two_dc_modules(self):
        ''' Test digital scan on two double chip modules '''
        from bdaq53.scans import scan_digital
        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config['modules']['module_0']['fe_1'] = copy.deepcopy(bench_config['modules']['module_0']['fe_0'])
        bench_config['modules']['module_0']['fe_1']['chip_sn'] = '0x0002'
        bench_config['modules']['module_0']['fe_1']['receiver'] = "rx1"
        bench_config['modules']['module_0']['fe_1']['send_data'] = "tcp://127.0.0.1:5501"
        # Add additional module
        bench_config['modules']['module_1'] = copy.deepcopy(bench_config['modules']['module_0'])
        bench_config['modules']['module_1']['fe_0']['chip_sn'] = '0x0003'
        bench_config['modules']['module_1']['fe_0']['receiver'] = "rx2"
        bench_config['modules']['module_1']['fe_0']['send_data'] = "tcp://127.0.0.1:5502"
        # Add additional chip to module 1
        bench_config['modules']['module_1']['fe_1'] = copy.deepcopy(bench_config['modules']['module_1']['fe_0'])
        bench_config['modules']['module_1']['fe_1']['chip_sn'] = '0x0004'
        bench_config['modules']['module_1']['fe_1']['receiver'] = "rx3"
        bench_config['modules']['module_1']['fe_1']['send_data'] = "tcp://127.0.0.1:5503"

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.scan()
            scan.analyze()
        self.assertTrue(self.check_scan_success())

    def test_scan_two_modules_sc_dc(self):
        ''' Test digital scan on two modules, one single and one double chip '''
        from bdaq53.scans import scan_digital
        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional module
        bench_config['modules']['module_1'] = copy.deepcopy(bench_config['modules']['module_0'])
        bench_config['modules']['module_1']['fe_0']['chip_sn'] = '0x0002'
        bench_config['modules']['module_1']['fe_0']['receiver'] = "rx1"
        bench_config['modules']['module_1']['fe_0']['send_data'] = "tcp://127.0.0.1:5501"
        # Add additional chip to module 1
        bench_config['modules']['module_1']['fe_1'] = copy.deepcopy(bench_config['modules']['module_1']['fe_0'])
        bench_config['modules']['module_1']['fe_1']['chip_sn'] = '0x0003'
        bench_config['modules']['module_1']['fe_1']['receiver'] = "rx2"
        bench_config['modules']['module_1']['fe_1']['send_data'] = "tcp://127.0.0.1:5502"

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.scan()
            scan.analyze()
        self.assertTrue(self.check_scan_success())

    def test_scan_wrong_module_cfg(self):
        ''' Test exceptions for invalid module configuration '''
        from bdaq53.scans import scan_digital
        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config['modules']['module_0']['fe_1'] = copy.deepcopy(bench_config['modules']['module_0']['fe_0'])
        bench_config['modules']['module_0']['fe_1']['chip_sn'] = '0x0002'  # same serial number as first chip will raise KeyError
        bench_config['modules']['module_0']['fe_1']['receiver'] = "rx1"
        bench_config['modules']['module_0']['fe_1']['send_data'] = "tcp://127.0.0.1:5501"

        # Same chip sn
        bench_config['modules']['module_0']['fe_1']['chip_sn'] = '0x0001'  # same serial number as first chip will raise KeyError
        with self.assertRaises(KeyError):
            with scan_digital.DigitalScan(bench_config=bench_config):
                pass
        bench_config['modules']['module_0']['fe_1']['chip_sn'] = '0x0002'  # reset

        # Same send data socket
        bench_config['modules']['module_0']['fe_1']['receiver'] = "rx1"
        # ValueError: Two or more chips have the same data sending address/port!
        bench_config['modules']['module_0']['fe_1']['send_data'] = "tcp://127.0.0.1:5500"
        with self.assertRaises(ValueError):
            with scan_digital.DigitalScan(bench_config=bench_config):
                pass
        bench_config['modules']['module_0']['fe_1']['send_data'] = "tcp://127.0.0.1:5501"  # reset

#         # Same receiver (should raise in future)
#         bench_config['modules']['module_0']['fe_1']['chip_sn'] = '0x0002'
#         bench_config['modules']['module_0']['fe_1']['receiver'] = "rx0"
#         with scan_digital.DigitalScan(bench_config=bench_config) as scan:
#             pass


if __name__ == '__main__':
    unittest.main()
