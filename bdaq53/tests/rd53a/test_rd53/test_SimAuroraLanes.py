#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import logging
import shutil

from bdaq53.tests import utils
from bdaq53.scans.test_registers import RegisterTest


local_configuration = {
    'lanes': [1, 4]
}


class TestAuroraLanes(unittest.TestCase):
    def test_test_registers(self):
        for lanes in local_configuration.pop('lanes'):
            logging.info('Test register write/read with %i Aurora Lane(s)' % (lanes))
            self.test = RegisterTest(bdaq_conf=utils.setup_cocotb(rx_lanes=lanes), scan_config={'ignore': list(range(3, 138))})
            self.test.scan()
            self.test.analyze()
            self.test.close()

    def tearDown(self):
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
