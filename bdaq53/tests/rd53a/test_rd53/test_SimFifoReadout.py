#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import numpy as np
import logging

from queue import Empty
from bdaq53.tests import utils
from bdaq53.system.bdaq53 import BDAQ53
from bdaq53.chips.rd53a import RD53A
from bdaq53.system.fifo_readout import FifoReadout, ReadoutChannel
from bdaq53.analysis import analysis_utils
from bdaq53.analysis import rd53a_analysis


class TestFifoReadout(unittest.TestCase):
    def setUp(self):
        self.bdaq = BDAQ53(utils.setup_cocotb())
        self.bdaq.init()
        self.chip = RD53A(self.bdaq)

    def test_fifo_readout(self):
        logging.info('Starting FIFO readout test')

        # Configure cmd encoder
        self.bdaq['cmd'].reset()
        self.chip.write_command(self.chip.write_sync(write=False) * 32)

        # Establish link
        self.assertTrue(self.bdaq.wait_for_pll_lock())
        self.chip.setup_aurora(tx_lanes=self.bdaq.rx_channels['rx0'].get_rx_config())

        # Workaround for locking problems
        for _ in range(30):
            self.chip.write_command(self.chip.write_sync(write=False) * 32)
            self.chip.write_ecr()
            self.bdaq.wait_for_pll_lock()
            self.bdaq.wait_for_aurora_sync()

        logging.info('Communication established')

        fr = FifoReadout(self.bdaq)

        fr.print_readout_status()

        fr.attach_channel(ReadoutChannel(receiver=self.chip.receiver, callback=None, fill_buffer=True))
        fr.start()

        # insert empty triggers
        indata = self.chip.send_trigger(trigger=0b1111, write=False) * 4
        self.chip.write_command(indata)

        # progress simulation
        for _ in range(100):
            self.bdaq.rx_channels['rx0'].get_rx_ready()

        fr.stop()
        fr.print_readout_status()

        queue = fr._data_buffers[int(self.chip.receiver[2])]
        buffer = []
        while True:
            try:
                data = queue.get(block=False)
            except Empty:
                break
            buffer.append(data[0])
        rawdata = np.concatenate(buffer)

        hits, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id, hist_event_status, hist_tdc_status, hist_tdc_value, hist_bcid_error = analysis_utils.init_outs(len(rawdata), 0)
        rd53a_analysis.interpret_data(rawdata, hits, hist_occ, hist_tot,
                                      hist_rel_bcid, hist_trigger_id,
                                      hist_event_status, hist_tdc_status,
                                      hist_tdc_value, hist_bcid_error,
                                      scan_param_id=0, rx_id=0)

        self.assertEqual(len(hits), 32)

    def tearDown(self):
        self.bdaq.close()
        utils.close_sim()


if __name__ == '__main__':
    unittest.main()
