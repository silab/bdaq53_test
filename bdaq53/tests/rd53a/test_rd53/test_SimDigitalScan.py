#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import unittest
import logging
import shutil
import tables as tb
import numpy as np

from bdaq53.tests import utils
from bdaq53.scans.scan_digital import DigitalScan


configuration = {
    'n_injections': 2,

    'start_column': 160,
    'stop_column': 162,
    'start_row': 100,
    'stop_row': 102,
}


class TestDigitalScan(unittest.TestCase):
    def test_scan_digital(self):
        logging.info('Starting digital scan test')

        self.scan = DigitalScan(bdaq_conf=utils.setup_cocotb(20), scan_config=configuration)
        self.scan.scan()
        self.scan.analyze()
        self.scan.close()

        ''' Assert raw data '''
        output_filename = os.path.join(self.scan._output_directories_per_scan[0], self.scan.run_name)
        with tb.open_file(output_filename + '_interpreted.h5', 'r+') as in_file_h5:
            hist_occ = in_file_h5.root.HistOcc[:]

        self.assertEqual(np.sum(hist_occ), 4 * configuration['n_injections'], 'Incorrect number of hits has been recorded!')
        self.assertEqual(hist_occ[160:162, 100:102].tolist(), [[[configuration['n_injections']]] * 2] * 2)

    def tearDown(self):
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
