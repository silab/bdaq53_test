#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import argparse
import os
import yaml

from importlib import import_module
from inspect import getmembers

from bdaq53.system import logger

log = logger.setup_derived_logger('BDAQ53 CLI')
root_dir = os.path.dirname(os.path.abspath(__file__))


def list_scans():
    # List all available scans and measurements
    scans = {}
    dirs = ['scans', 'measurements']

    for dir in dirs:
        for f in os.listdir(os.path.join(root_dir, dir)):
            if not f.split('.')[-1] == 'py' or f == '__init__.py':
                continue

            key = f.split('.')[0]
            with open(os.path.join(os.path.join(root_dir, dir), f), 'r') as sf:
                for line in sf.readlines():
                    if 'class ' in line:
                        cl = line.split(' ')[1].split('(')[0]
            scans[key] = cl

    return scans


def main():
    scans = list_scans()
    scan_names = [key for key in scans.keys()]

    parser = argparse.ArgumentParser(description='Bonn DAQ system for RD53 readout chips\nexample: bdaq scan_digital -p start_column=10 stop_column=20', formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('scan',
                        type=str,
                        nargs='?',
                        choices=scan_names,
                        help='Scan name. Allowed values are:\n\n' + ',\n'.join(scan_names),
                        metavar='scan_name')

    parser.add_argument('-f', '--configuration_file',
                        type=str,
                        nargs=1,
                        help='Path to testbench configuration file. If not given the default location is used.',
                        metavar='TESTBENCH_YAML')

    parser.add_argument('-p', '--parameters',
                        type=str,
                        nargs='+',
                        help='List of scan parameters. E.g. paramA=9 paramB=1',
                        metavar='',
                        default=[])

    parser.add_argument('-r', '--registers',
                        type=str,
                        nargs='+',
                        help='List of registers to set. E.g. paramA=9 paramB=1',
                        metavar='',
                        default=[])

    args = parser.parse_args()

    if args.scan:
        mod = import_module('bdaq53.scans.' + args.scan)

        if args.configuration_file:
            configuration_file = args.configuration_file
        else:
            configuration_file = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(mod.__file__)), '..' + os.sep + 'testbench.yaml'))

        log.info('Using testbench configuration file: ' + configuration_file + '\n')

        with open(configuration_file, 'r') as f:
            bench_config = yaml.full_load(f)

        scan_config = mod.scan_configuration

        for param in args.parameters:
            key, value = param.split('=')
            scan_config[key] = eval(value)

        if len(args.registers) > 0:
            default_file = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(mod.__file__))), 'rd53a_default.cfg.yaml')
            with open(default_file, 'r') as f:
                scan_config['chip'] = yaml.full_load(f)
        for param in args.registers:
            key, value = param.split('=')
            scan_config['chip']['registers'][key] = eval(value)

        for name, cls in getmembers(mod):
            if name == scans[args.scan]:
                break

        with cls(bench_config=bench_config, scan_config=scan_config) as scan:
            scan.scan()
            scan.analyze()


def deprecated():
    raise RuntimeError("'bdaq53' is deprecated. Please use 'bdaq' instead!")


if __name__ == '__main__':
    main()
