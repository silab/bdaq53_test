#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import git
import pkg_resources
import collections
from copy import deepcopy


VERSION = pkg_resources.get_distribution("bdaq53").version


def recursive_update(first, second={}):
    '''
        Recursively updates a nested dict with another nested dict (can be any dict-like collections.Mapping objects).
        Each value in 'second' that is not of dict-like type overwrites the corresponding (same key) value in 'first'.
        If it is of dict-like type, it updates the corresponding dict-like object in first,
        using this function again, i.e. recursively.
        If a key in 'second' doesn't exist in 'first' the value from 'second' is simply appended.

        Before updating, a deep copy of 'first' is created such that the two function arguments stay unchanged!

        Parameters:
        ----------
        first : dict-like object
                The dict that is to be updated.
        second : dict-like object
                Updates the 'first' dict.

        Returns:
        ----------
        The merged dict.
    '''
    for k, v in second.items():
        if isinstance(v, collections.abc.Mapping):
            first[k] = recursive_update(first.get(k, {}), v)
        else:
            first[k] = v
    return first


def recursive_update_deep(first, second={}):
    '''
        Recursively updates a nested dict with another nested dict, see recursive_update().

        Before updating, a deep copy of 'first' is created such that
        the two function arguments, 'first' and 'second', stay unchanged!

        Parameters:
        ----------
        first : dict-like object
                The dict that is to be updated.
        second : dict-like object
                Updates the 'first' dict.

        Returns:
        ----------
        The merged dict.
    '''

    retVal = deepcopy(first)
    retVal = recursive_update(retVal, second)
    return retVal


def get_software_version():
    ''' Extract software version from git checkout

        Fallback to package version if no git repo detected.
    '''

    def get_changed_files():
        ignore_files = ['bdaq53/periphery.yaml', 'bdaq53/testbench.yaml']
        return [item.a_path for item in repo.index.diff(None) if item.a_path not in ignore_files]

    try:
        if os.getenv('CI'):  # gitpython hangs in CI for unknown reasons
            raise git.InvalidGitRepositoryError
        repo = git.Repo(search_parent_directories=True)
        active_branch = repo.active_branch
        rev = active_branch.object.name_rev[:7]
        branch = active_branch.name

        version_string = branch + '@' + rev

        changed_files = get_changed_files()
        if len(changed_files) > 0:
            version_string += '\nChanged files: ' + ', '.join(changed_files)

        return version_string
    except TypeError:  # Tag with detached head creates type error on repo.active_branch, issue 212
        version_string = 'Tag: ' + str(next((tag for tag in repo.tags if tag.commit == repo.head.commit), None))

        changed_files = get_changed_files()
        if len(changed_files) > 0:
            version_string += '\nChanged files: ' + ', '.join(changed_files)

        return version_string
    except git.InvalidGitRepositoryError:
        return VERSION


def get_latest_file(directory, condition):
    candidates = []
    for root, _, files in os.walk(directory):
        for f in files:
            if condition(f):
                filename = os.path.join(root, f)
                candidates.append({'filename': filename, 'modified': os.path.getmtime(filename)})

    if len(candidates) == 0:
        return None

    file = candidates[0]
    for candidate in candidates:
        if candidate['modified'] > file['modified']:
            file = candidate

    return os.path.join(directory, file['filename'])


def get_latest_maskfile(directory):
    return get_latest_file(directory=directory, condition=lambda file: (file.split('.')[-1] == 'h5' and file.split('.')[-2] == 'masks'))


def get_latest_h5file(directory, scan_pattern, interpreted=False):
    if interpreted:
        scan_pattern += '_interpreted'
    return get_latest_file(directory=directory, condition=lambda file: (file.split('.')[-1] == 'h5' and file.split('.')[-2].endswith(scan_pattern)))


def get_latest_chip_configuration_file(directory):
    return get_latest_file(directory=directory, condition=lambda file: (file.split('.')[-1] == 'yaml' and file.split('.')[-2] == 'cfg'))
