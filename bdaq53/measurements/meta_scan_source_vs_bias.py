#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#


'''
    This meta script performs a simple source scan at different bias voltages.
'''

import os
import time
import tables as tb
import matplotlib.pyplot as plt

from bdaq53.system import logger
from bdaq53.scans.scan_source import SourceScan


scan_configuration = {
    'module_name': 'module_0',
    'hv_current_limit': 1e-6,
    'VBIAS_start': 0,
    'VBIAS_stop': -10,
    'VBIAS_step': -5,
}

source_scan_configuration = {
    'start_column': 128,  # start column for mask
    'stop_column': 264,  # stop column for mask
    'start_row': 0,  # start row for mask
    'stop_row': 192,  # stop row for mask

    'scan_timeout': False,  # timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': 80000,  # number of maximum received triggers after stopping readout, if False no limit on received trigger

    'trigger_latency': 100,  # latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 65,  # trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,  # length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,  # length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Should be adjusted for longer trigger length.

    'use_tdc': False,

    'hitor_calib_file': None,

    # Trigger configuration
    'bdaq': {'TLU': {
        'TRIGGER_MODE': 0,  # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 1  # Selecting trigger input: HitOR (1), disabled (0)
    }
    }
}


class MetaDataTable(tb.IsDescription):
    parameter = tb.StringCol(64, pos=0)
    value = tb.StringCol(64, pos=1)


class DataTable(tb.IsDescription):
    bias_voltage = tb.Int32Col(pos=1)
    n_clusters = tb.Float64Col(pos=2)


class SensorSourceScan(SourceScan):
    def _configure(self, module_name='module_0', trigger_latency=100, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        if not self.periphery.enabled:
            raise Exception('Periphery module needs to be enabled!')
        if not (module_name in list(self.periphery.module_devices.keys()) and 'HV' in self.periphery.module_devices[module_name].keys()):
            raise Exception('No sensor bias device defined for {}!'.format(module_name))
        super(SensorSourceScan, self)._configure(trigger_latency=trigger_latency, start_column=start_column, stop_column=stop_column, start_row=start_row, stop_row=stop_row, **_)

    def _scan(self, module_name='module_0', scan_timeout=10, max_triggers=False, bias_voltage=0, hv_current_limit=1e-6, **_):

        self.periphery.power_off_HV(module_name)
        try:
            self.periphery.power_on_HV(module_name, hv_voltage=bias_voltage, hv_current_limit=hv_current_limit)
            self.log.info('Bias current before scan: %e' % float(self.periphery.module_devices[module_name]['HV'].get_current()))

            super(SensorSourceScan, self)._scan(scan_timeout=scan_timeout, max_triggers=max_triggers, **_)
        except Exception as e:
            self.log.error('An error occurred: %s' % e)
        finally:
            self.log.info('Bias current after scan: %e' % float(self.periphery.module_devices[module_name]['HV'].get_current()))
            self.periphery.power_off_HV(module_name)


class MetaBiasScan(object):
    def __init__(self):
        self.log = logger.setup_derived_logger('MetaBiasScan')

        # Need ScanBase __init__ to obtain multi-chip/multi-module variable lists
        # TODO: temporary turn off logging here
        tmp_scan = SensorSourceScan()
        self._output_directories = tmp_scan._output_directories_per_scan
        self._scan_configuration_per_scan = tmp_scan._scan_configuration_per_scan
        del tmp_scan

        self._output_filename = time.strftime("%Y%m%d_%H%M%S") + '_meta_bias_source_scan'

        self._output_filenames = [os.path.join(directory, self._output_filename) for directory in self._output_directories]

        self._h5_files = []
        self._meta_data_tables = []
        self._raw_data_tables = []

        for filename in self._output_filenames:
            h5file = tb.open_file(filename + '.h5', 'w')
            meta_data_table = h5file.create_table(h5file.root, name='meta_data', title='Meta Data', description=MetaDataTable)
            for p, v in scan_configuration.items():
                row = meta_data_table.row
                row['parameter'] = p
                row['value'] = v
                row.append()
            meta_data_table.flush()
            raw_data_table = h5file.create_table(h5file.root, name='data', title='Data', description=DataTable)

            self._h5_files.append(h5file)
            self._meta_data_tables.append(meta_data_table)
            self._raw_data_tables.append(raw_data_table)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.log.error(exc_value)
            self.log.error('Scan failed!')

    def scan(self, module_name='module_0', hv_current_limit=1e-6, VBIAS_start=0, VBIAS_stop=-5, VBIAS_step=-1, **_):
        for bias in range(VBIAS_start, VBIAS_stop, VBIAS_step):
            source_scan_configuration.update({'module_name': module_name, 'bias_voltage': bias, 'hv_current_limit': 1e-6})
            self.log.info('Running source scan at V_bias = %1.0fV' % bias)
            with SensorSourceScan(scan_config=source_scan_configuration) as scn:
                scn.scan()
                n_clusters_dict = scn.analyze()

            # Create sorted list from return dict
            n_clusters_list = []
            for module, results in sorted(n_clusters_dict.items()):
                for chip, result in sorted(results.items()):
                    n_clusters_list.append(result)

            for raw_data_table, n_clusters in zip(self._raw_data_tables, n_clusters_list):
                row = raw_data_table.row
                row['bias_voltage'] = bias
                row['n_clusters'] = n_clusters
                row.append()
                raw_data_table.flush()

        for h5file in self._h5_files:
            h5file.close()

    def analyze(self):
        for filename in self._output_filenames:
            with tb.open_file(filename + '.h5') as in_file:
                parameter_table = in_file.root.meta_data[:]
                data_table = in_file.root.data[:]

            parameters = {}
            for tup in parameter_table:
                key = tup[0].decode('utf-8')
                value = tup[1].decode('utf-8')
                if key in ['start_column', 'stop_column', 'start_row', 'stop_row', 'VBIAS_start', 'VBIAS_stop', 'VBIAS_step']:
                    parameters[key] = int(value)
                else:
                    parameters[key] = value

            x, clusters = [], []
            for hv, n in data_table:
                x.append(hv)
                clusters.append(n)

            fig = plt.figure()
            ax1 = fig.add_subplot(111)
            plt.title('BDAQ53 bias source scan')
            ax1.set_xlabel('Bias voltage [V]')

            ax1.plot(x, clusters, 'o')
            ax1.set_ylabel('# of clusters')

            ax1.grid()
            plt.savefig(filename + '.pdf')


if __name__ == '__main__':
    with MetaBiasScan() as scan:
        scan.scan(**scan_configuration)
        scan.analyze()
