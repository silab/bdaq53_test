#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Collection of functions specific to the RD53B analysis
'''
import numpy as np
import numba
from collections import OrderedDict
import bdaq53.analysis.analysis_utils as au

# Word defines
USERK_FRAME_ID = 0x01000000
HEADER_ID = 0x00010000
NS_ID = 0x00008000
TRIGGER_ID = 0x80000000
TDC_ID_0 = 0x10000000
TDC_ID_1 = 0x20000000
TDC_ID_2 = 0x30000000
TDC_ID_3 = 0x40000000

# Data Masks
BCID_MASK = 0x7FFF
TRG_MASK = 0x7FFFFFFF  # Trigger data (number and/or time stamp)
TDC_HEADER_MASK = 0xF0000000
TDC_TRIG_DIST_MASK = 0x0FF00000
TDC_TIMESTAMP_MASK = 0x0FFFF000
TDC_VALUE_MASK = 0x00000FFF

# Event status bits
E_USER_K = 0x00000001  # event has user K words
E_EXT_TRG = 0x00000002  # event has trigger word from RO system
E_TDC = 0x00000004  # event has TDC word(s) from RO system
# E_BCID_INC_ERROR = 0x00000008  # BCID does not increase as expected
# E_UNKNOWN_TRIGGER_TAG_ERROR = 0x00000010  # TRG ID does not increase by 1
E_NOT_USED = 0x00000020  # not used event error
E_EVENT_TRUNC = 0x00000040  # event data interpretation aborted
E_UNKNOWN_WORD = 0x00000080  # unknown word occured
# Event structure wrong (hit before header or number of data header wrong)
E_STRUCT_WRONG = 0x00000100
E_EXT_TRG_ERR = 0x00000200  # event has external trigger number increase error

# Status bit for TDC; keep this separate in order to set it for each TDC line (per hit)
H_HAS_TDC = 0x00000001  # Hit has TDC word
H_TDC_OVF = 0x00000002  # TDC overflow
H_TDC_AMBIGUOUS = 0x00000004  # unique TDC hit assignment impossible
H_TDC_ERR = 0x00000008  # TDC error


@numba.njit()
def interpret_data(rawdata, hits_array, hist_occ, hist_tot,
                   hist_rel_bcid, hist_trigger_id, hist_event_status,
                   hist_tdc_status, hist_tdc_value, hist_bcid_error,
                   scan_param_id, event_number=0,
                   trig_pattern=0b11111111111111111111111111111111,
                   align_method=0,
                   prev_trig_id=-1, prev_trg_number=-1, analyze_tdc=False,
                   use_tdc_trigger_dist=False, last_chunk=False, rx_id=0, trigger_table_stretched=None):
    offset = 0
    new_tag = False
    new_ccol = False
    new_last_neighbor = False
    new_q_row = False
    new_hmap = False
    new_tot = False

    old_word = 0
    hmap_word = 0
    tag = 0
    ccol = 0
    is_last = 0
    is_neighbor = 0
    q_row_array = [0] * 54
    hits = 0
    tot = np.full(16, -1)
    n_hits = 0

    i = -1
    found_data_start = False

    while i + 1 < len(rawdata):
        i += 1
        word = rawdata[i]
        shift = 16
        # Aurora receiver ID encoded in header must match rx_id of currently analyzed chip
        # if ((word >> 20) & 0xf) != rx_id:
        #     event_status |= au.E_INV_RX_ID
        #     continue
        if (word & au.AURORA_HEADER) == au.USERK_FRAME_ID:  # skip USER_K frame
            # event_status |= au.E_USER_K
            continue
        if rawdata[i] & HEADER_ID:
            word = word & 0xffff
            if rawdata[i] & NS_ID:
                new_tag = True
                new_ccol = False
                new_last_neighbor = False
                new_q_row = False
                new_hmap = False
                new_tot = False
                found_data_start = True
                old_word = 0
            else:
                word = word & 0x7fff
                if old_word == 0:
                    word += 2 ** 15
                shift = 15
        if _get_length(old_word) < 47 and found_data_start:
            word = (old_word << shift) + word
            if _get_length(word) < 16 and not rawdata[i] & HEADER_ID:
                word += 2 ** 16
        elif (word == 0) and new_ccol == 0:
            continue
        else:
            word = old_word
            i -= 1

        if new_tag:
            if _get_length(word) < 8:
                old_word = word
                continue
            tag = _get_msb_num(word, 8)
            word = _rm_msb(word, 8)
            new_tag = False
            new_ccol = True
            new_last_neighbor = False
            new_q_row = False
            new_hmap = False
            new_tot = False

        if new_ccol:
            if _get_length(word) < 6:
                old_word = word
                continue
            ccol_buf = _get_msb_num(word, 6)
            if ccol_buf >= 56:
                word = _rm_msb(word, 3)
                old_word = word
                new_tag = True
                new_ccol = False
                new_last_neighbor = False
                new_q_row = False
                new_hmap = False
                new_tot = False
                continue
            elif ccol_buf == 0:
                found_data_start = False
                old_word = 0
                new_tag = False
                new_ccol = False
                new_last_neighbor = False
                new_q_row = False
                new_hmap = False
                new_tot = False
                continue
            else:
                word = _rm_msb(word, 6)
                ccol = ccol_buf
                old_word = word
                new_tag = False
                new_ccol = False
                new_last_neighbor = True
                new_q_row = False
                new_hmap = False
                new_tot = False
        if new_last_neighbor:
            if _get_length(word) < 3:
                old_word = word
                continue
            is_last = _get_msb_num(word, 1)
            word = _rm_msb(word, 1)
            is_neighbor = _get_msb_num(word, 1)
            word = _rm_msb(word, 1)
            if is_neighbor == 1:
                new_hmap = True
                new_last_neighbor = False
                old_word = word
                continue
            if is_neighbor == 0:
                new_q_row = True
                new_last_neighbor = False
                old_word = word
                continue
        if new_q_row:
            if _get_length(word) < 8:
                old_word = word
                continue
            q_row = _get_msb_num(word, 8)
            q_row_array[ccol] = q_row
            word = _rm_msb(word, 8)
            new_q_row = False
            new_hmap = True
        if new_hmap:
            tot = np.full(16, -1)
            tot_cnt = 0
            if _get_length(word) < 30:
                old_word = word
                continue
            word, hmap_word, hits = _hmap_raw(word)
            new_hmap = False
            new_tot = True
        if new_tot:
            for x in range(hits):
                n_hits += 1
                if _get_length(word) < 4:
                    break
                tot_val = _get_msb_num(word, 4)
                word = _rm_msb(word, 4)
                hits -= 1
                tot[tot_cnt] = tot_val
                tot_cnt += 1
            if hits == 0:
                new_tot = False
                old_word = word
                tot = tot[:tot_cnt]
                offset, q_row_array[ccol] = _pre_build_event(hits_array, offset, hist_occ, hist_tot, hist_rel_bcid,
                                                             hist_trigger_id,
                                                             hist_event_status, 0,
                                                             scan_param_id, event_number, 0, tag, ccol, is_neighbor,
                                                             q_row_array[ccol],
                                                             hmap_word, tot, hist_tdc_status, hist_tdc_value)

                continue
            if _get_length(word) < 4:
                old_word = word
                continue
        if is_last == 1:
            new_ccol = True
            old_word = word
            continue
        elif is_last == 0:
            new_last_neighbor = True
            old_word = word
            continue
    return n_hits, event_number + 1, 1, 1, 1


@numba.njit()
def _pre_build_event(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                     hist_event_status,
                     start_bcid,
                     scan_param_id, event_status, tdc_status_buffer, tag, ccol, is_neighbor, q_row, hmap_word, tot,
                     hist_tdc_status, hist_tdc_value):
    hits_buf = np.zeros_like(hits)[0:16]
    hit_buffer_i = len(tot)
    hit_buffer, q_row = _build_hits(hits_buf, tag, ccol, is_neighbor, q_row, hmap_word, tot)
    hit_buffer = hit_buffer[:len(tot)]
    data_out_i = au.build_event(hits, data_out_i,
                                hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                hist_event_status, hist_tdc_status, hist_tdc_value,
                                hit_buffer, hit_buffer_i, start_bcid,
                                scan_param_id, event_status, tdc_status_buffer)
    return data_out_i, q_row


@numba.njit()
def _hmap_raw(word):
    hits = 0
    vertical_indicator = _get_msb_num(word, 2)
    if vertical_indicator in [0, 1]:
        vertical_indicator_start = 1
        vertical_indicator_stop = 2
        word = _rm_msb(word, 1)
    elif vertical_indicator in [2]:
        vertical_indicator_start = 0
        vertical_indicator_stop = 1
        word = _rm_msb(word, 2)
    elif vertical_indicator in [3]:
        vertical_indicator_start = 0
        vertical_indicator_stop = 2
        word = _rm_msb(word, 2)
    store_array = [1, 1, 1, 1, 1, 1]
    for vertical_hits in range(vertical_indicator_start, vertical_indicator_stop):
        start_val = _get_msb_num(word, 2)
        if start_val in [0, 1]:
            start_val = 1
            word = _rm_msb(word, 1)
        else:
            word = _rm_msb(word, 2)
        start_val += 4
        store_array[vertical_hits * 3] = start_val
        for i in range(1, 4):
            buf = store_array[vertical_hits * 3 + i - 1]
            for _ in range(_get_length(store_array[vertical_hits * 3 + i - 1])):
                j = _get_msb_num(buf, 1)
                buf = _rm_msb(buf, 1)
                if i < 3:
                    if j == 0:
                        store_array[vertical_hits * 3 + i] = store_array[vertical_hits * 3 + i] << 2
                    if j == 1:
                        word_val = _get_msb_num(word, 2)
                        if word_val in [0, 1]:
                            store_array[vertical_hits * 3 + i] = (store_array[vertical_hits * 3 + i] << 2) + 1
                            word = _rm_msb(word, 1)
                        else:
                            store_array[vertical_hits * 3 + i] = (store_array[vertical_hits * 3 + i] << 2) + _get_msb_num(word, 2)
                            word = _rm_msb(word, 2)
                else:
                    if j == 1:
                        hits += 1
    if vertical_indicator_start == 1:
        hword = ((store_array[2]) << 16) + (store_array[5] & 0xff)
    elif vertical_indicator_stop == 1:
        hword = ((store_array[2]) << 8)
    else:
        hword = ((store_array[2]) << 8) + (store_array[5] & 0xff)
    return word, hword, hits


@numba.njit()
def _count_ones(word):
    one_cnt = 0
    word_cp = word
    for cnt in range(_get_length(word_cp)):
        if _get_msb_num(word_cp, 1) == 1:
            one_cnt += 1
        word_cp = _rm_msb(word_cp, 1)
    return one_cnt


@numba.njit()
def _add_msb(word, shift, val=0):
    length = _get_length(word)
    ret = 2 ** (length + shift) + word - 2 ** length
    ret += val << length
    return ret


@numba.njit()
def _rm_msb(word, shift):
    length = _get_length(word)
    word_buf = word << shift
    word_buf = word_buf & (0b1 * 2 ** (length + 1) - 1)
    word_buf = word_buf >> (shift)
    if _get_length(word_buf) < length - shift:
        word_buf = word_buf + 2 ** (length - shift)  # + 1)
    return word_buf


@numba.njit()
def _get_msb_num(word, shift):
    length = _get_length(word)
    word_buf = word >> (length - shift)
    word_buf = word_buf - 2 ** (shift)
    return word_buf


@numba.njit()
def _get_length(word):
    ret = 0
    word2 = word
    while word2:
        word2 = word2 >> 1
        ret += 1
    if ret != 0:
        ret -= 1
    # if word != 0:
    #     ret = int(np.log(word) / np.log(2))
    # else:
    #     ret = 0
    return ret


def _open_h5_file(fname):
    import tables as tb
    with tb.open_file(fname) as in_file:
        words = in_file.root.raw_data[:]
    return words


def _dump_print(content):
    full_str = ''
    cnt = 1
    cnt2 = 0
    for i in content:
        word = bin(i)[2:].zfill(16)
        if len(bin(i)[2:]) == 17:
            word = word[2:].zfill(16)
        full_str += word
        if cnt == 4:
            full_str += ' ' + str(len(full_str)) + ' '
            cnt = 0
        cnt += 1
        cnt2 += 1
    print(full_str)
    return full_str


@numba.njit()
def _build_hits(hits, tag, ccol, is_neighbor, q_row, hmap_word, tot):
    hit_cnt = 0
    if is_neighbor == 1:
        q_row += 1
    for cnt in range(_get_length(hmap_word)):
        hit = _get_msb_num(hmap_word, 1)
        hmap_word = _rm_msb(hmap_word, 1)
        col = (ccol - 1) * 8 + cnt
        row = q_row * 2
        if cnt > 7:
            col -= 8
            row += 1
        if hit == 1:
            hits[hit_cnt]['event_number'] = 0
            hits[hit_cnt]['ext_trg_number'] = 0
            hits[hit_cnt]['trigger_id'] = tag
            hits[hit_cnt]['bcid'] = tag
            hits[hit_cnt]['rel_bcid'] = tag
            hits[hit_cnt]['col'] = col
            hits[hit_cnt]['row'] = row
            hits[hit_cnt]['tot'] = tot[hit_cnt]
            hits[hit_cnt]['scan_param_id'] = 1
            hits[hit_cnt]['trigger_tag'] = tag
            hits[hit_cnt]['event_status'] = 1
            hit_cnt += 1
    return hits, q_row


def analyze_chunk(rawdata, return_hists=('HistOcc',), return_hits=False,
                  scan_param_id=0,
                  trig_pattern=0b11111111111111111111111111111111,
                  align_method=0,
                  analyze_tdc=False,
                  use_tdc_trigger_dist=False,
                  rx_id=0,
                  ext_trig_id_map=[-1]):
    ''' Helper function to quickly analyze a data chunk.

        Warning
        -------
            If the rawdata contains incomplete event data only data that do
            not need event building are correct (occupancy + tot histograms)

        Parameters
        ----------
        rawdata : np.array, 32-bit dtype
            The raw data containing FE, trigger and TDC words
        return_hists : iterable of strings
            Names to select the histograms to return. Is not case sensitive
            and string must contain only e.g.: occ, tot, bcid, event
        return_hits : boolean
            Return the hit array
        scan_param_id : integer
            Set scan par id in hit info table
        trig_pattern : integer (32-bits)
            Indicate the position of the sub-triggers. Needed to check
            the BCIDs.
        align_method : integer
            Methods to do event alignment
            0: New event when number if event headers exceeds number of
               sub-triggers. Many fallbacks for corrupt data implemented.
            1: New event when data word is TLU trigger word, with error checks
            2: Force new event always at TLU trigger word, no error checks
        analyze_tdc : boolean
            If analyze_tdc is True, interpret and analyze also TDC words. Default is False,
            meaning that TDC analysis is skipped. This is useful for scans which do no
            require an TDC word interpretation (e.g. threshold scan) in order to save time.
        use_tdc_trigger_dist : boolean
            If True use trigger distance (delay between Hitor and Trigger) in TDC word
            interpretation. If False use instead TDC timestamp from TDC word. Default
            is False.

        Usefull for tuning. Analysis per scan parameter not possible.
        Chunks should not be too large.

        Returns
        -------
            ordered dict: With key = return_hists string, value = data
            and first entry are hits when selected
    '''

    n_hits = rawdata.shape[0] * 4
    (hits, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id, hist_event_status, hist_tdc_status,
     hist_bcid_error) = au.init_outs(n_hits, n_scan_params=1, rows=384)

    interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_trigger_id, hist_event_status,
                   hist_bcid_error=hist_bcid_error,
                   scan_param_id=scan_param_id, event_number=0,
                   trig_pattern=trig_pattern,
                   align_method=align_method,
                   prev_trig_id=-1,
                   analyze_tdc=analyze_tdc,
                   use_tdc_trigger_dist=use_tdc_trigger_dist,
                   last_chunk=True, hist_tdc_status=hist_tdc_status)

    hists = {'occ': hist_occ,
             'tot': hist_tot,
             'rel': hist_rel_bcid,
             'event': hist_event_status,
             'error': hist_bcid_error
             }

    ret = OrderedDict()

    if return_hits:
        ret['hits'] = hits

    for key, value in hists.items():
        for word in return_hists:
            if key in word.lower():
                ret[word] = value
                break

    return ret


def interpret_userk_data(rawdata):
    userk_data = np.zeros(shape=rawdata.shape[0],
                          dtype={
                              'names': ['ChipID', 'AuroraKWord', 'Status', 'Data1', 'Data1_AddrFlag', 'Data1_Addr',
                                        'Data1_Data',
                                        'Data0', 'Data0_AddrFlag', 'Data0_Addr', 'Data0_Data'],
                              'formats': ['uint8', 'uint8', 'uint8', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16',
                                          'uint16',
                                          'uint16', 'uint16']})
    userk_word_cnt = 0
    userk_data_i = 0

    for word in rawdata:
        if (word & au.USERK_FRAME_ID):
            if userk_word_cnt == 0:
                userk_word = word & 0x0fff
                userk_data[userk_data_i]['Status'] = (word >> 12) & 0x3
                userk_data[userk_data_i]['ChipID'] = (word >> 14) & 0x3
            else:
                userk_word = userk_word << 16 | word & 0xffff
            userk_word_cnt += 1
            if userk_word_cnt == 4:
                userk_data[userk_data_i]['AuroraKWord'] = userk_word & 0xff
                Data1 = (userk_word >> 34) & 0x7ffffff
                Data0 = (userk_word >> 8) & 0x7ffffff
                userk_data[userk_data_i]['Data1'] = Data1
                userk_data[userk_data_i]['Data1_AddrFlag'] = (Data1 >> 25) & 0x1
                userk_data[userk_data_i]['Data1_Addr'] = (Data1 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data1_Data'] = (Data1 >> 0) & 0xffff
                userk_data[userk_data_i]['Data0'] = Data0
                userk_data[userk_data_i]['Data0_AddrFlag'] = (Data0 >> 25) & 0x1
                userk_data[userk_data_i]['Data0_Addr'] = (Data0 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data0_Data'] = (Data0 >> 0) & 0xffff
                userk_data_i += 1
                userk_word_cnt = 0

    return userk_data[:userk_data_i]
