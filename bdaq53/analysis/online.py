#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Online data analysis functions
'''

import ctypes
import logging
import multiprocessing
import time
import queue

import numpy as np
import numba

from bdaq53.analysis import analysis_utils as au

logger = logging.getLogger('OnlineAnalysis')


@numba.njit
def histogram(raw_data, occ_hist, data_word, is_fe_high_word, is_data_header):
    ''' Raw data to 2D occupancy histogram '''

    for word in raw_data:
        if word & au.TRIGGER_HEADER:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_0:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_1:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_2:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_3:
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.USERK_FRAME_ID:     # skip USER_K frame
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.HEADER_ID:          # data header
            is_data_header = 1
            is_fe_high_word = 1
        # Can only interpret FE words if high word is found, high word can
        # only be found in event header since event hit records do not
        # have a header
        elif is_fe_high_word < 0:
            continue
        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if is_fe_high_word == 1:
            data_word = word & 0xffff
            is_fe_high_word = 0  # Next is low word
            continue  # Low word still missing
        elif is_fe_high_word == 0:
            data_word = data_word << 16 | word & 0xffff
            is_fe_high_word = 1  # Next is high word

        if is_data_header == 1:
            # After data header follows header less hit data
            is_data_header = 0
        else:  # data word is hit data
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff
            for i in range(4):
                col, row = au.translate_mapping(multicol, region, i)
                tot = (data_word >> i * 4) & 0xf

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        occ_hist[col, row] += 1
                        # print np.count_nonzero(occ_hist)
                        # print occ_hist.sum()
                else:  # unknown word
                    continue

    return data_word, is_fe_high_word, is_data_header


@numba.njit
def histogram_tot(raw_data, hist_tot, data_word, is_fe_high_word, is_data_header):
    ''' Raw data to TOT histogram for each pixel'''
    for word in raw_data:
        if word & au.TRIGGER_HEADER:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_0:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_1:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_2:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_3:
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.USERK_FRAME_ID:     # skip USER_K frame
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.HEADER_ID:          # data header
            is_data_header = 1
            is_fe_high_word = 1
        # Can only interpret FE words if high word is found, high word can
        # only be found in event header since event hit records do not
        # have a header
        elif is_fe_high_word < 0:
            continue
        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if is_fe_high_word == 1:
            data_word = word & 0xffff
            is_fe_high_word = 0  # Next is low word
            continue  # Low word still missing
        elif is_fe_high_word == 0:
            data_word = data_word << 16 | word & 0xffff
            is_fe_high_word = 1  # Next is high word

        if is_data_header == 1:
            # After data header follows header less hit data
            is_data_header = 0
        else:  # data word is hit data
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff
            for i in range(4):
                col, row = au.translate_mapping(multicol, region, i)
                tot = (data_word >> i * 4) & 0xf

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        hist_tot[col, row, tot] += 1
                        # print np.count_nonzero(occ_hist)
                        # print occ_hist.sum()
                else:  # unknown word
                    continue

    return data_word, is_fe_high_word, is_data_header


class OccupancyHistogramming(object):
    ''' Fast histogramming of raw data to a 2D hit histogramm

        No event building and seperate process for speed up
    '''
    _queue_timeout = 0.01  # max blocking time to delete object [s]

    def __init__(self):
        self._raw_data_queue = multiprocessing.Queue()
        self.stop = multiprocessing.Event()
        self.lock = multiprocessing.Lock()

        # Create shared memory 32 bit unsigned int numpy array
        shared_array_base = multiprocessing.Array(ctypes.c_uint, 400 * 192)
        shared_array = np.ctypeslib.as_array(shared_array_base.get_obj())
        self.occ_hist = shared_array.reshape(400, 192)

        self.p = multiprocessing.Process(target=self.worker,
                                         args=(self._raw_data_queue, shared_array_base,
                                               self.lock, self.stop, ))
        self.p.start()

    def add(self, raw_data):
        ''' Add raw data to be histogrammed '''
        self._raw_data_queue.put(raw_data)

    def reset(self, wait=True, timeout=0.5):
        ''' Reset histogram '''
        if not wait:
            if self._raw_data_queue.qsize() != 0:
                logger.warning('Resetting histogram while adding data')
        else:
            n_time = 0
            while self._raw_data_queue.qsize() != 0:
                time.sleep(0.01)
                n_time += 1
                if n_time * 0.01 > timeout:
                    logger.warning('Resetting histogram while adding data')
                    break
        with self.lock:
            # No overwrite with a new zero array due to shared memory
            for col in range(400):
                for row in range(192):
                    self.occ_hist[col, row] = 0

    def get(self, wait=True, timeout=0.5, reset=True):
        ''' Get the result histogram '''
        if not wait:
            if self._raw_data_queue.qsize() != 0:
                logger.warning('Getting histogram while adding data')
        else:
            n_time = 0
            while self._raw_data_queue.qsize() != 0:
                time.sleep(0.01)
                n_time += 1
                if n_time * 0.01 > timeout:
                    logger.warning('Getting histogram while adding data')
                    break
        with self.lock:
            if reset:
                occ_hist = self.occ_hist.copy()
                # No overwrite with a new zero array due to shared memory
                for col in range(400):
                    for row in range(192):
                        self.occ_hist[col, row] = 0
                return occ_hist
            else:
                return self.occ_hist

    def worker(self, raw_data_queue, shared_array_base, lock, stop):
        ''' Histogramming in seperate process '''
        occ_hist = np.ctypeslib.as_array(shared_array_base.get_obj()).reshape(400, 192)
        is_fe_high_word = -1
        is_data_header = 0
        data_word = 0
        while not stop.is_set():
            try:
                raw_data = raw_data_queue.get(timeout=self._queue_timeout)
                with lock:
                    data_word, is_fe_high_word, is_data_header = histogram(raw_data, occ_hist, data_word, is_fe_high_word, is_data_header=is_data_header)
            except queue.Empty:
                continue
            except KeyboardInterrupt:   # Need to catch KeyboardInterrupt from main process
                stop.set()

    def __del__(self):
        self._raw_data_queue.close()
        self._raw_data_queue.join_thread()  # Needed otherwise IOError: [Errno 232] The pipe is being closed
        self.stop.set()
        self.p.join()


class TotHistogramming(object):
    ''' Fast histogramming of raw data to a TOT histogramm for each pixel

        No event building and seperate process for speed up
    '''
    _queue_timeout = 0.01  # max blocking time to delete object [s]

    def __init__(self):
        self._raw_data_queue = multiprocessing.Queue()
        self.stop = multiprocessing.Event()
        self.lock = multiprocessing.Lock()

        # Create shared memory 32 bit unsigned int numpy array
        shared_array_base = multiprocessing.Array(ctypes.c_uint, 400 * 192 * 16)
        shared_array = np.ctypeslib.as_array(shared_array_base.get_obj())
        self.tot_hist = shared_array.reshape(400, 192, 16)

        self.p = multiprocessing.Process(target=self.worker,
                                         args=(self._raw_data_queue, shared_array_base,
                                               self.lock, self.stop, ))
        self.p.start()

    def add(self, raw_data):
        ''' Add raw data to be histogrammed '''
        self._raw_data_queue.put(raw_data)

    def reset(self, wait=True, timeout=0.5):
        ''' Reset histogram '''
        if not wait:
            if self._raw_data_queue.qsize() != 0:
                logger.warning('Resetting histogram while adding data')
        else:
            n_time = 0
            while self._raw_data_queue.qsize() != 0:
                time.sleep(0.01)
                n_time += 1
                if n_time * 0.01 > timeout:
                    logger.warning('Resetting histogram while adding data')
                    break
        with self.lock:
            # No overwrite with a new zero array due to shared memory
            for col in range(400):
                for row in range(192):
                    for tot in range(16):
                        self.tot_hist[col, row, tot] = 0

    def get(self, wait=True, timeout=0.5, reset=True):
        ''' Get the result histogram '''
        if not wait:
            if self._raw_data_queue.qsize() != 0:
                logger.warning('Getting histogram while adding data')
        else:
            n_time = 0
            while self._raw_data_queue.qsize() != 0:
                time.sleep(0.01)
                n_time += 1
                if n_time * 0.01 > timeout:
                    logger.warning('Getting histogram while adding data')
                    break
        with self.lock:
            if reset:
                tot_hist = self.tot_hist.copy()
                # No overwrite with a new zero array due to shared memory
                for col in range(400):
                    for row in range(192):
                        for tot in range(16):
                            self.tot_hist[col, row, tot] = 0
                return tot_hist
            else:
                return self.tot_hist

    def worker(self, raw_data_queue, shared_array_base, lock, stop):
        ''' Histogramming in seperate process '''
        tot_hist = np.ctypeslib.as_array(shared_array_base.get_obj()).reshape(400, 192, 16)
        is_fe_high_word = -1
        is_data_header = 0
        data_word = 0
        while not stop.is_set():
            try:
                raw_data = raw_data_queue.get(timeout=self._queue_timeout)
                with lock:
                    data_word, is_fe_high_word, is_data_header = histogram_tot(raw_data, tot_hist, data_word, is_fe_high_word, is_data_header=is_data_header)
            except queue.Empty:
                continue
            except KeyboardInterrupt:   # Need to catch KeyboardInterrupt from main process
                stop.set()

    def __del__(self):
        self._raw_data_queue.close()
        self._raw_data_queue.join_thread()  # Needed otherwise IOError: [Errno 232] The pipe is being closed
        self.stop.set()
        self.p.join()


if __name__ == "__main__":
    pass
