#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import time
import multiprocessing
import os
import yaml
import inspect
import zmq
import tables as tb
import numpy as np

from slack import WebClient
from tables.exceptions import NodeError
from contextlib import contextmanager
from collections import OrderedDict
from online_monitor.utils import utils
from collections import defaultdict
from copy import deepcopy

from bdaq53.system import logger
from bdaq53.system.bdaq53 import BDAQ53
from bdaq53.chips.rd53a import RD53A
from bdaq53.chips.ITkPixV1 import ITkPixV1
from bdaq53.system.periphery import BDAQ53Periphery
from bdaq53.system.fifo_readout import FifoReadout, ReadoutChannel
from bdaq53.utils import recursive_update, recursive_update_deep, get_software_version, get_latest_maskfile, get_latest_chip_configuration_file
from bdaq53.analysis import analysis_utils as au


def load_bench_config(config=None):
    try:
        if config is None:
            config = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'bdaq53' + os.sep + 'testbench.yaml')
        with open(config) as f:
            cfg = yaml.full_load(f)
    except TypeError:
        cfg = config

    return cfg


def send_data(socket, data, scan_par_id, name='ReadoutData'):
    '''Sends the data of every read out (raw data and meta data)

        via ZeroMQ to a specified socket.
        Uses a serialization provided by the online_monitor package
    '''

    data_meta_data = dict(
        name=name,
        timestamp_start=data[1],    # float
        timestamp_stop=data[2],     # float
        error=data[3],              # int
        scan_par_id=scan_par_id
    )
    try:
        data_ser = utils.simple_enc(data[0], meta=data_meta_data)
        socket.send(data_ser, flags=zmq.NOBLOCK)
    except zmq.Again:
        pass


class MetaTable(tb.IsDescription):
    index_start = tb.UInt32Col(pos=0)
    index_stop = tb.UInt32Col(pos=1)
    data_length = tb.UInt32Col(pos=2)
    timestamp_start = tb.Float64Col(pos=3)
    timestamp_stop = tb.Float64Col(pos=4)
    scan_param_id = tb.UInt32Col(pos=5)
    error = tb.UInt32Col(pos=6)
    trigger = tb.Float64Col(pos=7)


class MapTable(tb.IsDescription):
    cmd_number_start = tb.UInt32Col(pos=0)
    cmd_number_stop = tb.UInt32Col(pos=1)
    cmd_length = tb.UInt32Col(pos=2)
    scan_param_id = tb.UInt32Col(pos=3)


class RunConfigTable(tb.IsDescription):
    attribute = tb.StringCol(64)
    value = tb.StringCol(512)


class ChipStatusTable(tb.IsDescription):
    attribute = tb.StringCol(64, pos=0)
    ADC = tb.UInt16Col(pos=1)
    value = tb.Float64Col(pos=2)


class RegisterTable(tb.IsDescription):
    register = tb.StringCol(64)
    value = tb.UInt16Col()


class PowersupplyTable(tb.IsDescription):
    powersupply = tb.StringCol(128, pos=0)
    voltage = tb.Float64Col(pos=1)
    current = tb.Float64Col(pos=2)


class ScanBase(object):
    '''
        Basic run meta class.
        Base class for scan- / tune- / analyze-class.
    '''

    is_parallel_scan = False    # Parallel readout of ExtTrigger-type scans etc.; must be overridden in the derived classes if needed

    class ScanDataContainer:
        '''
        Store any additional variables/data created by each scan
        that shall be shared between the _configure(), _scan() and _analyze()
        functions here instead of tampering with ScanBase's instance variables (self.*)
        Use multiple ScanDataContainer instances to deal with multi-chip/-module scans.
        '''

        def __init__(self):
            return

    def __init__(self, bdaq_conf=None, bench_config=None, scan_config={}, scan_config_override_per_scan=None, scan_scope='per_chip', record_chip_status=True, powercycle_modules=False):
        '''
            Initializer.

            Parameters:
            ----------
            bdaq_conf : str, dict or file
                    Readout board configuration (configuration as dict or file or its filename as string)

            bench_config : str or dict
                    Testbench configuration (configuration as dict or its filename as string)

            scan_config : dict
                    Dictionary of scan parameters. These can be complemented/overwritten per chip by scan_config_override_per_scan, see below.
                    The scan parameters will be passed to the _configure() and _scan() functions as expanded kwargs.

                    If the dictionary contains a key named 'chip' then the corresponding value (which should be a dict with format/structure
                    compatible to the 'rd53x_default.cfg.yaml') is used to complement/overwrite the chip configuration(s)
                    loaded from the chip_configuration file(s) specified in the testbench configuration.

                    If the dictionary contains a key named 'bdaq' then the corresponding value (which should be a dict with format/structure
                    compatible to the 'testbench.yaml') is used to complement/overwrite the testbench configuration loaded from 'testbench.yaml'.

            scan_config_override_per_scan : dict
                    Dictionary of modules/chips (format similar as in 'testbench.yaml') containing additional scan parameters
                    for each chip (or module, if scan_scope=='per_module') to be scanned. For each chip(module) the parameters
                    may be provided as a dictionary. Example: {'module_0': {'fe_0': {'par1': ..., ...},
                                                                            'fe_1': {'par1': ..., ...}},
                                                               'module_1': {...}, ...}
                    For each individual scan these parameters will be automatically merged with scan_config.
                    A 'chip' dict as additional parameter will complement/overwrite a 'chip' dict given in scan_config for the corresponding chip.

            scan_scope : str
                    If 'per_module': Scan must be executed for each module only and correspondingly no self.chip is provided for the _scan() function.
                                     However, all chips from testbench configuration will be initialized in order to ensure they are all connected and powered.
                                     Output files are placed in the 'module directory'. For single chip modules this is simply the chip's directory.
                    Otherwise: Scan for each chip on each module as usual.

            record_chip_status : boolean
                    Add chip statuses to the output files after the scan
        '''
        # Allow changes without changing originals
        if isinstance(bdaq_conf, dict):
            bdaq_conf = deepcopy(bdaq_conf)
        if isinstance(bench_config, dict):
            bench_config = deepcopy(bench_config)
        scan_config = deepcopy(scan_config)
        if isinstance(scan_config_override_per_scan, dict):
            scan_config_override_per_scan = deepcopy(scan_config_override_per_scan)

        # Perform scan not per chip but only once per module?
        self.scan_per_module = False
        if scan_scope == 'per_module':
            self.scan_per_module = True

        self.record_chip_status = record_chip_status

        self.timestamp = time.strftime("%Y%m%d_%H%M%S")
        self.run_name = self.timestamp + '_' + self.scan_id
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        self.fifo_readout_dict = {}  # Store raw data readout stuff for each scan

        self.ana_proc = None  # analysis process

        # Powercycle modules before starting scan
        self.powercycle_modules = powercycle_modules

        # Setup logger
        self.log = logger.setup_derived_logger(self.__class__.__name__)

        # Configuration dict for testbench and scan configuration
        self.configuration = au.ConfigDict()

        # Load testbench configuration
        try:
            if bench_config is None:
                bench_config = os.path.join(self.proj_dir, 'testbench.yaml')
            with open(bench_config) as f:
                self.configuration['bench'] = yaml.full_load(f)
        except TypeError:
            self.configuration['bench'] = bench_config

        # Update testbench configuration from scan configuration
        recursive_update(self.configuration['bench'], scan_config.pop('bdaq', {}))

        # Setup Slack notifications
        if not self.configuration['bench']['notifications']['enable_notifications']:
            self.slack = None
        else:
            if os.path.isfile(os.path.expanduser(self.configuration['bench']['notifications']['slack_token'])):
                with open(os.path.expanduser(self.configuration['bench']['notifications']['slack_token']), 'r') as token_file:
                    token = token_file.read().strip()
            else:
                token = self.configuration['bench']['notifications']['slack_token']
            self.slack = WebClient(token)

        # Chip configuration dict. Updates the configuration loaded from chip-yaml-files specified in testbench.yaml
        self._chip_configuration_override_global = scan_config.pop('chip', {})

        # Define scan configuration used for all scans.
        # Load scan configuration from default arguments of _configure() and _scan() and overwrite with scan_config.
        args = inspect.getfullargspec(self._configure)
        self._scan_configuration_global = {key: args[3][id] for id, key in enumerate(args[0][1:])}
        args = inspect.getfullargspec(self._scan)
        self._scan_configuration_global.update({key: args[3][id] for id, key in enumerate(args[0][1:])})
        self._scan_configuration_global.update(scan_config)

        # Scan configuration can be complemented or overridden for each chip (or module) individually?
        override_params = False
        if scan_config_override_per_scan is not None:
            override_params = True

        # Prepare output directories for each module/chip in testbench configuration
        self.log.debug('Prepare output directories.')
        self._setup_output_directories()

        # Search for configuration files for each chip
        self.log.debug('Search chip configuration files.')
        self._search_chip_configurations()

        # Lists of variables that can be different for each individual scan (or chip)

        self._chips = []            # Must create separate RD53x instance for each chip in testbench configuration
        self._chips_confs = []      # Separate configuration dict for each chip as well
        self._modules_confs = []    # Store configuration dicts for each module in case of a 'per module' scan

        self._mask_confs = []       # Copy original mask settings of loaded chip configuration files in order not to overwrite these when saving to .cfg.yaml files

        self._log_handlers_per_scan = []            # Log handlers
        self._output_directories_per_scan = []      # Output directories (see also _setup_output_directories())
        self._scan_configuration_per_scan = []      # Scan parameters
        self._chip_configuration_per_chip = []      # Chip configurations (also needed for 'per module' scan)

        self._scan_data_containers = []             # Store data to be shared between _configure()/_scan()/_analyze() for each individual scan

        # General description of all modules and chips connected to the readout board
        self.modules_conf = self.configuration['bench']['modules']

        # Merge scan configurations / chip configurations from the scan script that are intended to be the same
        # for all chips (modules) with those that are intended to be specific per chip (module).
        # Also setup log file handlers, power supply configurations etc.

        # Distinguish two cases:
        # --> Default: Run scan for every chip. Loop over modules and chips and add chip configurations to list
        if not self.scan_per_module:

            for module, chips_conf in sorted(self.modules_conf.items()):
                for key, chip_conf in sorted(chips_conf.items()):
                    if key.startswith('_'):     # Skip additional information dynamically added by ScanBase
                        continue
                    self._modules_confs.append({'_module_name': module})
                    self._chips_confs.append(chip_conf)

                    # Remember output directory for current chip
                    self._output_directories_per_scan.append(chip_conf['_out_path'])

                    # Process scan configuration and chip configuration for current chip.
                    # Enable overwriting of masks configuration from chip configuration by scan configuration
                    # (e.g. in order to ensure maskfile=None for a TDAC tuning etc.).
                    if not override_params:
                        params = deepcopy(self._scan_configuration_global)
                        additional_chip_conf = deepcopy(self._chip_configuration_override_global)

                        if 'masks' not in additional_chip_conf.keys():
                            additional_chip_conf['masks'] = {}
                        if 'maskfile' in params.keys():
                            additional_chip_conf['masks']['maskfile'] = params['maskfile']
                        if 'use_good_pixels_diff' in params.keys():
                            additional_chip_conf['masks']['use_good_pixels_diff'] = params['use_good_pixels_diff']

                        self._scan_configuration_per_scan.append(deepcopy(params))
                        self._chip_configuration_per_chip.append(deepcopy(additional_chip_conf))
                    else:
                        # Scan configuration specific to each chip
                        additional_params = scan_config_override_per_scan.get(chip_conf['_module_name'], {}).get(chip_conf['_chip_name'], {})
                        params = recursive_update_deep(first=self._scan_configuration_global, second=additional_params)

                        # Chip configuration dict from scan parameters. Specific to each chip, updates _chip_configuration_override_global
                        additional_chip_conf = params.pop('chip', {})
                        additional_chip_conf = recursive_update_deep(first=self._chip_configuration_override_global, second=additional_chip_conf)

                        if 'masks' not in additional_chip_conf.keys():
                            additional_chip_conf['masks'] = {}
                        if 'maskfile' in params.keys():
                            additional_chip_conf['masks']['maskfile'] = params['maskfile']
                        if 'use_good_pixels_diff' in params.keys():
                            additional_chip_conf['masks']['use_good_pixels_diff'] = params['use_good_pixels_diff']

                        self._scan_configuration_per_scan.append(deepcopy(params))
                        self._chip_configuration_per_chip.append(deepcopy(additional_chip_conf))

                    # Scan data
                    self._scan_data_containers.append(self.ScanDataContainer())

                    # Create log file handler for the current chip
                    chip_fh = self._create_logfile_handler(os.path.join(chip_conf['_out_path'], self.run_name))
                    self._log_handlers_per_scan.append(chip_fh)

        # --> When scan is a 'per module' scan, though: Loop over modules only and add a single "chip"-entry for each module
        else:
            # Do not need maskfiles
            self._chip_configuration_override_global['masks'] = {'maskfile': None}

            # In case of 'per module' scan the loop for creating chip objects
            # (see below) still needs log handler entries etc. for every chip.
            self._module_log_handlers_per_chip = []
            self._module_scan_configuration_per_chip = []

            for module, chips_conf in sorted(self.modules_conf.items()):
                # Remember output directory for current module
                self._output_directories_per_scan.append(chips_conf['_out_path'])

                # Process scan configuration
                if not override_params:
                    params = deepcopy(self._scan_configuration_global)
                else:
                    additional_params = scan_config_override_per_scan.get(chips_conf['_module_name'], {})
                    params = recursive_update_deep(first=self._scan_configuration_global, second=additional_params)
                self._scan_configuration_per_scan.append(params)

                # Scan data
                self._scan_data_containers.append(self.ScanDataContainer())

                # Create log file handler for the current module.
                module_fh = self._create_logfile_handler(os.path.join(chips_conf['_out_path'], self.run_name))
                self._log_handlers_per_scan.append(module_fh)

                # Need to at least initialize chips from testbench also for 'per module' scan
                for key, chip_conf in sorted(chips_conf.items()):

                    if key.startswith('_'):        # Skip additional information dynamically added by ScanBase
                        continue
                    self._chips_confs.append(chip_conf)

                    self._chip_configuration_per_chip.append(deepcopy(self._chip_configuration_override_global))    # No need to have different configuration per chip
                    self._module_log_handlers_per_chip.append(module_fh)
                    self._module_scan_configuration_per_chip.append(params)

        # Check if data sending address/port is different for each chip
        if not self.scan_per_module:
            send_data_addresses = []
            for chip_conf in self._chips_confs:
                address = chip_conf['send_data']
                if address is not None:
                    if address not in send_data_addresses:
                        send_data_addresses.append(address)
                    else:
                        raise ValueError('Two or more chips have the same data sending address/port!')

        # Instantiate periphery devices and FPGA board (append log to all log files)
        with self._logging_through_handlers():
            self.log.info('Initializing %s...', self.__class__.__name__)
            self.periphery = BDAQ53Periphery(bench_config=self.configuration['bench'])
            self.bdaq = BDAQ53(conf=bdaq_conf, bench_config=self.configuration['bench'])

        # In case of 'per module' scan the chip-init loop below still needs log handler entries for each chip
        if not self.scan_per_module:
            scan_configuration_per_scan = self._scan_configuration_per_scan
            log_handlers_per_scan = self._log_handlers_per_scan
        else:
            scan_configuration_per_scan = self._module_scan_configuration_per_chip
            log_handlers_per_scan = self._module_log_handlers_per_chip

        # Instantiate RD53A chips, each using configuration from yaml, updated with scan configuration, updated with per-chip scan configuration
        for chip_conf, chip_config_override, scan_params, log_fh in zip(self._chips_confs, self._chip_configuration_per_chip,
                                                                        scan_configuration_per_scan, log_handlers_per_scan):
            with self._logging_through_handler(log_fh):

                # Load chip configuration file
                t_conf = self._load_chip_configuration(chip_conf['chip_configuration'], chip_conf['chip_sn'], chip_conf['chip_type'])

                # Backup mask settings here, replace changed settings before saving configuration to .cfg.yaml
                self._mask_confs.append(t_conf['masks'].copy())

                # Update loaded chip configuration with values from scan configuration
                recursive_update(first=t_conf, second=chip_config_override)

                # For simplicity: Append masks configuration to scan configuration, for being accessible from there
                masks_config = t_conf.get('masks', {'maskfile': 'auto', 'use_good_pixels_diff': False})
                scan_params['maskfile'] = masks_config['maskfile']
                scan_params['use_good_pixels_diff'] = masks_config['use_good_pixels_diff']

                if 'rd53a' in chip_conf['chip_type']:
                    self._chips.append(RD53A(self.bdaq, chip_sn=chip_conf['chip_sn'], chip_id=chip_conf['chip_id'], receiver=chip_conf['receiver'], config=t_conf))
                if 'ITkPixV1' in chip_conf['chip_type']:
                    self._chips.append(ITkPixV1(self.bdaq, chip_sn=chip_conf['chip_sn'], chip_id=chip_conf['chip_id'], receiver=chip_conf['receiver'], config=t_conf))

        self.chip = None        # Current chip when looping over scans
        self.chip_conf = None   # Current chip configuration when looping over scans

        self.initialized = False
        self.errors_occured = False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.log.error(exc_value)
            with self._logging_through_handlers():
                self.log.error('Scan failed!')
        elif self.errors_occured:
            with self._logging_through_handlers():
                self.log.error('Scan failed!')
        else:
            self._dump_all_chip_configurations()
            with self._logging_through_handlers():
                self.log.success('All done!')
        self.close()

    def _setup_output_directories(self):
        '''
            Prepares an output directory for every chip in the testbench configuration
            named after its chip serial number. Chips on the same module are grouped together.
        '''

        # Determine main working directory
        if self.configuration['bench']['general']['output_directory'] is not None:
            self.working_dir = self.configuration['bench']['general']['output_directory']
        else:
            self.working_dir = os.path.join(os.getcwd(), "output_data")

        # Do some checks for valid 'modules' section in testbench configuration

        # Cannot start scan without any module configured
        modules = self.configuration['bench']['modules']
        if modules is None:
            raise KeyError("No connected module specified.")

        serial_numbers = []

        def check_serial_number(serial_number):
            ''' Each chip serial number must be distinct, since data folder uses the chip sn '''
            if serial_number in serial_numbers:
                raise KeyError("Multiple chips have the same serial number %s in the testbench configuration." % serial_number)
            else:
                serial_numbers.append(serial_number)

        for module, chips in sorted(modules.items()):
            # Need at least one chip per module
            if chips is None:
                raise KeyError("No chip specified for module %s." % module)
            elif len(chips) == 1:
                if (next(iter(chips.items()))[0]).startswith('_'):
                    raise KeyError("No chip specified for module %s." % module)

            # Determine number of chips on current module
            num_chips = 0
            for key in sorted(chips.keys()):
                if key.startswith('_'):
                    continue
                num_chips += 1

            if num_chips == 1:
                for t_key, t_chip_conf in sorted(chips.items()):
                    if t_key.startswith('_'):
                        continue
                    chip_conf = t_chip_conf
                check_serial_number(chip_conf['chip_sn'])
            else:
                for key, chip_conf in sorted(chips.items()):
                    if key.startswith('_'):
                        continue
                    check_serial_number(chip_conf['chip_sn'])

        # Create output directories for each chip on each module

        for module, chips in sorted(modules.items()):
            # Determine number of chips on current module
            num_chips = 0
            for key in sorted(chips.keys()):
                if key.startswith('_'):
                    continue
                num_chips += 1

            # Store modules with a single chip separately
            if num_chips == 1:
                for t_key, t_chip_conf in sorted(chips.items()):
                    if t_key.startswith('_'):
                        continue
                    chip = t_key
                    chip_conf = t_chip_conf

                chip_conf['_module_name'] = module
                chip_conf['_chip_name'] = chip

                chip_conf['_out_path'] = self.working_dir + os.sep + chip_conf['chip_sn']
                if not os.path.exists(chip_conf['_out_path']):
                    os.makedirs(chip_conf['_out_path'])

                if self.scan_per_module:
                    chips['_module_name'] = module
                    chips['_out_path'] = chip_conf['_out_path']

            # Store data of chips from the same module in a common module directory
            else:
                # Loop over all front-end chips of current module
                for key, chip_conf in sorted(chips.items()):
                    if key.startswith('_'):
                        continue

                    chip_conf['_module_name'] = module
                    chip_conf['_chip_name'] = key

                    chip_conf['_out_path'] = self.working_dir + os.sep + module + os.sep + chip_conf['chip_sn']
                    chip_conf['_module_path'] = self.working_dir + os.sep + module
                    if not self.scan_per_module:
                        if not os.path.exists(chip_conf['_out_path']):
                            os.makedirs(chip_conf['_out_path'])

                chips['_module_path'] = self.working_dir + os.sep + module

                # Create only module directory for 'per module' scan
                if self.scan_per_module:
                    chips['_module_name'] = module
                    chips['_out_path'] = self.working_dir + os.sep + module
                    if not os.path.exists(chips['_out_path']):
                        os.makedirs(chips['_out_path'])

    def _search_chip_configurations(self):
        '''
            Search for chip configuration files for each chip in the testbench configuration.
            When specified, uses chip_conf_directory from 'testbench,yaml' as additional search path.
        '''

        # Set additional search path for chip configuration files
        if (self.configuration['bench']['general']['chip_conf_directory'] is not None) and os.path.exists(self.configuration['bench']['general']['chip_conf_directory']):
            self.chip_conf_directory = self.configuration['bench']['general']['chip_conf_directory']
        else:
            self.chip_conf_directory = ''

        modules = self.configuration['bench']['modules']

        for _, chips in sorted(modules.items()):

            for key, chip_conf in sorted(chips.items()):
                if key.startswith('_'):
                    continue

                if chip_conf['chip_configuration'] == 'auto':

                    chip_config_file = get_latest_chip_configuration_file(chip_conf['_out_path'])
                    if chip_config_file is None:
                        if chip_conf['chip_sn'] == '0x0000':
                            end_path = '..' + os.sep + 'chips' + os.sep + 'rd53a_default.cfg.yaml'
                        else:
                            end_path = chip_conf['chip_sn'] + '.cfg.yaml'
                        possible_paths = [os.path.join(self.proj_dir + os.sep + 'bdaq53', end_path),
                                          os.path.join(self.proj_dir + os.sep + 'bdaq53' + os.sep + 'scans', end_path),
                                          os.path.join(chip_conf['_out_path'], end_path),
                                          os.path.join(self.chip_conf_directory, end_path)]

                        for path in possible_paths:
                            if os.path.isfile(path):
                                chip_config_file = path
                                break
                        else:
                            chip_config_file = None
                else:
                    possible_paths = [chip_conf['chip_configuration'],
                                      os.path.join(self.proj_dir + os.sep + 'bdaq53', chip_conf['chip_configuration']),
                                      os.path.join(self.proj_dir + os.sep + 'bdaq53' + os.sep + 'scans', chip_conf['chip_configuration']),
                                      os.path.join(chip_conf['_out_path'], chip_conf['chip_configuration']),
                                      os.path.join(self.chip_conf_directory, chip_conf['chip_configuration'])]
                    for path in possible_paths:
                        if os.path.isfile(path):
                            chip_config_file = path
                            break
                    else:
                        chip_config_file = None

                if chip_config_file is not None:
                    chip_conf['chip_configuration'] = chip_config_file
                else:
                    chip_conf['chip_configuration'] = None

    def _load_chip_configuration(self, chip_config_file, chip_sn, chip_type):
        try:
            if chip_config_file is not None:
                self.log.info('Loading chip configuration for chip {0} from {1}'.format(chip_sn, chip_config_file))
                with open(chip_config_file, 'r') as f:
                    chip_configuration = yaml.full_load(f)
            elif 'rd53a' in chip_type:
                self.log.warning('No explicit configuration supplied for chip {0}. Using \'rd53a_default.cfg.yaml\'!'.format(chip_sn))
                with open(os.path.join(os.path.dirname(__file__), '..', 'chips', 'rd53a_default.cfg.yaml'), 'r') as f:
                    chip_configuration = yaml.full_load(f)
            elif 'ITkPixV1' in chip_type:
                self.log.warning(
                    'No explicit configuration supplied for chip {0}. Using \'ITkPixV1_default.cfg.yaml\'!'.format(chip_sn))
                with open(os.path.join(os.path.dirname(__file__), '..', 'chips', 'rd53a_default.cfg.yaml'), 'r') as f:
                    chip_configuration = yaml.full_load(f)
            else:
                self.log.error('Unknown chip type: {0}. Use rd53a or ITkPixV1. Failed to load configuration.'.format(chip_type))
        except Exception:
            self.log.warning('Could not load configuration file for chip {0}!'.format(chip_sn))
            chip_configuration = {'masks': {'maskfile': 'auto', 'use_good_pixels_diff': False}}

        return chip_configuration

    def _get_latest_maskfile(self, maskfile_directory=None):
        if maskfile_directory is None:
            maskfile_directory = self.chip_conf['_out_path']

        return get_latest_maskfile(maskfile_directory)

    def _load_maskfile(self, maskfile):
        '''
            Load masks from an h5 file and load them into chip
        '''

        self.log.info('Loading masks for chip {0} from {1}'.format(self.chip.get_sn(), maskfile))
        with tb.open_file(maskfile, 'r') as out_file:
            # Disable mask
            for node in out_file.list_nodes('/masks/', classname='CArray'):
                if 'disable' in node.name:
                    self.chip.masks.disable_mask[:] = np.logical_and(self.chip.masks.disable_mask[:], node[:])
                    self.chip.masks.apply_disable_mask()
                    break

            # Other masks
            for mask in ['tdac', 'injection', 'hitbus', 'lin_gain_sel', 'injection_delay']:
                for node in out_file.list_nodes('/masks/', classname='CArray'):
                    if node.name == mask:
                        self.chip.masks[mask][:] = node[:]
                        break

    def save_maskfile(self, filename=None):
        '''
            Save masks to maskfile
        '''

        if filename is None:
            maskfile = os.path.join(self.output_filename + '.masks.h5')
        else:
            if '.masks.h5' in filename:
                maskfile = os.path.join(self.chip_conf['_out_path'], filename)
            else:
                maskfile = os.path.join(self.chip_conf['_out_path'], filename + '.masks.h5')
        self.log.info('Writing masks to file {0}'.format(maskfile))
        with tb.open_file(maskfile, 'a') as h5_file:
            self._dump_masks(h5_file)
        self.configuration['scan']['maskfile'] = maskfile

    def save_all_maskfiles(self):
        '''
            Save masks of all chips to maskfiles
        '''
        for self.chip, self.chip_conf, scan_params, output_dir, log_fh in zip(self._chips, self._chips_confs, self._scan_configuration_per_scan,
                                                                              self._output_directories_per_scan, self._log_handlers_per_scan):
            self.configuration['scan'] = scan_params
            self.output_filename = os.path.join(output_dir, self.run_name)
            maskfile = scan_params['maskfile']
            with self._logging_through_handler(log_fh):
                self.save_maskfile(filename=maskfile)

    def _dump_masks(self, h5_file):
        '''
            Dump masks in a given h5_file
        '''

        start_column = self.configuration['scan'].get('start_column', 0)
        stop_column = self.configuration['scan'].get('stop_column', 400)
        start_row = self.configuration['scan'].get('start_row', 0)
        stop_row = self.configuration['scan'].get('stop_row', 192)

        infile = None
        if self.configuration['scan']['maskfile'] is not None:
            infile = tb.open_file(self.configuration['scan']['maskfile'], 'r')

        try:
            h5_file.create_group(h5_file.root, 'masks', 'Masks')
        except NodeError:
            pass

        # Disable mask
        for node in h5_file.list_nodes('/masks/', classname='CArray'):
            if 'disable' in node.name:
                old_mask = node[:]
                h5_file.remove_node(node)
                break
        else:
            if infile is not None:
                old_mask = infile.root.masks.disable[:]
            else:
                old_mask = np.ones(self.chip.masks.dimensions, bool)

        new_mask = old_mask[:]
        new_mask[start_column:stop_column, start_row:stop_row] = np.logical_and(old_mask[start_column:stop_column, start_row:stop_row],
                                                                                self.chip.masks.disable_mask[start_column:stop_column, start_row:stop_row])

        h5_file.create_carray(h5_file.root.masks,
                              name='disable',
                              title='disable',
                              obj=new_mask,
                              filters=tb.Filters(complib='blosc',
                                                 complevel=5,
                                                 fletcher32=False))

        # TDAC mask
        for node in h5_file.list_nodes('/masks/', classname='CArray'):
            if 'tdac' in node.name:
                old_mask = node[:]
                h5_file.remove_node(node)
                break
        else:
            if infile is not None:
                old_mask = infile.root.masks.tdac[:]
            else:
                old_mask = np.full(self.chip.masks.dimensions, self.chip.masks.defaults['tdac'])
                old_mask[128:264, :] = 7

        new_mask = old_mask[:]
        new_mask[start_column:stop_column, start_row:stop_row] = self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row]

        h5_file.create_carray(h5_file.root.masks,
                              name='tdac',
                              title='tdac',
                              obj=new_mask,
                              filters=tb.Filters(complib='blosc',
                                                 complevel=5,
                                                 fletcher32=False))

        # Injection delay mask
        for node in h5_file.list_nodes('/masks/', classname='CArray'):
            if 'injection_delay' in node.name:
                old_mask = node[:]
                h5_file.remove_node(node)
                break
        else:
            if infile is not None:
                old_mask = infile.root.masks.injection_delay[:]
            else:
                old_mask = np.full(self.chip.masks.dimensions, self.chip.masks.defaults['injection_delay'])

        new_mask = old_mask[:]
        new_mask[start_column:stop_column, start_row:stop_row] = self.chip.masks['injection_delay'][start_column:stop_column, start_row:stop_row]

        h5_file.create_carray(h5_file.root.masks,
                              name='injection_delay',
                              title='injection_delay',
                              obj=new_mask,
                              filters=tb.Filters(complib='blosc',
                                                 complevel=5,
                                                 fletcher32=False))

        # Other masks
        for mask in ['injection', 'hitbus', 'lin_gain_sel']:
            for node in h5_file.list_nodes('/masks/', classname='CArray'):
                if node.name == mask:
                    old_mask = node[:]
                    h5_file.remove_node(node)
                    break
            else:
                if infile is not None:
                    for node in infile.list_nodes('/masks/', classname='CArray'):
                        if node.name == mask:
                            old_mask = node[:]
                            break
                else:
                    old_mask = np.full(self.chip.masks.dimensions, self.chip.masks.defaults[mask])

            new_mask = old_mask[:]
            new_mask[start_column:stop_column, start_row:stop_row] = np.logical_and(old_mask[start_column:stop_column, start_row:stop_row],
                                                                                    self.chip.masks.disable_mask[start_column:stop_column, start_row:stop_row])

            h5_file.create_carray(h5_file.root.masks,
                                  name=mask,
                                  title=mask,
                                  obj=new_mask,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
        if infile is not None:
            infile.close()

    def _dump_configuration(self, outfile, format='h5'):
        '''
            Dump run configuration to raw data file
        '''
        if format == 'yaml':
            configuration = self.chip.configuration
            configuration['masks'] = self._mask_conf    # Do not overwrite mask settings
            yaml.dump(configuration, outfile)
        elif format == 'h5':
            # Run configuration
            run_config_table = outfile.create_table(outfile.root.configuration, name='run_config', title='Run config', description=RunConfigTable)
            row = run_config_table.row
            row['attribute'] = 'scan_id'
            row['value'] = self.scan_id
            row.append()
            row = run_config_table.row
            row['attribute'] = 'run_name'
            row['value'] = self.run_name
            row.append()
            row = run_config_table.row
            row['attribute'] = 'software_version'
            row['value'] = get_software_version()
            row.append()
            row = run_config_table.row
            row['attribute'] = 'module'
            row['value'] = self.module_conf['_module_name']
            row.append()
            if not self.scan_per_module:
                row = run_config_table.row
                row['attribute'] = 'chip_sn'
                row['value'] = self.chip.get_sn()
                row.append()
                row = run_config_table.row
                row['attribute'] = 'chip_type'
                row['value'] = self.chip.get_type()
                row.append()
                row = run_config_table.row
                row['attribute'] = 'receiver'
                row['value'] = self.chip.receiver
                row.append()

            # attributes which are not saved in run_config
            exclude_attributes = ['bdaq', 'chip']

            for param, value in self.configuration['scan'].items():
                if param in exclude_attributes:
                    continue
                else:
                    row = run_config_table.row
                    row['attribute'] = param
                    row['value'] = value if isinstance(value, str) else str(value)
                    row.append()
            run_config_table.flush()

            if self.scan_id in ['tune_tlu', 'ext_trigger_scan', 'source_scan', 'source_scan_injection', 'eudaq_scan']:
                for param, value in self.configuration['bench']['TLU'].items():
                    row = run_config_table.row
                    row['attribute'] = param
                    row['value'] = value if isinstance(value, str) else str(value)
                    row.append()
                run_config_table.flush()

            if self.scan_id in ['source_scan', 'ext_trigger_scan', 'hitor_calibration']:
                for param, value in self.configuration['bench']['TDC'].items():
                    row = run_config_table.row
                    row['attribute'] = param
                    row['value'] = value if isinstance(value, str) else str(value)
                    row.append()
                run_config_table.flush()

            if self.chip is not None:

                # Chip calibration table
                calibration_table = outfile.create_table(outfile.root.configuration, name='calibration', title='Calibration', description=RunConfigTable)
                for attr, val in self.chip.calibration.return_all_values().items():
                    row = calibration_table.row
                    row['attribute'] = attr
                    row['value'] = val
                    row.append()
                calibration_table.flush()

                # Chip register table
                exclude_registers = ['PIX_PORTAL', 'REGION_COL', 'REGION_ROW', 'PIX_MODE', 'BCIDCnt', 'TrigCnt', 'AI_REGION_COL', 'AI_REGION_ROW']
                register_table = outfile.create_table(outfile.root.configuration, name='registers', title='Registers', description=RegisterTable)
                for name, reg in self.chip.registers.items():
                    if name not in exclude_registers:
                        row = register_table.row
                        row['register'] = name
                        row['value'] = reg.get()
                        row.append()
                register_table.flush()

    def _dump_all_chip_configurations(self):
        '''
            Dump chip configurations of all chips to yaml files (if not a 'per module' scan)
        '''
        if not self.scan_per_module:
            for self.chip, self.chip_conf, self._mask_conf, scan_params, output_dir, log_fh in zip(self._chips, self._chips_confs, self._mask_confs,
                                                                                                   self._scan_configuration_per_scan, self._output_directories_per_scan,
                                                                                                   self._log_handlers_per_scan):
                with self._logging_through_handler(log_fh):
                    self.configuration['scan'] = scan_params
                    self.output_filename = os.path.join(output_dir, self.run_name)
                    with open(self.output_filename + '.cfg.yaml', 'w') as f:
                        self._dump_configuration(f, format='yaml')

    def _set_readout_status(self):
        self.readout_status = self.fifo_readout.print_readout_status()

    def _get_readout_status(self, receiver):
        discard_counts, soft_error_counts, hard_error_counts = self.readout_status
        discard_count = discard_counts[int(receiver[2])]
        soft_error_count = soft_error_counts[int(receiver[2])]
        hard_error_count = hard_error_counts[int(receiver[2])]
        return discard_count, soft_error_count, hard_error_count

    def _add_chip_status(self):
        '''
            Read all important chip values and dump to raw data file
        '''
        self.h5_file.create_group(self.h5_file.root, 'chip_status', 'Chip status')

        # Add periphery monitoring data if any
        if self.periphery.enabled and self.configuration['bench']['periphery'].get('monitoring', False):
            monitoring_data = {}
            for group_name, group in self.periphery.monitoring_data.items():
                monitoring_data[group_name] = {}
                for table_name, table in group.items():
                    t = table[:]
                    for rid, row in enumerate(t):
                        if row['timestamp'] < time.mktime(time.strptime(self.timestamp, "%Y%m%d_%H%M%S")):
                            t = np.delete(t, rid, axis=0)

                    monitoring_data[group_name][table_name] = t

            self.h5_file.create_group(self.h5_file.root.chip_status, name='periphery_monitoring', title='Periphery Monitoring')
            for grp in monitoring_data.keys():
                group = self.h5_file.create_group(self.h5_file.root.chip_status.periphery_monitoring, grp)
                for name, table in monitoring_data[grp].items():
                    self.h5_file.create_table(group, name, table)

        if self.record_chip_status and self.bdaq.board_version != 'SIMULATION':
            discard_count, soft_error_count, hard_error_count = self._get_readout_status(self.chip.receiver)
            aurora_table = self.h5_file.create_table(self.h5_file.root.chip_status, name='aurora_link', title='Aurora link status', description=RunConfigTable)
            row = aurora_table.row
            row['attribute'] = 'discard_counter'
            row['value'] = discard_count
            row.append()
            row['attribute'] = 'soft_error_counter'
            row['value'] = soft_error_count
            row.append()
            row['attribute'] = 'hard_error_counter'
            row['value'] = hard_error_count
            row.append()
            aurora_table.flush()

            voltages, currents = self.chip.get_chip_status()
            dac_currents_table = self.h5_file.create_table(self.h5_file.root.chip_status, name='ADCCurrents', title='ADC Currents', description=ChipStatusTable)
            dac_voltages_table = self.h5_file.create_table(self.h5_file.root.chip_status, name='ADCVoltages', title='ADC Voltages', description=ChipStatusTable)

            for name, value in currents.items():
                row = dac_currents_table.row
                row['attribute'] = name
                row['ADC'] = value[0]
                row['value'] = value[1]
                row.append()
            dac_currents_table.flush()
            for name, value in voltages.items():
                row = dac_voltages_table.row
                row['attribute'] = name
                row['ADC'] = value[0]
                row['value'] = value[1]
                row.append()
            dac_voltages_table.flush()

            # Temperature measurements
            other_table = self.h5_file.create_table(self.h5_file.root.chip_status, name='other', title='Other', description=RunConfigTable)
            row = other_table.row
            row['attribute'] = 'Chip temperature'
            row['value'] = self.chip.get_temperature(log=False)
            row.append()
            other_table.flush()

    def _store_scan_par_values(self):
        '''
            Create scan_params table after a scan
        '''
        # Create parameter description
        keys = set()  # find all keys to make the table column names
        for par_values in self.scan_parameters.values():
            keys.update(par_values.keys())
        fields = [('scan_param_id', np.uint32)]
        # FIXME only float32 supported so far
        fields.extend([(name, np.float32) for name in keys])

        scan_par_table = self.h5_file.create_table(self.h5_file.root.configuration, name='scan_params', title='Scan parameter values per scan parameter id', description=np.dtype(fields))
        for par_id, par_values in self.scan_parameters.items():
            a = np.full(shape=(1,), fill_value=np.NaN).astype(np.dtype(fields))
            for key, val in par_values.items():
                a['scan_param_id'] = par_id
                a[key] = np.float32(val)
            scan_par_table.append(a)

    def store_scan_par_values(self, scan_param_id, **kwargs):
        '''
            Manually store the scan parameter values for the scan parameter id
            This allows to reconstruct the scan parameter values for a given parameter state vector
        '''
        if self.scan_parameters.get(scan_param_id) and self.scan_parameters.get(scan_param_id) != kwargs:
            raise ValueError('You cannot change the scan parameter value of a scan parameter id')
        self.scan_parameters[scan_param_id] = kwargs

    def _configure_masks(self):
        '''
            Masks configuring steps always needed after chip reset and before scan configure
        '''

        if self.configuration['scan']['use_good_pixels_diff']:
            self.chip.masks.apply_good_pixel_mask_diff()

        # See if there's a mask file and load masks if necessary
        maskfile = self.configuration['scan']['maskfile']
        if maskfile == 'auto':
            maskfile = self._get_latest_maskfile()
        if maskfile is not None:
            self._load_maskfile(maskfile)
        self.configuration['scan']['maskfile'] = maskfile

    def _configure_fifo_readout(self):
        readout_all_channels = self.is_parallel_scan
        if not self.scan_per_module:
            self._first_read = False
            self.fifo_readout = FifoReadout(self.bdaq)
            self.readout_all_channels = readout_all_channels
            for receiver in self.bdaq.receivers:
                self.bdaq.rx_channels[receiver].reset_counters()

    def _configure_readout(self, readout_container):
        '''
            Prepare readout for on scan of the scan loop
        '''
        # Setup raw  data file
        self.ext_trig_num = 0
        filename = self.output_filename + '.h5'
        filter_raw_data = tb.Filters(complib='blosc', complevel=5, fletcher32=False)
        readout_container.filter_tables = tb.Filters(complib='zlib', complevel=5, fletcher32=False)
        readout_container.h5_file = tb.open_file(filename, mode='w', title=self.scan_id)
        readout_container.h5_file.create_group(readout_container.h5_file.root, 'configuration', 'Configuration')
        readout_container.raw_data_earray = readout_container.h5_file.create_earray(readout_container.h5_file.root, name='raw_data', atom=tb.UIntAtom(),
                                                                                    shape=(0,), title='raw_data', filters=filter_raw_data)
        readout_container.meta_data_table = readout_container.h5_file.create_table(readout_container.h5_file.root, name='meta_data', description=MetaTable,
                                                                                   title='meta_data', filters=readout_container.filter_tables)
        readout_container.trigger_table = readout_container.h5_file.create_table(readout_container.h5_file.root, name='trigger_table', description=MapTable,
                                                                                 title='trigger_table', filters=readout_container.filter_tables)
        readout_container.scan_parameters = OrderedDict()

        # Setup data sending
        if not self.scan_per_module:
            socket_addr = self.chip_conf.get('send_data', None)
            if socket_addr:
                try:
                    readout_container.context = zmq.Context()
                    readout_container.socket = readout_container.context.socket(zmq.PUB)  # publisher socket
                    readout_container.socket.bind(socket_addr)
                    self.log.debug('Sending data to server %s', socket_addr)
                except zmq.error.ZMQError:
                    self.log.exception('Cannot connect to socket for data sending.')
                    readout_container.context = None
                    readout_container.socket = None
            else:
                readout_container.context = None
                readout_container.socket = None
        else:
            readout_container.context = None
            readout_container.socket = None

    def _set_receiver_enabled(self, receiver=None, enabled=True):
        if receiver is not None:
            self.bdaq.rx_channels[receiver].set_en(enabled)

    def _run_pre_scan_tasks(self):

        # Load masks from config
        if not self.scan_per_module:
            self._set_receiver_enabled(receiver=self.chip.receiver, enabled=True)
            self._configure_masks()

        # Scan dependent configuration step before actual scan can be started (set enable masks etc.)
        self._configure(**self.configuration['scan'])

        if not self.scan_per_module:
            self._set_receiver_enabled(receiver=self.chip.receiver, enabled=False)

        self.periphery.get_module_power(module=self.module_conf['_module_name'], log=True)

    def _run_post_scan_tasks(self, errors=False):
        if errors:
            if not self.scan_per_module:
                string = 'chip ' + self.chip_conf['chip_sn']
            else:
                if self.module_conf['_module_name'].startswith('module_'):
                    string = 'module ' + self.module_conf['_module_name'].split('module_')[1]
                else:
                    string = 'module ' + self.module_conf['_module_name']
            self.log.error('Errors occured during ' + self.__class__.__name__ + ' for ' + string)

        self._dump_configuration(self.h5_file, format='h5')

        if not self.scan_per_module:
            self._dump_masks(self.h5_file)
            self._add_chip_status()

        self._store_scan_par_values()

        if not self.scan_per_module and self.socket:
            self.log.debug('Closing socket connection')
            self.socket.close()
            self.socket = None

        self.log.info('Closing raw data file: %s', self.output_filename + '.h5')
        self.h5_file.close()

    def _run_pre_analyze_tasks(self):
        if not self.scan_per_module:
            string = 'chip ' + self.chip_conf['chip_sn']
        else:
            if self.module_conf['_module_name'].startswith('module_'):
                string = 'module ' + self.module_conf['_module_name'].split('module_')[1]
            else:
                string = 'module ' + self.module_conf['_module_name']
        self.log.info('Starting analysis for ' + string)

    def _run_post_analyze_tasks(self, errors=False):
        if errors:
            if not self.scan_per_module:
                string = 'chip ' + self.chip_conf['chip_sn']
            else:
                if self.module_conf['_module_name'].startswith('module_'):
                    string = 'module ' + self.module_conf['_module_name'].split('module_')[1]
                else:
                    string = 'module ' + self.module_conf['_module_name']
            self.log.error('Errors occured during analysis of ' + self.__class__.__name__ + ' for ' + string)

    def _switch_chip(self, receiver):
        self.filter_tables = self.fifo_readout_dict[receiver].filter_tables
        self.h5_file = self.fifo_readout_dict[receiver].h5_file
        self.raw_data_earray = self.fifo_readout_dict[receiver].raw_data_earray
        self.trigger_table = self.fifo_readout_dict[receiver].trigger_table
        self.meta_data_table = self.fifo_readout_dict[receiver].meta_data_table
        self.scan_parameters = self.fifo_readout_dict[receiver].scan_parameters
        self.context = self.fifo_readout_dict[receiver].context
        self.socket = self.fifo_readout_dict[receiver].socket

    def _configure(self, **kwargs):
        '''
            Place here: configuration steps that need to be performed before each scan but don't belong to the actual scan routine.
            For multi-chip scans (i.e. multiple chips (or modules, if 'per-module scan') defined in the testbench config)
            this function is called by scan() before every single scan in the scan loop.

            To be implemented in the scan class, if needed.
        '''
        self.log.debug('configure() method not implemented; take std. configuration')

    def _scan(self, **kwargs):
        raise NotImplementedError('ScanBase.scan() not implemented')

    def _analyze(self, **_):
        self.log.warning('analyze() method not implemented; do not analyze data')

    def enable_hitor(self, enable=True):
        '''
            Configure the hitor display port connectors depending on scan type.
            If enable is False all HitOr ports are disabled.
            If enable is True and the scan is a parallel scan (e.g. external trigger scan, source scan),
            there is parallel data readout and thus all HitOr ports get activated and or-ed in the FPGA.
            If enable is True and the scan is *not* a parallel scan, the available HitOr ports
            are activated one after another, always one during the scan of one chip;
            the order depends on the sorting order in 'testbench.yaml' module section (see wiki...).

            Note: the TDC feature does currently only work with *one* chip.
        '''
        if enable:
            if self.is_parallel_scan:                # Enable all HitOr ports simultaneously
                active_ports_conf = 0b000
                for _ in range(len(self._chips)):
                    active_ports_conf = (active_ports_conf << 1) + 0b001
                self.bdaq.configure_hitor_inputs(active_ports_conf=active_ports_conf)
            else:
                if not hasattr(self, 'hitor_en'):
                    self.hitor_en = 0b001               # Enable first HitOr port (DP)
                else:
                    self.hitor_en = self.hitor_en << 1  # Enable next HitOr port (mDP)
                if self.hitor_en == 0b100:
                    self.hitor_en = 0b000               # Only two HitOr ports available
                self.bdaq.configure_hitor_inputs(active_ports_conf=self.hitor_en)
        else:
            self.bdaq.configure_hitor_inputs(active_ports_conf=0b000)     # Disable all ports

    def init(self, force=False, powercycle_modules=False):
        '''
            Initialize hardware and set one time settings (e.g. chip link config)
        '''
        if not self.initialized or force:

            with self._logging_through_handlers():      # TODO: log power supply logs for chips of same module only
                self.log.debug('Initialize hardware')
                self.periphery.init()

                if self.periphery.enabled and self.configuration['bench']['periphery'].get('monitoring'):
                    self.periphery.start_monitoring(outfile=os.path.join(self.working_dir, self.timestamp + '_periphery_monitoring'))

                if self.periphery.enabled:
                    self.periphery.power_on_BDAQ()

                    for module, chips_conf in sorted(self.modules_conf.items()):
                        if '_powersupply' in chips_conf.keys():
                            if powercycle_modules:
                                self.periphery.power_off_module(module)
                                time.sleep(1)
                            self.periphery.power_on_module(module, **chips_conf['_powersupply'])

                self.bdaq.init()
                self.bdaq.print_powered_dp_connectors()

                self.log.info('Initializing chips...')

            # In case of 'per module' scan the chip-init loop below still needs log handler entries for each chip
            if not self.scan_per_module:
                log_handlers_per_scan = self._log_handlers_per_scan
            else:
                log_handlers_per_scan = self._module_log_handlers_per_chip

            for chip, log_fh in zip(self._chips, log_handlers_per_scan):
                with self._logging_through_handler(log_fh):

                    self.bdaq.rx_channels[chip.receiver].set_en(True)

                    # Initialize chip
                    chip.init()

                    # Check if chip is configured properly
                    if self.bdaq.board_version != 'SIMULATION':
                        try:
                            chip.registers.check_all()
                        except RuntimeError:
                            chip.init_communication()
                            chip.write_command(chip.write_sync(write=False) * 256)
                            chip.registers.check_all()

                    self.bdaq.rx_channels[chip.receiver].set_en(False)

            # Keep each Aurora receiver disabled until the scan starts
            for _, rx in self.bdaq.rx_channels.items():
                rx.set_en(False)

            self.initialized = True
        else:
            with self._logging_through_handlers():
                self.log.info('Hardware already initialized, skip initialization!')

    def scan(self):
        '''
            Loop over all chips/modules in testbench and for each perform the scan routine:
            - Initialize hardware (communication, ...)
            - Configure readout / data handling (file writing, data sending)
            - Configure chip (maskfile, registers, ...)
            - Configure scan
            - Power supply measurement
            - Scan
            - Save scan configuration, masks, chip status
            - Close data file, stop data sending
        '''
        # Try to initialize hardware and establish communication with each chip.
        # If something fails already here, stop the scan.
        try:
            self.init(powercycle_modules=self.powercycle_modules)
        except Exception as e:
            self.log.error(e)
            self.errors_occured = True
            raise e

        self.scan_return = defaultdict(dict)        # Store return values of scan function
        self.fifo_readout_dict = {}                 # Reset dict

        # Default: run scan for every chip
        if not self.scan_per_module:

            errors = [False] * len(self._chips)

            # Configure readout files, masks etc. for each chip
            for idx, (self.chip,
                      self.chip_conf,
                      self.module_conf,
                      scan_params,
                      output_dir,
                      log_fh,
                      self.data,
                      error) in enumerate(zip(self._chips,
                                              self._chips_confs,
                                              self._modules_confs,
                                              self._scan_configuration_per_scan,
                                              self._output_directories_per_scan,
                                              self._log_handlers_per_scan,
                                              self._scan_data_containers,
                                              errors)):
                with self._logging_through_handler(log_fh):
                    self.configuration['scan'] = scan_params
                    self.output_directory = output_dir
                    self.output_filename = os.path.join(output_dir, self.run_name)
                    self.log.info('Configuring chip {0}...'.format(self.chip.get_sn()))
                    try:
                        readout_container = self.ScanDataContainer()
                        self._configure_readout(readout_container)
                    except Exception as e:
                        self.log.error(e)
                        self.errors_occured = True
                        errors[idx] = True
                    else:
                        self.fifo_readout_dict[self.chip_conf['receiver']] = readout_container
                        self._switch_chip(self.chip.receiver)
                    try:
                        self._run_pre_scan_tasks()
                    except Exception as e:
                        self.log.error(e)
                        self.errors_occured = True
                        errors[idx] = True

            # Create general FIFO readout (for all chips/modules)
            self._configure_fifo_readout()

            # Make sure monitor filter is blocking for all receivers before starting scan
            self.bdaq.set_monitor_filter(mode='block')

            # Perform actual scan for each chip
            for idx, (self.chip,
                      self.chip_conf,
                      self.module_conf,
                      scan_params,
                      output_dir,
                      log_fh,
                      self.data,
                      error) in enumerate(zip(self._chips,
                                              self._chips_confs,
                                              self._modules_confs,
                                              self._scan_configuration_per_scan,
                                              self._output_directories_per_scan,
                                              self._log_handlers_per_scan,
                                              self._scan_data_containers,
                                              errors)):

                self._switch_chip(self.chip.receiver)

                if error:
                    if self.is_parallel_scan:
                        break
                    else:
                        continue

                if self.is_parallel_scan:
                    for chip in self._chips:
                        self._set_receiver_enabled(receiver=chip.receiver, enabled=True)

                with self._logging_through_handler(log_fh):
                    self.configuration['scan'] = scan_params
                    if not self.is_parallel_scan:
                        self.output_directory = output_dir
                        self.output_filename = os.path.join(output_dir, self.run_name)
                        self.log.info('Starting {0} for chip {1}...'.format(self.__class__.__name__, self.chip.get_sn()))
                    else:
                        self.output_directory = None
                        self.output_filename = None
                        self.log.info('Starting {0}...'.format(self.__class__.__name__))
                    try:
                        # Enable receiver channel of current chip
                        self._set_receiver_enabled(receiver=self.chip.receiver, enabled=True)

                        scan_ret_val = self._scan(**self.configuration['scan'])
                        self.scan_return[self.chip_conf['_module_name']][self.chip_conf['_chip_name']] = scan_ret_val
                    except Exception as e:
                        self.log.error(e)
                        self.errors_occured = True
                        errors[idx] = True
                    finally:
                        # Disable receiver channel of current chip
                        self._set_receiver_enabled(receiver=self.chip.receiver, enabled=False)

                # Just run _scan for one chip, readout all receiver channels though
                if self.is_parallel_scan:
                    for chip in self._chips:
                        self._set_receiver_enabled(receiver=chip.receiver, enabled=False)
                    with self._logging_through_handlers():
                        self.log.info('Total amount of triggers accepted: {0}'.format(self.bdaq.get_trigger_counter()))
                        self.log.info('Total amount of triggers collected: {0}'.format(self.data.n_trigger))
                    break

            # Need to copy scan_parameters dict for all chips in case of ExtTrigger-type scan
            if self.is_parallel_scan:
                scan_parms = self.fifo_readout_dict[self.chip_conf['receiver']].scan_parameters
                for _, rd_cont in self.fifo_readout_dict.items():
                    rd_cont.scan_parameters = scan_parms

            # Get readout status once, for all receiver channels
            with self._logging_through_handlers():
                self._set_readout_status()

            # Finish scan for each chip: dump chip status, close file etc.
            for idx, (self.chip,
                      self.chip_conf,
                      self.module_conf,
                      scan_params,
                      output_dir,
                      log_fh,
                      self.data,
                      error) in enumerate(zip(self._chips,
                                              self._chips_confs,
                                              self._modules_confs,
                                              self._scan_configuration_per_scan,
                                              self._output_directories_per_scan,
                                              self._log_handlers_per_scan,
                                              self._scan_data_containers,
                                              errors)):

                self._switch_chip(self.chip.receiver)

                with self._logging_through_handler(log_fh):
                    self.configuration['scan'] = scan_params
                    self.output_directory = output_dir
                    self.output_filename = os.path.join(output_dir, self.run_name)
                    self.log.info('Closing {0} for chip {1}...'.format(self.__class__.__name__, self.chip.get_sn()))

                    self._set_receiver_enabled(receiver=self.chip.receiver, enabled=True)
                    if error:
                        try:
                            self._run_post_scan_tasks(errors=True)
                        except Exception as e:
                            self.log.error(e)
                    else:
                        try:
                            self._run_post_scan_tasks()
                        except Exception as e:
                            self.log.error(e)
                            self.errors_occured = True
                    self._set_receiver_enabled(receiver=self.chip.receiver, enabled=False)

        # For 'per module' scan: run scan for every module
        else:
            # Dont need / provide any chip instances
            self.chip = None
            self.chip_conf = None

            # Configure, perform and finish scan for each module
            for (self.module_conf,
                 scan_params,
                 output_dir,
                 log_fh,
                 self.data) in zip(self._modules_confs,
                                   self._scan_configuration_per_scan,
                                   self._output_directories_per_scan,
                                   self._log_handlers_per_scan,
                                   self._scan_data_containers):

                with self._logging_through_handler(log_fh):
                    self.configuration['scan'] = scan_params
                    self.output_directory = output_dir
                    self.output_filename = os.path.join(output_dir, self.run_name)

                    try:
                        # Need no readout, but perhaps still a raw data file
                        readout_container = self.ScanDataContainer()
                        self._configure_readout(readout_container)
                        self.fifo_readout_dict[self.module_conf['_module_name']] = readout_container
                        self._switch_chip(self.module_conf['_module_name'])

                        self._run_pre_scan_tasks()

                        # Perform actual scan
                        scan_ret_val = self._scan(**self.configuration['scan'])
                        self.scan_return[self.module_conf['_module_name']] = scan_ret_val
                    except Exception as e:
                        self.log.error(e)
                        self.errors_occured = True
                        try:
                            self._run_post_scan_tasks(errors=True)
                        except Exception as e:
                            self.log.error(e)
                    else:
                        try:
                            self._run_post_scan_tasks()
                        except Exception as e:
                            self.log.error(e)
                            self.errors_occured = True

        self.scan_return = dict(self.scan_return)
        return self.scan_return

    def analyze(self):
        '''
            Loop over all chips/modules in testbench and for each perform the analysis routine of the scan:
            - Analyze raw data, plotting
        '''

        # Default: run scan for every chip
        if not self.scan_per_module:
            for self.chip, self.chip_conf, scan_params, output_dir, log_fh, self.data in zip(self._chips, self._chips_confs, self._scan_configuration_per_scan,
                                                                                             self._output_directories_per_scan, self._log_handlers_per_scan,
                                                                                             self._scan_data_containers):
                self._switch_chip(self.chip.receiver)

                with self._logging_through_handler(log_fh):

                    self.configuration['scan'] = scan_params

                    # Place output files in chip's (or module's) distinct directory
                    self.output_directory = output_dir
                    self.output_filename = os.path.join(output_dir, self.run_name)

                    # Perform actual analysis
                    try:
                        self._run_pre_analyze_tasks()
                        if self.configuration['bench']['analysis'].get('blocking', True):
                            self._analyze()
                        else:
                            self.log.info('Analysis in seperate process')
                            self.wait_for_analysis()
                            self.ana_proc = multiprocessing.Process(target=self._analyze)
                            self.ana_proc.daemon = True
                            self.ana_proc.start()
                    except Exception as e:
                        self.log.error(e)
                        self.errors_occured = True
                        try:
                            self._run_post_analyze_tasks(errors=True)
                        except Exception as e:
                            self.log.error(e)
                    else:
                        try:
                            self._run_post_analyze_tasks()
                        except Exception as e:
                            self.log.error(e)
                            self.errors_occured = True

        # For 'per module' scan: run scan for every module
        else:
            # Dont need / provide any chip instances or maskfiles in this case
            self.chip = None
            self.chip_conf = None

            for self.module_conf, scan_params, output_dir, log_fh, self.data in zip(self._modules_confs, self._scan_configuration_per_scan,
                                                                                    self._output_directories_per_scan, self._log_handlers_per_scan,
                                                                                    self._scan_data_containers):
                self._switch_chip(self.module_conf['_module_name'])

                with self._logging_through_handler(log_fh):

                    self.configuration['scan'] = scan_params

                    # Place output files in chip's (or module's) distinct directory
                    self.output_directory = output_dir
                    self.output_filename = os.path.join(output_dir, self.run_name)

                    # Perform actual analysis
                    try:
                        self._run_pre_analyze_tasks()
                        if self.configuration['bench']['analysis'].get('blocking', True):
                            self._analyze()
                        else:
                            self.log.info('Analysis in seperate process')
                            self.wait_for_analysis()
                            self.ana_proc = multiprocessing.Process(target=self._analyze)
                            self.ana_proc.daemon = True
                            self.ana_proc.start()
                    except Exception as e:
                        self.log.error(e)
                        self.errors_occured = True
                        try:
                            self._run_post_analyze_tasks(errors=True)
                        except Exception as e:
                            self.log.error(e)
                    else:
                        try:
                            self._run_post_analyze_tasks()
                        except Exception as e:
                            self.log.error(e)
                            self.errors_occured = True

    def wait_for_analysis(self):
        ''' Block exction until analysis is finished '''
        if self.ana_proc:
            self.log.info('Waiting for analysis process to finish...')
            self.ana_proc.join()

    def start(self):
        '''
            Shortcut for consecutive calls to scan() and analyze()
        '''
        self.scan()
        self.analyze()

    def notify(self, message):
        if self.configuration['bench']['notifications']['enable_notifications']:
            try:
                for user in self.configuration['bench']['notifications']['slack_users']:
                    self.slack.chat_postMessage(channel=user, text=message, username='BDAQ53 Bot', icon_emoji=':robot_face:')
            except Exception as e:
                self.log.error('Notification error: {0}'.format(e))

    def close(self):
        for rd_dict in self.fifo_readout_dict.values():  # Close all raw data files and sockets
            try:
                rd_dict.h5_file.close()
            except AttributeError:
                pass
            try:
                if rd_dict.socket:
                    self.log.debug('Closing socket connection')
                    rd_dict.socket.close()
                    rd_dict.socket = None
            except AttributeError:
                pass
        self.bdaq.close()
        self.periphery.close()
        self._close_logfiles()
        self.initialized = False

    # Logfile handling

    def _create_logfile_handler(self, output_filename=None):
        if output_filename is None:
            output_filename = self.output_filename  # Make overwrite global output filename possible

        return logger.setup_logfile(output_filename + '.log')

    def _open_logfile(self, handler):
        logger.add_logfile_to_loggers(handler)

    def _close_logfile(self, handler):
        logger.close_logfile(handler)

    def _close_logfiles(self):
        for handler in self._log_handlers_per_scan:
            self._close_logfile(handler)

    @contextmanager
    def _logging_through_handler(self, handler):
        self._open_logfile(handler)
        try:
            yield
        finally:
            self._close_logfile(handler)

    @contextmanager
    def _logging_through_handlers(self):
        for handler in self._log_handlers_per_scan:
            self._open_logfile(handler)
        try:
            yield
        finally:
            for handler in self._log_handlers_per_scan:
                self._close_logfile(handler)

    # FIFOReadout methods

    @contextmanager
    def readout(self, scan_param_id=0, timeout=10.0, *args, **kwargs):

        self.scan_param_id = scan_param_id

        callback = kwargs.pop('callback', self.handle_data)
        fill_buffer = kwargs.pop('fill_buffer', False)
        clear_buffer = kwargs.pop('clear_buffer', False)

        receivers_readout = []
        if not self.readout_all_channels:
            self.fifo_readout.attach_channel(ReadoutChannel(receiver=self.chip.receiver, callback=callback, clear_buffer=clear_buffer, fill_buffer=fill_buffer))
            receivers_readout.append(self.chip.receiver)
        else:
            for chip in self._chips:
                self.fifo_readout.attach_channel(ReadoutChannel(receiver=chip.receiver, callback=callback, clear_buffer=clear_buffer, fill_buffer=fill_buffer))
                receivers_readout.append(chip.receiver)

        if not self._first_read:
            self._first_read = True
            for receiver in receivers_readout:
                self.bdaq.rx_channels[receiver].reset_logic()
            time.sleep(0.01)
            self.bdaq.wait_for_aurora_sync(sync_receivers=receivers_readout)

        self.start_readout(*args, **kwargs)
        try:
            yield
        finally:
            if self.bdaq.board_version == 'SIMULATION':
                for _ in range(100):
                    self.bdaq.rx_channels[self.chip.receiver].get_rx_ready()
            self.stop_readout(timeout=timeout)

    def start_readout(self, *args, **kwargs):
        errback = kwargs.pop('errback', self.handle_err)
        reset_rx = kwargs.pop('reset_rx', False)
        reset_sram_fifo = kwargs.pop('reset_sram_fifo', True)
        no_data_timeout = kwargs.pop('no_data_timeout', None)

        self.fifo_readout.start(errback=errback, reset_rx=reset_rx, reset_sram_fifo=reset_sram_fifo, no_data_timeout=no_data_timeout)

    def stop_readout(self, timeout=10.0):
        self.fifo_readout.stop(timeout=timeout)

    def add_trigger_table_data(self, cmd_repetitions, scan_param_id):
        '''
            Mapping data to scan_param_id.
        '''
        self.trigger_table.row['cmd_number_start'] = self.ext_trig_num
        self.trigger_table.row['cmd_number_stop'] = self.ext_trig_num + cmd_repetitions
        self.trigger_table.row['cmd_length'] = cmd_repetitions
        self.trigger_table.row['scan_param_id'] = scan_param_id

        self.ext_trig_num += cmd_repetitions

        self.trigger_table.row.append()
        self.trigger_table.flush()

    def handle_data(self, data_tuple, receiver=None):
        '''
            Handling of the data.
        '''
#         get_bin = lambda x, n: format(x, 'b').zfill(n)

        if self.is_parallel_scan:
            if receiver is None:
                raise ValueError('Callback function for parallel scan needs receiver ID!')
            self._switch_chip(receiver)

        total_words = self.raw_data_earray.nrows

        self.raw_data_earray.append(data_tuple[0])
        self.raw_data_earray.flush()

        len_raw_data = data_tuple[0].shape[0]
        self.meta_data_table.row['timestamp_start'] = data_tuple[1]
        self.meta_data_table.row['timestamp_stop'] = data_tuple[2]
        self.meta_data_table.row['error'] = data_tuple[3]
        self.meta_data_table.row['data_length'] = len_raw_data
        self.meta_data_table.row['index_start'] = total_words
        total_words += len_raw_data
        self.meta_data_table.row['index_stop'] = total_words
        self.meta_data_table.row['scan_param_id'] = self.scan_param_id

        self.meta_data_table.row.append()
        self.meta_data_table.flush()

        if self.socket:
            send_data(self.socket, data=data_tuple, scan_par_id=self.scan_param_id)

    def handle_err(self, exc):
        ''' Handle errors when readout is started '''
        msg = '%s' % exc[1]
        if msg:
            self.log.error('%s', msg)
        if self.configuration['bench']['general'].get('abort_on_rx_error', True):
            self.log.error('Aborting run...')
            self.fifo_readout.stop_readout.set()

    @property
    def data_buffer(self):
        '''
            Access data buffer of current receiver
        '''
        return self.fifo_readout.get_data_buffer(self.chip.receiver)
